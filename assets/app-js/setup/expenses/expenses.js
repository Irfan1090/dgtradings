var Expenses = function() {
	var getMaxId = function() {

		$.ajax({

			url : baseUrl + expenses.controller+'/getMaxId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$('#'+expenses.smallname+'Id').val(data);
				$('#max'+expenses.smallname+'IdHidden').val(data);
				$('#'+expenses.smallname+'IdHidden').val(data);
			}, error : function(xhr, status, error) {

				console.log(xhr.responseText);
			}
		});
	}

	return {

		init : function() {

			this.bindUI();
			this['bind'+this.bigname+'FormValidation']();
		},

		bindUI : function() {

			var self = this;
			$('#VoucherTypeHidden').val('new');

			shortcut.add("F10", function() {
    			
    			$('.btnSave').trigger('click');
			});

			shortcut.add("F11", function() {
    			
    			$('.btnSaveAndMore').trigger('click');
			});

			shortcut.add("F1", function() {

				$('a[href="#'+self.smallname+'Lookup"]').trigger('click');
			});

			shortcut.add("F5", function() {

    			$('.btnReset').trigger('click');
			});

			shortcut.add("F2", function() {

    			common.redirectUrl($('.btnBack').data('url'));
			});

			// when save button is clicked
			$('.btnSave, .btnSave2').on('click', function(e) {

				e.preventDefault();
				$("a[href='#basicInfo']").trigger('click');
		        if($(self.formid).valid()) {
		            
		            self.initSave('save');
		        }
			});

			$('.btnSaveAndMore, .btnSaveAndMore2').on('click', function(e) {

				e.preventDefault();
				$("a[href='#basicInfo']").trigger('click');
		        if($(self.formid).valid()) {
		            
		            self.initSave('SaveAndMore');
		        }
			});

			// when addMoreInf is clicked
			$('.btnNext').on('click', function(e) {

				if($(self.formid).valid()) {
		           	
		           	e.preventDefault(); 
		           	$('.alert-danger').hide();
		            $("a[href='#detailInfo']").trigger('click');
					$('.btnNext').hide();
					$('.btnPrev').show();
		        }
			});

			$('.btnPrev').on('click', function(e) {

				e.preventDefault();
				$("a[href='#basicInfo']").trigger('click');
				$('.btnNext').show();
				$('.btnPrev').hide();
			});

			$('a[href="#basicInfo"]').on('click',function(e){
			  	
			  	e.preventDefault();
			  	$('.btnNext').show();
				$('.btnPrev').hide();
			});

			$('a[href="#detailInfo"]').on('click',function(e){
			  	
			  	e.preventDefault();
			  	$('.btnNext').hide();
				$('.btnPrev').show();
			});

			$('#'+self.smallname+'Id').on('change', function() {
				
				var pid = $(this).val();
				obj = {};
				obj['url'] = self.controller+'/fetch';
				obj['data'] = {'spid' : pid};
				obj['form'] = self.formname;
				obj['maxId'] = self.smallname+'IdHidden';
				obj['maxHidId'] = 'max'+self.bigname+'IdHidden';

				customForm.fetch(obj,'definitions');
			});

			// when text is chenged inside the id textbox
			$('#'+self.smallname+'Id').on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $(this).val().trim() !== "" ) {

						var pid = $(this).val();
						obj = {};
						obj['url'] = self.controller+'/fetch';
						obj['data'] = {'spid' : pid};
						obj['form'] = self.formname;
						obj['maxId'] = self.smallname+'IdHidden';
						obj['maxHidId'] = 'max'+self.bigname+'IdHidden';

						customForm.fetch(obj);
					}
				}
			});

			$('.btnReset, .btnReset2').on('click', function(e){

				e.preventDefault();
				customForm.resetForm({'form' : self.formname});
				getMaxId();

				$('a[href="#basicInfo"]').trigger('click');
			});

			// when edit button is clicked inside the table view
			$('#'+self.smallname+'ModalTable').on('click', '.btnEdit', function(e) {
				
				e.preventDefault();
		
				var pid = $(this).data('spid');
				obj = {};
				obj['url'] = self.controller+'/fetch';
				obj['data'] = {'spid' : spid,'etype':etype};
				obj['form'] = self.formname;

				customForm.fetch(obj);
				$('#'+self.smallname+'Id').val(pid);
				$('a[href="#basicInfo"]').trigger('click');
			});

			self.fetchRequestedForm();
		},

		// makes the voucher ready to save
		initSave : function(saveType) {

			var Obj = customForm.getSaveObj({'form' : this.formname});	// returns the account detail object to save into database
			
			var obj = {};
			obj['url'] = this.controller+'/save';
			obj['data'] = { [this.savedataname] : Obj, 'voucher_type_hidden' : $('#voucherTypeHidden').val()};
			obj['saveType'] = saveType;
			obj['form'] = this.formname;
			obj['attr'] = this.showname;
			obj['redirect'] = this.controller;
			customForm.saveForm(obj);		// saves the detail into the database				
		},
		controller : "Expenses",
		formname : "formAddExpenses",
	 	formid : "#formAddExpenses",
	 	smallname : "expenses",
	 	bigname : this.controller,
	 	etype : this.smallname,
	 	savedataname : "expenses_detail",
	 	showname : "Expenses",
		fetchRequestedForm : function () {

			var innerid = common.getQueryStringVal('id');
			if (innerid && !isNaN(innerid) ) {

				$("input[name='"+this.smallname+"Id']").val(innerid).trigger('change');
			}else{

				getMaxId();
			}
		},

		['bind'+this.bigname+'FormValidation'] : function(){
			var ob = {
				[this.smallname+"id"]:'hello'
			};
			console.log(ob);

			var form = $(this.formid);
	        var error = $('.alert-danger', form);
	        var success = $('.alert-success', form);

	        form.validate({

	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",  // validate all fields including form hidden input
	            rules: {
	                [this.smallname+"id"]: {
	                    min: 1,
	                    required: true
	                },
	                name: {
	                    minlength: 2,
	                    required: true
	                },
	                default_date: {
	                    minlength: 8,
	                    required: true
	                },
	                level3: {
	                    minlength: 8,
	                    required: true
	                }
	         
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	                success.hide();
	                error.show();
	                App.scrollTo(error, -200);
	            },

	            errorPlacement: function (error, element) { // render error placement for each input type

	                var icon = $(element).parent('.input-icon').children('i');
	                icon.removeClass('fa-check').addClass("fa-warning");  
	                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	                if (element.attr("data-error-container")) { 

	                    error.appendTo(element.attr("data-error-container"));
	                }else{

	                    error.insertAfter(element);
	                }
	            },

	            highlight: function (element) { // hightlight error inputs
	                
	                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                
	            },

	            success: function (label, element) {

	                var icon = $(element).parent('.input-icon').children('i');
	                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	                icon.removeClass("fa-warning").addClass("fa-check");
	            },

	            submitHandler: function (form) {

	                success.show();
	                error.hide();
	                form[0].submit(); // submit the form
	            }
	        });
		}
	};
};

var expenses = new Expenses();
expenses.init();