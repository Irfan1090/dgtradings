var Banks = function() {

	var getMaxId = function() {

		$.ajax({

			url : baseUrl + 'banks/getMaxId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$('#banksId').val(data);
				$('#maxBanksIdHidden').val(data);
				$('#banksIdHidden').val(data);
			}, error : function(xhr, status, error) {

				console.log(xhr.responseText);
			}
		});
	}

	return {

		init : function() {

			this.bindUI();
			this.bindBanksFormValidation();
		},

		bindUI : function() {

			var self = this;
			$('#VoucherTypeHidden').val('new');

			shortcut.add("F10", function() {
    			
    			$('.btnSave').trigger('click');
			});

			shortcut.add("F11", function() {
    			
    			$('.btnSaveAndMore').trigger('click');
			});

			shortcut.add("F1", function() {

				$('a[href="#banksLookup"]').trigger('click');
			});

			shortcut.add("F5", function() {

    			$('.btnReset').trigger('click');
			});

			shortcut.add("F2", function() {

    			common.redirectUrl($('.btnBack').data('url'));
			});

			// when save button is clicked
			$('.btnSave, .btnSave2').on('click', function(e) {

				e.preventDefault();
				$("a[href='#basicInfo']").trigger('click');
		        if($('#formAddBanks').valid()) {
		            
		            self.initSave('save');
		        }
			});

			$('.btnSaveAndMore, .btnSaveAndMore2').on('click', function(e) {

				e.preventDefault();
				$("a[href='#basicInfo']").trigger('click');
		        if($('#formAddBanks').valid()) {
		            
		            self.initSave('SaveAndMore');
		        }
			});

			// when addMoreInf is clicked
			$('.btnNext').on('click', function(e) {

				if($('#formAddBanks').valid()) {
		           	
		           	e.preventDefault(); 
		           	$('.alert-danger').hide();
		            $("a[href='#detailInfo']").trigger('click');
					$('.btnNext').hide();
					$('.btnPrev').show();
		        }
			});

			$('.btnPrev').on('click', function(e) {

				e.preventDefault();
				$("a[href='#basicInfo']").trigger('click');
				$('.btnNext').show();
				$('.btnPrev').hide();
			});

			$('a[href="#basicInfo"]').on('click',function(e){
			  	
			  	e.preventDefault();
			  	$('.btnNext').show();
				$('.btnPrev').hide();
			});

			$('a[href="#detailInfo"]').on('click',function(e){
			  	
			  	e.preventDefault();
			  	$('.btnNext').hide();
				$('.btnPrev').show();
			});

			$('#banksId').on('change', function() {
				
				var pid = $(this).val();
				obj = {};
				obj['url'] = 'banks/fetch';
				obj['data'] = {'pid' : pid};
				obj['form'] = 'formAddBanks';
				obj['maxId'] = 'banksIdHidden';
				obj['maxHidId'] = 'maxBanksIdHidden';

				customForm.fetch(obj);
			});

			// when text is chenged inside the id textbox
			$('#banksId').on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $(this).val().trim() !== "" ) {

						var pid = $(this).val();
						obj = {};
						obj['url'] = 'banks/fetch';
						obj['data'] = {'pid' : pid};
						obj['form'] = 'formAddBanks';
						obj['maxId'] = 'banksIdHidden';
						obj['maxHidId'] = 'maxBanksIdHidden';

						customForm.fetch(obj);
					}
				}
			});

			$('.btnReset, .btnReset2').on('click', function(e){

				e.preventDefault();
				customForm.resetForm({'form' : 'formAddBanks'});
				getMaxId();

				$('a[href="#basicInfo"]').trigger('click');
			});

			// when edit button is clicked inside the table view
			$('#banksModalTable').on('click', '.btnBanksEdit', function(e) {
				
				e.preventDefault();
		
				var pid = $(this).data('spid');
				obj = {};
				obj['url'] = 'banks/fetch';
				obj['data'] = {'spid' : spid};
				obj['form'] = 'formAddBanks';

				customForm.fetch(obj);
				$('#banksId').val(pid);
				$('a[href="#basicInfo"]').trigger('click');
			});


			// when selection is change in txtLevel3 dropdown
			/*$(settings.txtLevel3).on('change', function() {

				var level3 = $(settings.txtLevel3).val();
				var level2 = $(settings.txtselectedLevel2);
				var level1 = $(settings.txtselectedLevel1);

				// reset values
				level2.text('');
				level1.text('');

				if (level3 !== "" && level3 !== null) {


					level2.text(' ' + $(this).find('option:selected').data('level2'));
					level1.text(' ' + $(this).find('option:selected').data('level1'));
				}
			});*/

			self.fetchRequestedForm();
		},

		// makes the voucher ready to save
		initSave : function(saveType) {

			var banksObj = customForm.getSaveObj({'form' : 'formAddBanks'});	// returns the account detail object to save into database
			
			var obj = {};
			obj['url'] = 'banks/save';
			obj['data'] = { 'banks_detail' : banksObj, 'voucher_type_hidden' : $('#voucherTypeHidden').val()};
			obj['saveType'] = saveType;
			obj['form'] = 'formAddBanks';
			obj['attr'] = 'Banks';
			obj['redirect'] = 'banks';
			customForm.saveForm(obj);		// saves the detail into the database				
		},

		fetchRequestedForm : function () {

			var banksId = common.getQueryStringVal('id');
			if (banksId && !isNaN(banksId) ) {

				$("input[name='banksId']").val(banksId).trigger('change');
			}else{

				getMaxId();
			}
		},

		bindBanksFormValidation : function(){

			var form = $('#formAddBanks');
	        var error = $('.alert-danger', form);
	        var success = $('.alert-success', form);

	        form.validate({

	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",  // validate all fields including form hidden input
	            rules: {
	                accountId: {
	                    min: 1,
	                    required: true
	                },
	                name: {
	                    minlength: 2,
	                    required: true
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	                success.hide();
	                error.show();
	                App.scrollTo(error, -200);
	            },

	            errorPlacement: function (error, element) { // render error placement for each input type

	                var icon = $(element).parent('.input-icon').children('i');
	                icon.removeClass('fa-check').addClass("fa-warning");  
	                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	                if (element.attr("data-error-container")) { 

	                    error.appendTo(element.attr("data-error-container"));
	                }else{

	                    error.insertAfter(element);
	                }
	            },

	            highlight: function (element) { // hightlight error inputs
	                
	                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                
	            },

	            success: function (label, element) {

	                var icon = $(element).parent('.input-icon').children('i');
	                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	                icon.removeClass("fa-warning").addClass("fa-check");
	            },

	            submitHandler: function (form) {

	                success.show();
	                error.hide();
	                form[0].submit(); // submit the form
	            }
	        });
		}
	};
};

var banks = new Banks();
banks.init();