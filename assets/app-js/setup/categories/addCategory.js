var Category = function() {

	var getMaxId = function() {

		$.ajax({

			url : baseUrl + 'categories/getMaxId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$('#catId').val(data);
				$('#maxCatIdHidden').val(data);
				$('#catIdHidden').val(data);
			}, error : function(xhr, status, error) {

				console.log(xhr.responseText);
			}
		});
	}

	return {

		init : function() {

			this.bindUI();
			this.bindCategoryFormValidation();
		},

		bindUI : function() {

			var self = this;
			$('#VoucherTypeHidden').val('new');

			shortcut.add("F10", function() {
    			
    			$('.btnSave').trigger('click');
			});

			shortcut.add("F11", function() {
    			
    			$('.btnSaveAndMore').trigger('click');
			});

			shortcut.add("F5", function() {

    			$('.btnReset').trigger('click');
			});

			shortcut.add("F2", function() {

    			common.redirectUrl($('.btnBack').data('url'));
			});

			// when save button is clicked
			$('.btnSave, .btnSave2').on('click', function(e) {

				e.preventDefault();
		        if($('#formAddCategory').valid()) {
		            
		            self.initSave('save');
		        }
			});

			$('.btnSaveAndMore, .btnSaveAndMore2').on('click', function(e) {

				e.preventDefault();
		        if($('#formAddCategory').valid()) {
		            
		            self.initSave('SaveAndMore');
		        }
			});

			$('#catId').on('change', function() {
				
				var catId = $(this).val();
				obj = {};
				obj['url'] = 'categories/fetch';
				obj['data'] = {'cat_id' : catId};
				obj['form'] = 'formAddCategory';
				obj['maxId'] = 'catIdHidden';
				obj['maxHidId'] = 'maxCatIdHidden';

				customForm.fetch(obj);
			});

			// when text is chenged inside the id textbox
			$('#catId').on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $(this).val().trim() !== "" ) {

						var catId = $(this).val();
						obj = {};
						obj['url'] = 'categories/fetch';
						obj['data'] = {'cat_id' : catId};
						obj['form'] = 'formAddCategory';
						obj['maxId'] = 'catIdHidden';
						obj['maxHidId'] = 'maxCatIdHidden';

						customForm.fetch(obj);
					}
				}
			});

			$('.btnReset, .btnReset2').on('click', function(e){

				e.preventDefault();
				customForm.resetForm({'form' : 'formAddCategory'});
				getMaxId();
			});

			self.fetchRequestedForm();
		},

		// makes the voucher ready to save
		initSave : function(saveType) {

			var categoryObj = customForm.getSaveObj({'form' : 'formAddCategory'});	// returns the account detail object to save into database
			
			var obj = {};
			obj['url'] = 'categories/save';
			obj['data'] = { 'category_detail' : categoryObj, 'voucher_type_hidden' : $('#voucherTypeHidden').val()};
			obj['saveType'] = saveType;
			obj['form'] = 'formAddCategory';
			obj['attr'] = 'Category';
			obj['redirect'] = 'categories';
			customForm.saveForm(obj);		// saves the detail into the database				
		},

		fetchRequestedForm : function () {

			var catId = common.getQueryStringVal('id');
			if (catId && !isNaN(catId) ) {

				$("input[name='catId']").val(catId).trigger('change');
			}else{

				getMaxId();
			}
		},

		bindCategoryFormValidation : function(){

			var form = $('#formAddCategory');
	        var error = $('.alert-danger', form);
	        var success = $('.alert-success', form);

	        form.validate({

	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",  // validate all fields including form hidden input
	            rules: {
	                catId: {
	                    min: 1,
	                    required: true
	                },
	                name: {
	                    minlength: 2,
	                    required: true
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	                success.hide();
	                error.show();
	                App.scrollTo(error, -200);
	            },

	            errorPlacement: function (error, element) { // render error placement for each input type

	                var icon = $(element).parent('.input-icon').children('i');
	                icon.removeClass('fa-check').addClass("fa-warning");  
	                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	                if (element.attr("data-error-container")) { 

	                    error.appendTo(element.attr("data-error-container"));
	                }else{

	                    error.insertAfter(element);
	                }
	            },

	            highlight: function (element) { // hightlight error inputs
	                
	                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                
	            },

	            success: function (label, element) {

	                var icon = $(element).parent('.input-icon').children('i');
	                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	                icon.removeClass("fa-warning").addClass("fa-check");
	            },

	            submitHandler: function (form) {

	                success.show();
	                error.hide();
	                form[0].submit(); // submit the form
	            }
	        });
		}
	};
};

var category = new Category();
category.init();