var Item = function() {

	var getMaxId = function() {

		$.ajax({

			url : baseUrl + 'items/getMaxId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$('#itemId').val(data);
				$('#maxItemIdHidden').val(data);
				$('#itemIdHidden').val(data);
			}, error : function(xhr, status, error) {

				console.log(xhr.responseText);
			}
		});
	}

	return {

		init : function() {

			this.bindUI();
			this.bindItemFormValidation();
			this.bindCategoryFormValidation();
			this.bindSubCategoryFormValidation();
			this.bindBrandFormValidation();
			this.bindMadeFormValidation();
		},

		bindUI : function() {

			var self = this;
			$('#VoucherTypeHidden').val('new');

			shortcut.add("F10", function() {
    			
    			$('.btnSave').trigger('click');
			});

			shortcut.add("F11", function() {
    			
    			$('.btnSaveAndMore').trigger('click');
			});

			shortcut.add("F5", function() {

    			$('.btnReset').trigger('click');
			});

			shortcut.add("F2", function() {

    			common.redirectUrl($('.btnBack').data('url'));
			});

			// when save button is clicked
			$('.btnSave, .btnSave2').on('click', function(e) {

				e.preventDefault();
				$("a[href='#basicInfo']").trigger('click');
		        if($('#formAddItem').valid()) {
		            
		            self.initSave('save');
		        }
			});

			$('.btnSaveAndMore, .btnSaveAndMore2').on('click', function(e) {

				e.preventDefault();
				$("a[href='#basicInfo']").trigger('click');
		        if($('#formAddItem').valid()) {
		            
		            self.initSave('SaveAndMore');
		        }
			});

			// when addMoreInf is clicked
			$('.btnNext').on('click', function(e) {

				if($('#formAddItem').valid()) {
		           	
		           	e.preventDefault(); 
		           	$('.alert-danger').hide();
		            $("a[href='#detailInfo']").trigger('click');
					$('.btnNext').hide();
					$('.btnPrev').show();
		        }
			});

			$('.btnPrev').on('click', function(e) {

				e.preventDefault();
				$("a[href='#basicInfo']").trigger('click');
				$('.btnNext').show();
				$('.btnPrev').hide();
			});

			$('a[href="#basicInfo"]').on('click',function(e){
			  	
			  	e.preventDefault();
			  	$('.btnNext').show();
				$('.btnPrev').hide();
			});

			$('a[href="#detailInfo"]').on('click',function(e){
			  	
			  	e.preventDefault();
			  	$('.btnNext').hide();
				$('.btnPrev').show();
			});

			$('#itemId').on('change', function() {
				
				var itemId = $(this).val();
				obj = {};
				obj['url'] = 'items/fetch';
				obj['data'] = {'item_id' : itemId};
				obj['form'] = 'formAddItem';
				obj['maxId'] = 'itemIdHidden';
				obj['maxHidId'] = 'maxItemIdHidden';

				customForm.fetch(obj);
			});

			// when text is chenged inside the id textbox
			$('#itemId').on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $(this).val().trim() !== "" ) {

						var itemId = $(this).val();
						obj = {};
						obj['url'] = 'items/fetch';
						obj['data'] = {'item_id' : itemId};
						obj['form'] = 'formAddItem';
						obj['maxId'] = 'itemIdHidden';
						obj['maxHidId'] = 'maxItemIdHidden';

						customForm.fetch(obj);
					}
				}
			});

			$('.btnReset, .btnReset2').on('click', function(e){

				e.preventDefault();
				customForm.resetForm({'form' : 'formAddItem'});
				getMaxId();

				$('a[href="#basicInfo"]').trigger('click');
			});

			$('#itemCode').on('change', function() {
				
				if($(this).val()){

					$('#itemId').val($(this).val()).trigger('change');
				}
			});

			$('select[name=catid]').on('change', function(){
				
				if($(this).val() == 'new'){

					$('#categoryModal').modal('show');
				}
			});

			$('.btnSaveCategory').on('click', function(e) {

				e.preventDefault();
		        if($('#formAddCategory').valid()) {
		            
		            self.initSaveCategory('modal');
		        }
			});

			$('.btnResetCategory').on('click', function(e){

				e.preventDefault();
				customForm.resetForm({'form' : 'formAddCategory'});
			});

			$('select[name=subcatid]').on('change', function(){
				
				if($(this).val() == 'new'){

					$('#subCategoryModal').modal('show');
				}
			});

			$('.btnSaveSubCategory').on('click', function(e) {

				e.preventDefault();
		        if($('#formAddSubCategory').valid()) {
		            
		            self.initSaveSubCategory('modal');
		        }
			});

			$('.btnResetSubCategory').on('click', function(e){

				e.preventDefault();
				customForm.resetForm({'form' : 'formAddSubCategory'});
			});

			$('select[name=bid]').on('change', function(){
				
				if($(this).val() == 'new'){

					$('#brandModal').modal('show');
				}
			});

			$('.btnSaveBrand').on('click', function(e) {

				e.preventDefault();
		        if($('#formAddBrand').valid()) {
		            
		            self.initSaveBrand('modal');
		        }
			});

			$('.btnResetBrand').on('click', function(e){

				e.preventDefault();
				customForm.resetForm({'form' : 'formAddBrand'});
			});

			$('select[name=made_id]').on('change', function(){
				
				if($(this).val() == 'new'){

					$('#madeModal').modal('show');
				}
			});

			$('.btnSaveMade').on('click', function(e) {

				e.preventDefault();
		        if($('#formAddMade').valid()) {
		            
		            self.initSaveMade('modal');
		        }
			});

			$('.btnResetMade').on('click', function(e){

				e.preventDefault();
				customForm.resetForm({'form' : 'formAddMade'});
			});

			self.fetchRequestedForm();
		},

		// makes the voucher ready to save
		initSave : function(saveType) {

			var itemObj = customForm.getSaveObj({'form' : 'formAddItem'});	// returns the account detail object to save into database
			
			var obj = {};
			obj['url'] = 'items/save';
			obj['data'] = { 'item_detail' : itemObj, 'voucher_type_hidden' : $('#voucherTypeHidden').val()};
			obj['saveType'] = saveType;
			obj['form'] = 'formAddItem';
			obj['attr'] = 'Item';
			obj['redirect'] = 'items';
			customForm.saveForm(obj);		// saves the detail into the database				
		},

		initSaveCategory : function(saveType) {

			var categoryObj = customForm.getSaveObj({'form' : 'formAddCategory'});	// returns the account detail object to save into database
			
			var obj = {};
			obj['url'] = 'categories/save';
			obj['data'] = { 'category_detail' : categoryObj, 'voucher_type_hidden' : $('#voucherTypeHidden').val()};
			obj['saveType'] = saveType;
			obj['form'] = 'formAddCategory';
			obj['attr'] = 'Category';
			obj['appendForm'] = 'formAddItem';
			obj['appendInputName'] = 'name';
			obj['appendSelectName'] = 'catid';
			obj['modal'] = 'categoryModal';
			customForm.saveForm(obj);		// saves the detail into the database				
		},

		initSaveSubCategory : function(saveType) {

			var subcategoryObj = customForm.getSaveObj({'form' : 'formAddSubCategory'});	// returns the account detail object to save into database
			
			var obj = {};
			obj['url'] = 'subCategories/save';
			obj['data'] = { 'sub_category_detail' : subcategoryObj, 'voucher_type_hidden' : $('#voucherTypeHidden').val()};
			obj['saveType'] = saveType;
			obj['form'] = 'formAddSubCategory';
			obj['attr'] = 'Subcategory';
			obj['appendForm'] = 'formAddItem';
			obj['appendInputName'] = 'name';
			obj['appendSelectName'] = 'subcatid';
			obj['modal'] = 'subCategoryModal'
			customForm.saveForm(obj);		// saves the detail into the database				
		},

		initSaveBrand : function(saveType) {

			var brandObj = customForm.getSaveObj({'form' : 'formAddBrand'});	// returns the account detail object to save into database
			
			var obj = {};
			obj['url'] = 'brands/save';
			obj['data'] = { 'brand_detail' : brandObj, 'voucher_type_hidden' : $('#voucherTypeHidden').val()};
			obj['saveType'] = saveType;
			obj['form'] = 'formAddBrand';
			obj['attr'] = 'Brand';
			obj['appendForm'] = 'formAddItem';
			obj['appendInputName'] = 'name';
			obj['appendSelectName'] = 'bid';
			obj['modal'] = 'brandModal'
			customForm.saveForm(obj);		// saves the detail into the database				
		},

		initSaveMade : function(saveType) {

			var madeObj = customForm.getSaveObj({'form' : 'formAddMade'});	// returns the account detail object to save into database
			
			var obj = {};
			obj['url'] = 'made/save';
			obj['data'] = { 'made_detail' : madeObj, 'voucher_type_hidden' : $('#voucherTypeHidden').val()};
			obj['saveType'] = saveType;
			obj['form'] = 'formAddMade';
			obj['attr'] = 'Made';
			obj['appendForm'] = 'formAddItem';
			obj['appendInputName'] = 'name';
			obj['appendSelectName'] = 'made_id';
			obj['modal'] = 'madeModal'
			customForm.saveForm(obj);		// saves the detail into the database				
		},

		fetchRequestedForm : function () {

			var itemId = common.getQueryStringVal('id');
			if (itemId && !isNaN(itemId) ) {

				$("input[name='itemId']").val(itemId).trigger('change');
			}else{

				getMaxId();
			}
		},

		bindItemFormValidation : function(){

			var form = $('#formAddItem');
	        var error = $('.alert-danger', form);
	        var success = $('.alert-success', form);

	        form.validate({

	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",  // validate all fields including form hidden input
	            rules: {
	                itemId: {
	                    min: 1,
	                    required: true
	                },
	                catid: {
	                    required: true
	                },
	                item_des: {
	                    minlength: 3,
	                    required: true
	                },
	                short_code: {
	                	required: true
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	                success.hide();
	                error.show();
	                App.scrollTo(error, -200);
	            },

	            errorPlacement: function (error, element) { // render error placement for each input type

	                var icon = $(element).parent('.input-icon').children('i');
	                icon.removeClass('fa-check').addClass("fa-warning");  
	                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	                if (element.attr("data-error-container")) { 

	                    error.appendTo(element.attr("data-error-container"));
	                }else{

	                    error.insertAfter(element);
	                }
	            },

	            highlight: function (element) { // hightlight error inputs
	                
	                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                
	            },

	            success: function (label, element) {

	                var icon = $(element).parent('.input-icon').children('i');
	                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	                icon.removeClass("fa-warning").addClass("fa-check");
	            },

	            submitHandler: function (form) {

	                success.show();
	                error.hide();
	                form[0].submit(); // submit the form
	            }
	        });
		},

		bindCategoryFormValidation : function(){

			var form = $('#formAddCategory');
	        var error = $('.alert-danger', form);
	        var success = $('.alert-success', form);

	        form.validate({

	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",  // validate all fields including form hidden input
	            rules: {
	                name: {
	                    minlength: 3,
	                    required: true
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	                success.hide();
	                error.show();
	                App.scrollTo(error, -200);
	            },

	            errorPlacement: function (error, element) { // render error placement for each input type

	                var icon = $(element).parent('.input-icon').children('i');
	                icon.removeClass('fa-check').addClass("fa-warning");  
	                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	                if (element.attr("data-error-container")) { 

	                    error.appendTo(element.attr("data-error-container"));
	                }else{

	                    error.insertAfter(element);
	                }
	            },

	            highlight: function (element) { // hightlight error inputs
	                
	                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                
	            },

	            success: function (label, element) {

	                var icon = $(element).parent('.input-icon').children('i');
	                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	                icon.removeClass("fa-warning").addClass("fa-check");
	            },

	            submitHandler: function (form) {

	                success.show();
	                error.hide();
	                form[0].submit(); // submit the form
	            }
	        });
		},

		bindSubCategoryFormValidation : function(){

			var form = $('#formAddSubCategory');
	        var error = $('.alert-danger', form);
	        var success = $('.alert-success', form);

	        form.validate({

	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",  // validate all fields including form hidden input
	            rules: {
	                name: {
	                    minlength: 2,
	                    required: true
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	                success.hide();
	                error.show();
	                App.scrollTo(error, -200);
	            },

	            errorPlacement: function (error, element) { // render error placement for each input type

	                var icon = $(element).parent('.input-icon').children('i');
	                icon.removeClass('fa-check').addClass("fa-warning");  
	                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	                if (element.attr("data-error-container")) { 

	                    error.appendTo(element.attr("data-error-container"));
	                }else{

	                    error.insertAfter(element);
	                }
	            },

	            highlight: function (element) { // hightlight error inputs
	                
	                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                
	            },

	            success: function (label, element) {

	                var icon = $(element).parent('.input-icon').children('i');
	                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	                icon.removeClass("fa-warning").addClass("fa-check");
	            },

	            submitHandler: function (form) {

	                success.show();
	                error.hide();
	                form[0].submit(); // submit the form
	            }
	        });
		},

		bindBrandFormValidation : function(){

			var form = $('#formAddBrand');
	        var error = $('.alert-danger', form);
	        var success = $('.alert-success', form);

	        form.validate({

	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",  // validate all fields including form hidden input
	            rules: {
	                name: {
	                    minlength: 2,
	                    required: true
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	                success.hide();
	                error.show();
	                App.scrollTo(error, -200);
	            },

	            errorPlacement: function (error, element) { // render error placement for each input type

	                var icon = $(element).parent('.input-icon').children('i');
	                icon.removeClass('fa-check').addClass("fa-warning");  
	                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	                if (element.attr("data-error-container")) { 

	                    error.appendTo(element.attr("data-error-container"));
	                }else{

	                    error.insertAfter(element);
	                }
	            },

	            highlight: function (element) { // hightlight error inputs
	                
	                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                
	            },

	            success: function (label, element) {

	                var icon = $(element).parent('.input-icon').children('i');
	                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	                icon.removeClass("fa-warning").addClass("fa-check");
	            },

	            submitHandler: function (form) {

	                success.show();
	                error.hide();
	                form[0].submit(); // submit the form
	            }
	        });
		},

		bindMadeFormValidation : function(){

			var form = $('#formAddMade');
	        var error = $('.alert-danger', form);
	        var success = $('.alert-success', form);

	        form.validate({

	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",  // validate all fields including form hidden input
	            rules: {
	                name: {
	                    minlength: 2,
	                    required: true
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	                success.hide();
	                error.show();
	                App.scrollTo(error, -200);
	            },

	            errorPlacement: function (error, element) { // render error placement for each input type

	                var icon = $(element).parent('.input-icon').children('i');
	                icon.removeClass('fa-check').addClass("fa-warning");  
	                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	                if (element.attr("data-error-container")) { 

	                    error.appendTo(element.attr("data-error-container"));
	                }else{

	                    error.insertAfter(element);
	                }
	            },

	            highlight: function (element) { // hightlight error inputs
	                
	                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                
	            },

	            success: function (label, element) {

	                var icon = $(element).parent('.input-icon').children('i');
	                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	                icon.removeClass("fa-warning").addClass("fa-check");
	            },

	            submitHandler: function (form) {

	                success.show();
	                error.hide();
	                form[0].submit(); // submit the form
	            }
	        });
		}
	};
};

var item = new Item();
item.init();