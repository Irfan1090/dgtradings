var SubCategory = function() {

	var getMaxId = function() {

		$.ajax({

			url : baseUrl + 'subCategories/getMaxId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$('#subCatId').val(data);
				$('#maxSubCatIdHidden').val(data);
				$('#subCatIdHidden').val(data);
			}, error : function(xhr, status, error) {

				console.log(xhr.responseText);
			}
		});
	}

	return {

		init : function() {

			this.bindUI();
			this.bindSubCategoryFormValidation();
		},

		bindUI : function() {

			var self = this;
			$('#VoucherTypeHidden').val('new');

			shortcut.add("F10", function() {
    			
    			$('.btnSave').trigger('click');
			});

			shortcut.add("F11", function() {
    			
    			$('.btnSaveAndMore').trigger('click');
			});

			shortcut.add("F5", function() {

    			$('.btnReset').trigger('click');
			});

			shortcut.add("F2", function() {

    			common.redirectUrl($('.btnBack').data('url'));
			});

			// when save button is clicked
			$('.btnSave, .btnSave2').on('click', function(e) {

				e.preventDefault();
		        if($('#formAddSubCategory').valid()) {
		            
		            self.initSave('save');
		        }
			});

			$('.btnSaveAndMore, .btnSaveAndMore2').on('click', function(e) {

				e.preventDefault();
		        if($('#formAddSubCategory').valid()) {
		            
		            self.initSave('SaveAndMore');
		        }
			});

			$('#subCatId').on('change', function() {
				
				var subCatId = $(this).val();
				obj = {};
				obj['url'] = 'subCategories/fetch';
				obj['data'] = {'sub_cat_id' : subCatId};
				obj['form'] = 'formAddSubCategory';
				obj['maxId'] = 'subCatIdHidden';
				obj['maxHidId'] = 'maxSubCatIdHidden';

				customForm.fetch(obj);
			});

			// when text is chenged inside the id textbox
			$('#subCatId').on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $(this).val().trim() !== "" ) {

						var subCatId = $(this).val();
						obj = {};
						obj['url'] = 'subCategories/fetch';
						obj['data'] = {'sub_cat_id' : subCatId};
						obj['form'] = 'formAddSubCategory';
						obj['maxId'] = 'subCatIdHidden';
						obj['maxHidId'] = 'maxSubCatIdHidden';

						customForm.fetch(obj);
					}
				}
			});

			$('.btnReset, .btnReset2').on('click', function(e){

				e.preventDefault();
				customForm.resetForm({'form' : 'formAddSubCategory'});
				getMaxId();
			});

			self.fetchRequestedForm();
		},

		// makes the voucher ready to save
		initSave : function(saveType) {

			var subcategoryObj = customForm.getSaveObj({'form' : 'formAddSubCategory'});	// returns the account detail object to save into database
			
			var obj = {};
			obj['url'] = 'subCategories/save';
			obj['data'] = { 'sub_category_detail' : subcategoryObj, 'voucher_type_hidden' : $('#voucherTypeHidden').val()};
			obj['saveType'] = saveType;
			obj['form'] = 'formAddSubCategory';
			obj['attr'] = 'Subcategory';
			obj['redirect'] = 'subCategories';
			customForm.saveForm(obj);		// saves the detail into the database				
		},

		fetchRequestedForm : function () {

			var subCatId = common.getQueryStringVal('id');
			if (subCatId && !isNaN(subCatId) ) {

				$("input[name='subCatId']").val(subCatId).trigger('change');
			}else{

				getMaxId();
			}
		},

		bindSubCategoryFormValidation : function(){

			var form = $('#formAddSubCategory');
	        var error = $('.alert-danger', form);
	        var success = $('.alert-success', form);

	        form.validate({

	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",  // validate all fields including form hidden input
	            rules: {
	                subCatId: {
	                    min: 1,
	                    required: true
	                },
	                name: {
	                    minlength: 2,
	                    required: true
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	                success.hide();
	                error.show();
	                App.scrollTo(error, -200);
	            },

	            errorPlacement: function (error, element) { // render error placement for each input type

	                var icon = $(element).parent('.input-icon').children('i');
	                icon.removeClass('fa-check').addClass("fa-warning");  
	                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	                if (element.attr("data-error-container")) { 

	                    error.appendTo(element.attr("data-error-container"));
	                }else{

	                    error.insertAfter(element);
	                }
	            },

	            highlight: function (element) { // hightlight error inputs
	                
	                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                
	            },

	            success: function (label, element) {

	                var icon = $(element).parent('.input-icon').children('i');
	                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	                icon.removeClass("fa-warning").addClass("fa-check");
	            },

	            submitHandler: function (form) {

	                success.show();
	                error.hide();
	                form[0].submit(); // submit the form
	            }
	        });
		}
	};
};

var subcategory = new SubCategory();
subcategory.init();