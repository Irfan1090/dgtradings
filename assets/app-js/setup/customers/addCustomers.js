var Customers = function() {

	var getMaxId = function() {

		$.ajax({

			url : baseUrl + 'customers/getMaxId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$('#customersId').val(data);
				$('#maxCustomersIdHidden').val(data);
				$('#customersIdHidden').val(data);
			}, error : function(xhr, status, error) {

				console.log(xhr.responseText);
			}
		});
	}

	return {

		init : function() {

			this.bindUI();
			this.bindCustomersFormValidation();
		},

		bindUI : function() {

			var self = this;
			$('#VoucherTypeHidden').val('new');

			shortcut.add("F10", function() {
    			
    			$('.btnSave').trigger('click');
			});

			shortcut.add("F11", function() {
    			
    			$('.btnSaveAndMore').trigger('click');
			});

			shortcut.add("F1", function() {

				$('a[href="#customersLookup"]').trigger('click');
			});

			shortcut.add("F5", function() {

    			$('.btnReset').trigger('click');
			});

			shortcut.add("F2", function() {

    			common.redirectUrl($('.btnBack').data('url'));
			});

			// when save button is clicked
			$('.btnSave, .btnSave2').on('click', function(e) {

				e.preventDefault();
				$("a[href='#basicInfo']").trigger('click');
		        if($('#formAddCustomers').valid()) {
		            
		            self.initSave('save');
		        }
			});

			$('.btnSaveAndMore, .btnSaveAndMore2').on('click', function(e) {

				e.preventDefault();
				$("a[href='#basicInfo']").trigger('click');
		        if($('#formAddCustomers').valid()) {
		            
		            self.initSave('SaveAndMore');
		        }
			});

			// when addMoreInf is clicked
			$('.btnNext').on('click', function(e) {

				if($('#formAddCustomers').valid()) {
		           	
		           	e.preventDefault(); 
		           	$('.alert-danger').hide();
		            $("a[href='#detailInfo']").trigger('click');
					$('.btnNext').hide();
					$('.btnPrev').show();
		        }
			});

			$('.btnPrev').on('click', function(e) {

				e.preventDefault();
				$("a[href='#basicInfo']").trigger('click');
				$('.btnNext').show();
				$('.btnPrev').hide();
			});

			$('a[href="#basicInfo"]').on('click',function(e){
			  	
			  	e.preventDefault();
			  	$('.btnNext').show();
				$('.btnPrev').hide();
			});

			$('a[href="#detailInfo"]').on('click',function(e){
			  	
			  	e.preventDefault();
			  	$('.btnNext').hide();
				$('.btnPrev').show();
			});

			$('#customersId').on('change', function() {
				
				var pid = $(this).val();
				obj = {};
				obj['url'] = 'customers/fetch';
				obj['data'] = {'spid' : pid};
				obj['form'] = 'formAddCustomers';
				obj['maxId'] = 'customersIdHidden';
				obj['maxHidId'] = 'maxCustomersIdHidden';

				customForm.fetch(obj);
			});

			// when text is chenged inside the id textbox
			$('#customersId').on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $(this).val().trim() !== "" ) {

						var pid = $(this).val();
						obj = {};
						obj['url'] = 'customers/fetch';
						obj['data'] = {'spid' : pid};
						obj['form'] = 'formAddCustomers';
						obj['maxId'] = 'customersIdHidden';
						obj['maxHidId'] = 'maxCustomersIdHidden';

						customForm.fetch(obj);
					}
				}
			});

			$('.btnReset, .btnReset2').on('click', function(e){

				e.preventDefault();
				customForm.resetForm({'form' : 'formAddCustomers'});
				getMaxId();

				$('a[href="#basicInfo"]').trigger('click');
			});

			// when edit button is clicked inside the table view
			$('#customersModalTable').on('click', '.btnCustomersEdit', function(e) {
				
				e.preventDefault();
		
				var pid = $(this).data('spid');
				obj = {};
				obj['url'] = 'customers/fetch';
				obj['data'] = {'spid' : spid,'etype':'customers'};
				obj['form'] = 'formAddCustomers';

				customForm.fetch(obj);
				$('#customersId').val(pid);
				$('a[href="#basicInfo"]').trigger('click');
			});

			self.fetchRequestedForm();
		},

		// makes the voucher ready to save
		initSave : function(saveType) {

			var customersObj = customForm.getSaveObj({'form' : 'formAddCustomers'});	// returns the account detail object to save into database
			
			var obj = {};
			obj['url'] = 'customers/save';
			obj['data'] = { 'customers_detail' : customersObj, 'voucher_type_hidden' : $('#voucherTypeHidden').val()};
			obj['saveType'] = saveType;
			obj['form'] = 'formAddCustomers';
			obj['attr'] = 'Customers';
			obj['redirect'] = 'customers';
			customForm.saveForm(obj);		// saves the detail into the database				
		},

		fetchRequestedForm : function () {

			var customersId = common.getQueryStringVal('id');
			if (customersId && !isNaN(customersId) ) {

				$("input[name='customersId']").val(customersId).trigger('change');
			}else{

				getMaxId();
			}
		},

		bindCustomersFormValidation : function(){

			var form = $('#formAddCustomers');
	        var error = $('.alert-danger', form);
	        var success = $('.alert-success', form);

	        form.validate({

	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",  // validate all fields including form hidden input
	            rules: {
	                accountId: {
	                    min: 1,
	                    required: true
	                },
	                name: {
	                    minlength: 2,
	                    required: true
	                }
	         
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	                success.hide();
	                error.show();
	                App.scrollTo(error, -200);
	            },

	            errorPlacement: function (error, element) { // render error placement for each input type

	                var icon = $(element).parent('.input-icon').children('i');
	                icon.removeClass('fa-check').addClass("fa-warning");  
	                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	                if (element.attr("data-error-container")) { 

	                    error.appendTo(element.attr("data-error-container"));
	                }else{

	                    error.insertAfter(element);
	                }
	            },

	            highlight: function (element) { // hightlight error inputs
	                
	                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                
	            },

	            success: function (label, element) {

	                var icon = $(element).parent('.input-icon').children('i');
	                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	                icon.removeClass("fa-warning").addClass("fa-check");
	            },

	            submitHandler: function (form) {

	                success.show();
	                error.hide();
	                form[0].submit(); // submit the form
	            }
	        });
		}
	};
};

var customers = new Customers();
customers.init();