var CustomerType = function() {
	var controller = 'customertype';
	var formname = 'formAddCustomerType';
	var formid = '#formAddCustomerType';
	var getMaxId = function() {

		$.ajax({

			url : baseUrl + controller + '/getMaxId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$('#customertypeId').val(data);
				$('#maxCustomerTypedHidden').val(data);
				$('#customertypeIdHidden').val(data);
			}, error : function(xhr, status, error) {

				console.log(xhr.responseText);
			}
		});
	}

	return {

		init : function() {

			this.bindUI();
			this.bindcustomerTypeFormValidation();
		},

		bindUI : function() {

			var self = this;
			$('#VoucherTypeHidden').val('new');

			shortcut.add("F10", function() {
    			
    			$('.btnSave').trigger('click');
			});

			shortcut.add("F11", function() {
    			
    			$('.btnSaveAndMore').trigger('click');
			});

			shortcut.add("F5", function() {

    			$('.btnReset').trigger('click');
			});

			shortcut.add("F2", function() {

    			common.redirectUrl($('.btnBack').data('url'));
			});

			// when save button is clicked
			$('.btnSave, .btnSave2').on('click', function(e) {

				e.preventDefault();
		        if($(formid).valid()) {
		            
		            self.initSave('save');
		        }
			});

			$('.btnSaveAndMore, .btnSaveAndMore2').on('click', function(e) {

				e.preventDefault();
		        if($(formid).valid()) {
		            
		            self.initSave('SaveAndMore');
		        }
			});

			$('#customertypeId').on('change', function() {
				
				var customertypeId = $(this).val();
				obj = {};
				obj['url'] = controller+'/fetch';
				obj['data'] = {'ctypeid' : customertypeId};
				obj['form'] = formname;
				obj['maxId'] = 'customertypeIdHidden';
				obj['maxHidId'] = 'maxLevel1IdHidden';

				customForm.fetch(obj);
			});

			// when text is chenged inside the id textbox
			$('#customertypeId').on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $(this).val().trim() !== "" ) {

						var ctypeid = $(this).val();
						obj = {};
						obj['url'] = controller+'/fetch';
						obj['data'] = {'ctypeid' : ctypeid};
						obj['form'] = formname;
						obj['maxId'] = 'customertypeIdHidden';
						obj['maxHidId'] = 'maxcustomertypeIdHidden';

						customForm.fetch(obj);
					}
				}
			});

			$('.btnReset, .btnReset2').on('click', function(e){

				e.preventDefault();
				customForm.resetForm({'form' : formname});
				getMaxId();
			});

			self.fetchRequestedForm();
		},

		// makes the voucher ready to save
		initSave : function(saveType) {

			var customertypeObj = customForm.getSaveObj({'form' : formname});	// returns the account detail object to save into database
			
			var obj = {};
			obj['url'] = controller + '/save';
			obj['data'] = { 'customertype_detail' : customertypeObj, 'voucher_type_hidden' : $('#voucherTypeHidden').val()};
			obj['saveType'] = saveType;
			obj['form'] = formname;
			obj['attr'] = 'Customer Type';
			obj['redirect'] = controller;
			customForm.saveForm(obj);		// saves the detail into the database				
		},

		fetchRequestedForm : function () {
			var level1Id = common.getQueryStringVal('id');
			if (level1Id && !isNaN(level1Id) ) {

				$("input[name='customertypeId']").val(level1Id).trigger('change');
			}else{

				getMaxId();
			}
		},

		bindcustomerTypeFormValidation : function(){

			var form = $(formid);
			console.log(formid);
	        var error = $('.alert-danger', form);
	        var success = $('.alert-success', form);

	        form.validate({

	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",  // validate all fields including form hidden input
	            rules: {
	                level1Id: {
	                    min: 1,
	                    required: true
	                },
	                name: {
	                    minlength: 2,
	                    required: true
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	                success.hide();
	                error.show();
	                App.scrollTo(error, -200);
	            },

	            errorPlacement: function (error, element) { // render error placement for each input type

	                var icon = $(element).parent('.input-icon').children('i');
	                icon.removeClass('fa-check').addClass("fa-warning");  
	                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	                if (element.attr("data-error-container")) { 

	                    error.appendTo(element.attr("data-error-container"));
	                }else{

	                    error.insertAfter(element);
	                }
	            },

	            highlight: function (element) { // hightlight error inputs
	                
	                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                
	            },

	            success: function (label, element) {

	                var icon = $(element).parent('.input-icon').children('i');
	                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	                icon.removeClass("fa-warning").addClass("fa-check");
	            },

	            submitHandler: function (form) {

	                success.show();
	                error.hide();
	                form[0].submit(); // submit the form
	            }
	        });
		}
	};
};

var customertype = new CustomerType();
customertype.init();