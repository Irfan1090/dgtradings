var GlobalSettings = function() {

    return {

        init : function() {

            this.bindUI();
        },

        bindUI : function() {

            var self = this;
            $('#VoucherTypeHidden').val('new');

            shortcut.add("F10", function() {
                
                $('.btnSave').trigger('click');
            });

            shortcut.add("F11", function() {
                
                $('.btnSaveAndMore').trigger('click');
            });

            shortcut.add("F5", function() {

                $('.btnReset').trigger('click');
            });

            shortcut.add("F2", function() {

                common.redirectUrl($('.btnBack').data('url'));
            });

            // when save button is clicked
            $('.btnSave, .btnSave2').on('click', function(e) {

                e.preventDefault();
                $("a[href='#generalSettings']").trigger('click');
                self.initSave('save');
            });

            $('.btnReset, .btnReset2').on('click', function(e){

                e.preventDefault();
                customForm.resetForm({'form' : 'formAddGlobalSettings'});
            });

            $('.btnClearSettings').on('click', function(){

                $('select[name="item_f_name"]').val('').trigger('change');
                $('select[name="item_m_name"]').val('').trigger('change');
                $('select[name="item_l_name"]').val('').trigger('change');
            });
        },

        // makes the voucher ready to save
        initSave : function(saveType) {

            var GlobalSettingsObj = customForm.getSaveObj({'form' : 'formAddGlobalSettings'});  // returns the account detail object to save into database
            
            var obj = {};
            obj['url'] = 'globalSettings/save';
            obj['data'] = { 'global_settings_detail' : GlobalSettingsObj};
            obj['saveType'] = saveType;
            obj['form'] = 'formAddGlobalSettings';
            obj['attr'] = 'Global settings';
            obj['redirect'] = 'globalSettings';
            customForm.saveForm(obj);       // saves the detail into the database               
        }
    };
};

var globalSettings = new GlobalSettings();
globalSettings.init();