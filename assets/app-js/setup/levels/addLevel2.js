var Level2 = function() {

	var getMaxId = function() {

		$.ajax({

			url : baseUrl + 'level2/getMaxId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$('#level2Id').val(data);
				$('#maxLevel2IdHidden').val(data);
				$('#level2IdHidden').val(data);
			}, error : function(xhr, status, error) {

				console.log(xhr.responseText);
			}
		});
	}

	return {

		init : function() {

			this.bindUI();
			this.bindLevel2FormValidation();
		},

		bindUI : function() {

			var self = this;
			$('#VoucherTypeHidden').val('new');

			shortcut.add("F10", function() {
    			
    			$('.btnSave').trigger('click');
			});

			shortcut.add("F11", function() {
    			
    			$('.btnSaveAndMore').trigger('click');
			});

			shortcut.add("F5", function() {

    			$('.btnReset').trigger('click');
			});

			shortcut.add("F2", function() {

    			common.redirectUrl($('.btnBack').data('url'));
			});

			// when save button is clicked
			$('.btnSave, .btnSave2').on('click', function(e) {

				e.preventDefault();
		        if($('#formAddLevel2').valid()) {
		            
		            self.initSave('save');
		        }
			});

			$('.btnSaveAndMore, .btnSaveAndMore2').on('click', function(e) {

				e.preventDefault();
		        if($('#formAddLevel2').valid()) {
		            
		            self.initSave('SaveAndMore');
		        }
			});

			$('#level2Id').on('change', function() {
				
				var level2Id = $(this).val();
				obj = {};
				obj['url'] = 'level2/fetch';
				obj['data'] = {'l2' : level2Id};
				obj['form'] = 'formAddLevel2';
				obj['maxId'] = 'level2IdHidden';
				obj['maxHidId'] = 'maxLevel2IdHidden';

				customForm.fetch(obj);
			});

			// when text is chenged inside the id textbox
			$('#level2Id').on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $(this).val().trim() !== "" ) {

						var level2Id = $(this).val();
						obj = {};
						obj['url'] = 'level2/fetch';
						obj['data'] = {'l2' : level2Id};
						obj['form'] = 'formAddLevel2';
						obj['maxId'] = 'level2IdHidden';
						obj['maxHidId'] = 'maxLevel2IdHidden';

						customForm.fetch(obj);
					}
				}
			});

			$('.btnReset, .btnReset2').on('click', function(e){

				e.preventDefault();
				customForm.resetForm({'form' : 'formAddLevel2'});
				getMaxId();
			});

			self.fetchRequestedForm();
		},

		// makes the voucher ready to save
		initSave : function(saveType) {

			var level2Obj = customForm.getSaveObj({'form' : 'formAddLevel2'});	// returns the account detail object to save into database
			
			var obj = {};
			obj['url'] = 'level2/save';
			obj['data'] = { 'level2_detail' : level2Obj, 'voucher_type_hidden' : $('#voucherTypeHidden').val()};
			obj['saveType'] = saveType;
			obj['form'] = 'formAddLevel2';
			obj['attr'] = 'Level2';
			obj['redirect'] = 'level2';
			customForm.saveForm(obj);		// saves the detail into the database				
		},

		fetchRequestedForm : function () {

			var level2Id = common.getQueryStringVal('id');
			if (level2Id && !isNaN(level2Id) ) {

				$("input[name='level2Id']").val(level2Id).trigger('change');
			}else{

				getMaxId();
			}
		},

		bindLevel2FormValidation : function(){

			var form = $('#formAddLevel2');
	        var error = $('.alert-danger', form);
	        var success = $('.alert-success', form);

	        form.validate({

	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",  // validate all fields including form hidden input
	            rules: {
	                level2Id: {
	                    min: 1,
	                    required: true
	                },
	                l1: {
	                    required: true
	                },
	                name: {
	                    minlength: 2,
	                    required: true
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	                success.hide();
	                error.show();
	                App.scrollTo(error, -200);
	            },

	            errorPlacement: function (error, element) { // render error placement for each input type

	                var icon = $(element).parent('.input-icon').children('i');
	                icon.removeClass('fa-check').addClass("fa-warning");  
	                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	                if (element.attr("data-error-container")) { 

	                    error.appendTo(element.attr("data-error-container"));
	                }else{

	                    error.insertAfter(element);
	                }
	            },

	            highlight: function (element) { // hightlight error inputs
	                
	                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                
	            },

	            success: function (label, element) {

	                var icon = $(element).parent('.input-icon').children('i');
	                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	                icon.removeClass("fa-warning").addClass("fa-check");
	            },

	            submitHandler: function (form) {

	                success.show();
	                error.hide();
	                form[0].submit(); // submit the form
	            }
	        });
		}
	};
};

var level2 = new Level2();
level2.init();