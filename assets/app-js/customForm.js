var customForm = {

    init    : function (){

    },

    bindUI  : function (){

    },

    removeErrors : function(form){

        $('#'+form+' .alert-danger').hide();
        $('#'+form+' .alert-success').hide();

        $('#'+form+' .error-message-body').text('');
        $('#'+form+' .error-message').removeClass('alert-danger');

        $('#'+form+' .success-message-body').text('');
        $('#'+form+' .success-message').removeClass('alert-success');
    },

    getSaveObj : function (obj) {
      
        var formObj = {},name='';

        $('#'+obj.form+' .saveColumn').each(function(index, elem){

            if($(elem).attr('name') == 'active'){

                formObj[$(elem).attr('name')] = ($('#status').bootstrapSwitch('state') === true) ? '1' : '0';
            }
            if($(elem).prop('type') == 'radio')
            {   
              
                if($(elem).prop('checked')==true)
                {
                       
                    formObj[$(elem).attr('name')] = $(elem).attr('saveattr'); // saveattr custom attribute for radio buttons.............
                }
            }

            if($(elem).val() != null && $(elem).val() != ''){

                formObj[$(elem).attr('name')] = $.trim($(elem).val());
            }
           
        });
        return formObj;
    },

    // saves the data into the database
    saveForm : function(obj) {
        // console.log(obj);
        // throw new Error('sfds');
        customForm.removeErrors(obj.form);
        if(obj.form == 'formAddItem' || obj.form == 'formAddModalItem'){

            var form_data = new FormData();
            for ( var key in obj.data['item_detail'] ) {

                form_data.append(key, obj.data['item_detail'][key]);
            }

            if(obj.form == 'formAddItem'){

                form_data.append("photo", $('#photos').get(0).files[0]);    
            }
            
            form_data.append('voucher_type_hidden', $('#voucherTypeHidden').val());
            obj.data = form_data;
            $.ajax({
                url : baseUrl + obj.url,
                type : 'POST',
                data : obj.data,
                dataType : 'JSON',
                processData: false,
                contentType : false,
                success : function(data) {

                    if (!data) {
                        
                        $('#'+obj.form+' .error-message-body').text('An internal error occured while saving data. Please try again.');
                        $('#'+obj.form+' .error-message').addClass('alert-danger');
                        $('#'+obj.form+' .error-message').show();
                    } else if(data == 'duplicate_name'){

                        $('#'+obj.form+' .error-message-body').text(obj.attr+' name already exist.');
                        $('#'+obj.form+' .error-message').addClass('alert-danger');
                        $('#'+obj.form+' .error-message').show();
                    }else if(data == 'duplicateshortcode'){

                        $('#'+obj.form+' .error-message-body').text('Short code already exist.');
                        $('#'+obj.form+' .error-message').addClass('alert-danger');
                        $('#'+obj.form+' .error-message').show();
                    }else{

                        if(obj.saveType == 'modal'){

                            $('#'+obj.form+' .success-message-body').text(obj.attr+' saved successfully.');
                            $('#'+obj.form+' .success-message').addClass('alert-success');
                            $('#'+obj.form+' .success-message').show();
                            setTimeout(function(){

                                $('#'+obj.modal).modal('hide');
                                var name = $("#"+obj.form+" input[name='"+obj.appendInputName+"']").val();
                                var id = data;
                                $("#"+obj.appendForm+" select[name='"+obj.appendSelectName+"']").append('<option selected value="'+id+'">'+name+'</option>');
                                customForm.resetForm({'form' : obj.form});
                            }, 1000);
                        }else if(obj.saveType == 'save'){

                            $('#'+obj.form+' .success-message-body').text(obj.attr+' saved successfully.');
                            $('#'+obj.form+' .success-message').addClass('alert-success');
                            $('#'+obj.form+' .success-message').show();
                            setTimeout(function(){

                                common.redirectUrl(obj.redirect);
                            }, 2000);
                        }else{

                            $('#'+obj.form+' .success-message-body').text(obj.attr+' saved successfully.');
                            $('#'+obj.form+' .success-message').addClass('alert-success');
                            $('#'+obj.form+' .success-message').show();
                            setTimeout(function(){

                                common.reloadForm();
                            }, 2000);
                        }
                    }
                }, error : function(xhr, status, error) {

                    console.log(xhr.responseText);
                }
            });
        }else{

            $.ajax({
                url : baseUrl + obj.url,
                type : 'POST',
                data : obj.data,
                dataType : 'JSON',
                beforeSend:function(data){
                   console.log(obj.data);
                },
                success : function(data) {

                    if (!data) {
                        
                        $('#'+obj.form+' .error-message-body').text('An internal error occured while saving data. Please try again.');
                        $('#'+obj.form+' .error-message').addClass('alert-danger');
                        $('#'+obj.form+' .error-message').show();
                    } else if(data == 'duplicate_name'){

                        $('#'+obj.form+' .error-message-body').text(obj.attr+' name already exist.');
                        $('#'+obj.form+' .error-message').addClass('alert-danger');
                        $('#'+obj.form+' .error-message').show();
                    }else{

                        if(obj.saveType == 'modal'){

                            $('#'+obj.form+' .success-message-body').text(obj.attr+' saved successfully.');
                            $('#'+obj.form+' .success-message').addClass('alert-success');
                            $('#'+obj.form+' .success-message').show();
                            setTimeout(function(){

                                $('#'+obj.modal).modal('hide');
                                var name = $("#"+obj.form+" input[name='"+obj.appendInputName+"']").val();
                                var id = data;
                                $("#"+obj.appendForm+" select[name='"+obj.appendSelectName+"']").append('<option selected value="'+id+'">'+name+'</option>');
                                customForm.resetForm({'form' : obj.form});
                            }, 1000);
                        }else if(obj.saveType == 'save'){

                            $('#'+obj.form+' .success-message-body').text(obj.attr+' saved successfully.');
                            $('#'+obj.form+' .success-message').addClass('alert-success');
                            $('#'+obj.form+' .success-message').show();
                            setTimeout(function(){

                                common.redirectUrl(obj.redirect);
                            }, 2000);
                        }else{

                            $('#'+obj.form+' .success-message-body').text(obj.attr+' saved successfully.');
                            $('#'+obj.form+' .success-message').addClass('alert-success');
                            $('#'+obj.form+' .success-message').show();
                            setTimeout(function(){

                                common.reloadForm();
                            }, 2000);
                        }
                    }
                }, error : function(xhr, status, error) {

                    console.log(xhr.responseText);
                }
            });
        }
    },

    fetch : function(obj,mine=undefined) {

        customForm.removeErrors(obj.form);
        $('#'+obj.form+' .saveColumn').val('');
        $('#'+obj.form+' #voucherTypeHidden').val('new');
        $('#'+obj.form+' .select2').val('').trigger('change');
        $('#'+obj.form+' #'+obj.maxId).val($('#'+obj.form+' #'+obj.maxHidId).val());
        $.ajax({
            url : baseUrl + obj.url,
            type : 'POST',
            data : obj.data,
            dataType : 'JSON',
            success : function(data) {

                if (!data) {
                    
                    $('#'+obj.form+' .error-message-body').text('An internal error occured while fetching data. Please try again.');
                    $('#'+obj.form+' .error-message').addClass('alert-danger');
                    $('#'+obj.form+' .error-message').show();
                } else{
                    if(mine===undefined)
                    {
                        customForm.populateData(data, obj.form);
                    }
                    else{
                        customForm.populateData(data, obj.form,mine);
                    }
                }
            }, error : function(xhr, status, error) {

                console.log(xhr.responseText);
            }
        });
    },

    populateData : function(data, form,mine=undefined) {
        console.log(form);
        $('#'+form+' #voucherTypeHidden').val('edit');
        $.each(data[0], function(index, value){

            if(form == 'formAdd'){

                if(index == 'level3'){

                    $("#"+form+" select[name='"+index+"']").select2('val', value);
                }else if(index == 'active'){

                    (value === "1") ? $("#"+form+" input[name='"+index+"']").bootstrapSwitch('state', true) : $("#"+form+" input[name='"+index+"']").bootstrapSwitch('state', false);         
                }else if(index == 'address' || index == 'uaddress'){

                    $("#"+form+" textarea[name='"+index+"']").val(value);
                }else{

                    $("#"+form+" input[name='"+index+"']").val(value);
                }   
            }else if(form == 'formAddItem'){
                
                if(index == 'catid' || index == 'subcatid' || index == 'bid' || index == 'made_id'){

                    $("#"+form+" select[name='"+index+"']").select2('val', value);
                }else if(index == 'active'){

                    (value === "1") ? $("#"+form+" input[name='"+index+"']").bootstrapSwitch('state', true) : $("#"+form+" input[name='"+index+"']").bootstrapSwitch('state', false);         
                }else if(index == 'photo'){

                    if(value && value != ''){

                        $('#imgUrl').attr("src", baseUrl+"assets/uploads/items/"+value);
                    }else{
                        $('#imgUrl').attr("src", baseUrl+"assets/uploads/items/no-image.png");
                    }
                    
                }else{

                    $("#"+form+" input[name='"+index+"']").val(value);
                }   
            }
            else if(form == 'formAddCustomers' || form == 'formAddBanks' || form =='formAddCustomerType' || mine==='definitions'){
                
                var name,select2,amb,attr,attr2,attrname,index2,ambup,ine='';
                $('.fetch').each(function(inde,elem){
                    select2 = $(this).hasClass('select2');
                    ambup = '';
                    name = ((attr = $(elem).attr('ambigousname')) && typeof attr!=='undefined' && attr!==false)?( (  attr.indexOf(',')>0)?ambup = $.trim(attr.split('-as-')[1].split(',')[0]): $(elem).attr('name') ): $(elem).attr('name');
                    bootstrapSwitch = $(this).hasClass('make-switch');
                    if( $.trim(ambup)==$.trim(index))
                    {
                        attrname = 'ambigousname';
                        index2 = $(elem).attr('ambigousname');
                    }
                    else
                    {
                        attrname = 'name';
                        index2 = index;
                    }
                    // just dummy not permanent downside.......
                    if(index==='level3')
                    {
                        $('input[name="level3"]').val(2);
                    } // just dummy not permanent upside.......
                    if(name==index && select2===false && bootstrapSwitch==false)
                    {

                        if($(this).context.localName==='textarea')
                        {
                            $("#"+form+" textarea["+attrname+"='"+index2+"']").val(value);
                        }
                        else
                        {
                            $("#"+form+" input["+attrname+"='"+index2+"']").val(value);
                        }
                    }
                    else if(select2==true && name==index)
                    {
                        $("#"+form+" select["+attrname+"='"+index2+"']").select2('val', value);
                        $('#'+form+"select["+attrname+"='"+index2+"']").trigger('change');
                    }
                    else if(bootstrapSwitch==true && name==index)
                    {
                        (value === "1") ? $("#"+form+" input["+attrname+"='"+index2+"']").bootstrapSwitch('state', true) : $("#"+form+" input["+attrname+"='"+index2+"']").bootstrapSwitch('state', false);
                    }
                    else{

                    }
                })
                   
            
            }else if(form == 'formAddCategory' || form == 'formAddBrand' || form == 'formAddLevel1' || form == 'formAddMade' || form == 'formAddWarehouse'){

                if(index == 'description'){

                    $("#"+form+" textarea[name='"+index+"']").val(value);
                }else{

                    $("#"+form+" input[name='"+index+"']").val(value);
                }   
            }else if(form == 'formAddSubCategory' || form == 'formAddLevel2' || form == 'formAddLevel3'){

                if(index == 'description'){

                    $("#"+form+" textarea[name='"+index+"']").val(value);
                }else if(index == 'catid' || index == 'l1' || index == 'l2'){

                    $("#"+form+" select[name='"+index+"']").select2('val', value);
                }else{
                    $("#"+form+" input[name='"+index+"']").val(value);
                }   
            }
        });
    },

    resetForm : function(obj){

        var icon = $('#'+obj.form+' .input-icon').children('i');
        $('#'+obj.form+' .form-group').removeClass('has-error has-success');
        $('#'+obj.form+' .alert-danger').hide();
        $('#'+obj.form+' .alert-success').hide();
        icon.removeClass("fa-warning fa-check");
        
        $('#'+obj.form).validate().resetForm();

        $('#'+obj.form+' .select2').val('').trigger('change');
        $('#'+obj.form)[0].reset();
    }
}

$(function(){

    customForm.init();
});