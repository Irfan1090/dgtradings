var appendToTable = function(srno, barCode,itemDesc, itemId, qty, discP, disc, rate, amount, brandDesc, brandId, itemShortCode, itemBarCode) {

	var srno = $('#purchaseTable tbody tr').length + 1;
	var row = 	"<tr>" +
					"<td class='srno numeric text-left' data-title='Sr#' > "+ srno +"</td>" +
					"<td class='barCodes numeric text-right' data-title='Barcode' > "+ barCode +"</td>" +
					"<td class='saveDetailColumn hide' data-name='stid' data-value=''> "+  +"</td>" +
			 		"<td class='itemDesc saveDetailColumn' data-name='item_id' data-value='"+itemId+"' data-title='Description' data-item_id='"+ itemId +"' data-shortcode='"+ itemShortCode +"'> "+ itemDesc +"</td>" +
			 		"<td class='barCode hide numeric text-right' data-title='BarCode'>  "+ itemBarCode +"</td>" +
			 		"<td class='qty saveDetailColumn numeric text-right' data-name='qty' data-value="+qty+" data-title='Qty'>  "+ qty +"</td>" +
			 		"<td class='rate saveDetailColumn numeric text-right' data-name='rate' data-value="+rate+" data-title='Rate'> "+ rate +"</td>" +
			 		"<td class='discP saveDetailColumn numeric text-right' data-name='discount' data-value="+discP+" data-title='Dis%'>  "+ discP +"</td>" +
			 		"<td class='disc saveDetailColumn numeric text-right' data-name='damount' data-value="+disc+" data-title='Discount'>  "+ disc +"</td>" +
				 	"<td class='amount saveDetailColumn numeric text-right' data-name='amount' data-value="+amount+" data-title='Amount' > "+ amount +"</td>" +
				 	"<td class='netAmount saveDetailColumn hide' data-name='netamount' data-value="+amount+" data-title='Net Amount' > "+ amount +"</td>" +
				 	"<td class='barCodePrintNum numeric text-right' data-title='Sticker' ><input type='text' class='form-control num' value='"+ qty +"'></td>" +
				 	"<td><a href='' class='btn btn-outline blue btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-outline red btnRowRemove'><span class='fa fa-trash-o'></span></a></td>" +
				 	"<td class='brand text-right hide' data-brand_id='"+ brandId +"' data-title='brand' > "+ brandDesc +"</td>" +
			 	"</tr>";

	$(row).appendTo('#purchaseTable');
}

var calculateLowerTotal = function(qty, amount, discount) {

	var _qty = common.getNumText($('.totalQty'));
	var _disc = common.getNumText($('.totalDisc'));
	var _amnt = common.getNumText($('.totalAmount'));

	
	var _discount = common.getNumVal($('#discAmount'));
	var _taxAmount = common.getNumVal($('#taxAmount'));
	var _expense = common.getNumVal($('#expAmount'));

	var tempQty = parseFloat(_qty) + parseFloat(qty);
	$('.totalQty').text(tempQty);

	var tempdiscount = parseFloat(parseFloat(_disc) + parseFloat(discount)).toFixed(0);
	$('.totalDisc').text(tempdiscount);

	var tempAmnt = (parseFloat(_amnt) + parseFloat(amount)).toFixed(0);
	$('.totalAmount').text(tempAmnt);

	var net = (parseFloat(tempAmnt) - parseFloat(_discount) + parseFloat(_taxAmount) + parseFloat(_expense)).toFixed(0) ;
	$('#netAmount').val(net);
}

var Purchase = function() {

	// gets the max id of the voucher
	var getMaxVrno = function() {

		$.ajax({

			url : baseUrl + 'purchase/getMaxVrno',
			type : 'POST',
			data : {'company_id': $('#cId').val()},
			dataType : 'JSON',
			success : function(data) {

				$('#vrno').val(data);
				$('#maxVrnoHidden').val(data);
				$('#vrnoHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var getMaxVrnoa = function() {

		$.ajax({

			url : baseUrl + 'purchase/getMaxVrnoa',
			type : 'POST',
			data : {'company_id': $('#cId').val()},
			dataType : 'JSON',
			success : function(data) {

				$('#vrnoa').val(data);
				$('#maxVrnoaHidden').val(data);
				$('#vrnoaHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var validateSingleProductAdd = function() {

		var errorFlag = false;
		var item = $('#itemDropdown');
		var qty = $('#qty');
		var rate = $('#rate');

		$('.singleProduct').addClass('display-hide');
		item.closest('.form-group').removeClass("has-success").removeClass('has-error');
		qty.closest('.form-group').removeClass("has-success").removeClass('has-error');
		rate.closest('.form-group').removeClass("has-success").removeClass('has-error');
		
		if (!item.val()) {
			
			item.closest('.form-group').removeClass("has-success").addClass('has-error');
			errorFlag = true;
		}

		if (!qty.val()) {
		
			qty.closest('.form-group').removeClass("has-success").addClass('has-error');
			errorFlag = true;
		}

		if (!rate.val()) {

			rate.closest('.form-group').removeClass("has-success").addClass('has-error');
			errorFlag = true;
		}

		return errorFlag;
	}

	var calculateUpperValues = function() {

		var _qty = common.getNumVal($('#qty'));
		var _discount = common.getNumVal($('#disc'));
		var _rate = common.getNumVal($('#rate'));
		var _tempAmnt =  ((parseFloat(_rate)*parseFloat(_qty))-parseFloat(_discount)).toFixed(0);
		$('#amount').val(_tempAmnt);
	}

	var getSaveLedgerObj = function(desc) {

		var ledgers = [];
		
		///////////////////////////////////////////////////////////////
		//// for over all voucher
		///////////////////////////////////////////////////////////////
		
		var pledger = {};
		pledger.pledid = '';
		pledger.pid = $('#partyDropdown').val();
		pledger.description = desc + ', ' + $('#remarks').val();
		pledger.date = $('#currentDate').val();
		pledger.debit = 0;
		pledger.credit = $('#netAmount').val();
		pledger.dcno = $('#vrnoaHidden').val();
		pledger.invoice = $('#vrnoaHidden').val();
		pledger.etype = 'purchase';
		pledger.pid_key = $('#purchaseId').val();
		pledger.uid = $('#uId').val();
		pledger.company_id = $('#cId').val();
		pledger.isFinal = 0;
		ledgers.push(pledger);
 		
 		var purAmount= parseFloat($('.totalAmount').text()) + parseFloat($('.totalDisc').text());
		var pledger = {};
		pledger.pledid = '';
		pledger.pid = $('#purchaseId').val();
		pledger.description = $('#partyDropdown').find('option:selected').text() + ' ' + $('#remarks').val();
		pledger.date = $('#currentDate').val();
		pledger.debit = purAmount;
		// alert(purAmount);
		pledger.credit = 0;
		pledger.dcno = $('#vrnoaHidden').val();
		pledger.invoice = $('#invNo').val();
		pledger.etype = 'purchase';
		pledger.pid_key = $('#partyDropdown').val();
		pledger.uid = $('#uId').val();
		pledger.company_id = $('#cId').val();	
		pledger.isFinal = 0;
		ledgers.push(pledger);

		///////////////////////////////////////////////////////////////
		//// for Discount
		///////////////////////////////////////////////////////////////
		if ($('.totalDisc').text() != 0 ) {

			pledger = undefined;
			var pledger = {};
			pledger.etype = 'purchase';
			pledger.description = $('#partyDropdown option:selected').text() + '. ' + $('#remarks').val();
			pledger.dcno = $('#vrnoaHidden').val();
			pledger.invoice = $('#vrnoaHidden').val();
			pledger.pid = $('#itemDiscountId').val();
			pledger.date = $('#currentDate').val();
			pledger.debit = 0;
			pledger.credit = $('.totalDisc').text();
			pledger.isFinal = 0;
			pledger.uid = $('#uId').val();
			pledger.company_id = $('#cId').val();	
			pledger.pid_key = $('#partyDropdown').val();								

			ledgers.push(pledger);
		}

		if ($('#discAmount').val() != 0 ) {

			pledger = undefined;
			var pledger = {};
			pledger.etype = 'purchase';
			pledger.description = $('#partyDropdown option:selected').text() + '. ' + $('#remarks').val();
			pledger.dcno = $('#vrnoaHidden').val();
			pledger.invoice = $('#vrnoaHidden').val();
			pledger.pid = $('#discountId').val();
			pledger.date = $('#currentDate').val();
			pledger.debit = 0;
			pledger.credit = $('#discAmount').val();
			pledger.isFinal = 0;
			pledger.uid = $('#uId').val();
			pledger.company_id = $('#cId').val();	
			pledger.pid_key = $('#partyDropdown').val();								

			ledgers.push(pledger);
		}

		if ($('#taxAmount').val() != 0 ) {

			pledger = undefined;
			var pledger = {};
			pledger.etype = 'purchase';
			pledger.description = $('#partyDropdown option:selected').text() + '. ' + $('#remarks').val();
			pledger.dcno = $('#vrnoaHidden').val();
			pledger.invoice = $('#vrnoaHidden').val();
			pledger.pid = $('#taxId').val();
			pledger.date = $('#currentDate').val();
			pledger.debit = $('#taxAmount').val();
			pledger.credit = 0;
			pledger.isFinal = 0;
			pledger.uid = $('#uId').val();
			pledger.company_id = $('#cId').val();	
			pledger.pid_key = $('#partyDropdown').val();
			ledgers.push(pledger);
		}

		if ($('#expAmount').val() != 0 ) {

			pledger = undefined;
			var pledger = {};
			pledger.etype = 'purchase';
			pledger.description = $('#partyDropdown option:selected').text() + '. ' + $('#remarks').val();
			pledger.dcno = $('#vrnoaHidden').val();
			pledger.invoice = $('#vrnoaHidden').val();
			pledger.pid = $('#expenseId').val();
			pledger.date = $('#currentDate').val();
			pledger.debit = $('#expAmount').val();
			pledger.credit = 0;
			pledger.isFinal = 0;
			pledger.uid = $('#uId').val();
			pledger.company_id = $('#cId').val();	
			pledger.pid_key = $('#partyDropdown').val();
			ledgers.push(pledger);
		}

		if ($('#paid').val() != 0 ) {

			pledger = undefined;
			var pledger = {};
			pledger.etype = 'purchase';
			pledger.description = $('#partyDropdown option:selected').text() + '. ' + $('#remarks').val();
			pledger.dcno = $('#vrnoaHidden').val();
			pledger.invoice = $('#vrnoaHidden').val();
			pledger.pid = $('#cashId').val();
			pledger.date = $('#currentDate').val();
			pledger.debit = 0;
			pledger.credit = $('#paid').val();
			pledger.isFinal = 0;
			pledger.uid = $('#uId').val();
			pledger.company_id = $('#cId').val();	
			pledger.pid_key = $('#partyDropdown').val();
			ledgers.push(pledger);

			pledger = undefined;
			var pledger = {};
			pledger.etype = 'purchase';
			pledger.description =  'Cash Paid  ' + $('#remarks').val();
			pledger.dcno = $('#vrnoaHidden').val();
			pledger.invoice = $('#vrnoaHidden').val();
			pledger.pid = $('#partyDropdown').val();
			pledger.date = $('#currentDate').val();
			pledger.debit = $('#paid').val();
			pledger.credit = 0;
			pledger.isFinal = 0;
			pledger.uid = $('#uId').val();
			pledger.company_id = $('#cId').val();	
			pledger.pid_key = $('#cashId').val();
			ledgers.push(pledger);
		}

		return ledgers;
	}

	var getCrit = function(){

		var crit ='';
		var accId = $('#partyDropdown').val();
		var itemId = $('#itemIdDropdown').val();

		if (itemId !== null){

            crit +='AND i.item_id in (' + itemId +') ';
        }
        if (accId !== null) {
        
            crit +='AND m.party_id in (' + accId +') '
        }

        crit += 'AND m.stid <>0 ';

        return crit;
	}

	var lastFiveSRate = function(crit) {

		$("table .rateTbody tr").remove();
		$.ajax({

			url : baseUrl + 'purchase/lastFiveSRate',
			type : 'POST',
			data : { 'crit' : crit, 'company_id' : $('#cId').val(), 'etype' : 'purchase', 'date' : $('#current_date').val()},
			dataType : 'JSON',
			
			success : function(data) {
			
				if (data === 'false') {
				
					$('.lFiveRateDisp').addClass('disp');
				} else {
				
					$('.lFiveRateDisp').removeClass('disp');
					
					$.each(data, function(index, elem) {
						
						appendToTableLastRate(elem.vrnoa, elem.vrdate, elem.rate,elem.qty);
					});
				}
			}, error : function(xhr, status, error) {
				
				console.log(xhr.responseText);
			}
		});
	}

	var lastStockLocatons = function(itemId) {

		$("table .rateTbody tr").remove();
		$.ajax({

			url : baseUrl + 'purchase/lastStockLocatons',
			type : 'POST',
			data : { 'item_id' : itemId , 'etype' : 'sale'},
			dataType : 'JSON',
			
			success : function(data) {
				
				if (data === 'false') {
				
					   $('.lastStockLocationTableDisp').addClass('disp');
				} else {
				
					$('.lastStockLocationTableDisp').removeClass('disp');
					
					$.each(data, function(index, elem) {
						
						appendToTableLastStockLocation(elem.location, elem.qty);
					});
				}
			}, error : function(xhr, status, error) {
				
				console.log(xhr.responseText);
			}
		});
	}

	var appendToTableLastRate = function(vrnoa, vrdate, rate, stock) {

		var row ="<tr>" +
					"<td class='vr# numeric' data-title='Vr#' > "+ vrnoa +"</td>" +
			 		"<td class='vrdatee' data-title='Date'> "+ vrdate +"</td>" +
				 	"<td class='rate numeric' data-title='Rate'> "+ rate +"</td>" +
				 	"<td class='stock numeric' data-title='stock'> "+ stock +"</td>" +
			 	"</tr>";
		
		$(row).appendTo('#lastRateTable');
	}

	var appendToTableLastStockLocation = function(location, qty) {
		
		var row ="<tr>" +
					"<td class='location numeric' data-title='location' > "+ location +"</td>" +
				 	"<td class='qty numeric' data-title='qty'> "+ qty +"</td>" +
			 	"</tr>";
		
		$(row).appendTo('#lastStockLocationTable');
	}

	return {

		init : function() {

			this.bindUI();
			this.bindPurchaseFormValidation();
			this.bindWarehouseFormValidation();
			this.bindAccountFormValidation();
			this.bindItemFormValidation();
		},

		bindUI : function() {

			var self = this;
			$('#VoucherTypeHidden').val('new');

			shortcut.add("F10", function() {
    			
    			$('.btnSave').trigger('click');
			});

			shortcut.add("F5", function() {

    			$('.btnReset').trigger('click');
			});

			shortcut.add("F2", function() {

    			common.redirectUrl($('.btnBack').data('url'));
			});

			// when save button is clicked
			$('.btnSave, .btnSave2').on('click', function(e) {

				e.preventDefault();
				$("a[href='#main-tab']").trigger('click');
		        if($('#purchaseForm').valid()) {
		            
		            if($('#purchaseTable tbody tr').length > 0){

		            	self.initSave();	
		            }else{

		            	$('.gridItem').removeClass('display-hide');
		            }
		        }
			});

			$('.btnReset, .btnReset2').on('click', function(e){

				e.preventDefault();
				customForm.resetForm({'form' : 'purchaseForm'});
				getMaxId();
			});

			/*$('#itemIdDropdown').on('change', function() {

				var itemId = $(this).val();
				var rate = $(this).find('option:selected').data('prate');
				var grWeight = $(this).find('option:selected').data('grweight');
				var uomItem = $(this).find('option:selected').data('uom_item');
				var stQty = $(this).find('option:selected').data('stqty');
	            var stWeight = $(this).find('option:selected').data('stweight');
	            var size = $(this).find('option:selected').data('size');
				var barCode = $(this).find('option:selected').data('barcode');
				var discount = $(this).find('option:selected').data('discount');
	            
	            $('#sizeHidden').val(size);
				$('#rate').val(parseFloat(rate).toFixed(0));
				$('#itemDropdown').val(itemId).trigger('change');
				$('#uom').val(uomItem);
				$('#barCode').val(barCode);
				$('#discP').val(discount);

				var crit = getCrit();
				lastFiveSRate(crit);
				lastStockLocatons(item_id);
				$('#discP').trigger('input');
			});*/
			
			$('#itemDropdown').on('change', function() {

				if($(this).val() == 'new'){

					$('#itemModal').modal('show');
				}else{

					if($(this).val()){

						var itemId = $(this).val();
						var rate = $(this).find('option:selected').data('prate');
						var grWeight = $(this).find('option:selected').data('grweight');
						var uomItem = $(this).find('option:selected').data('uom_item');
						var stqty = $(this).find('option:selected').data('stqty');
		                var stWeight = $(this).find('option:selected').data('stweight');
		                var size = $(this).find('option:selected').data('size');
						var barCode = $(this).find('option:selected').data('barcode');
						var discount = $(this).find('option:selected').data('discount');

		                $('#sizeHidden').val(size);

						$('#rate').val(parseFloat(rate).toFixed(0));
						$('#itemIdDropdown').select2('val', itemId);
						$('#uom').val(uomItem);
						$('#barCode').val(barCode);
						$('#discP').val(discount);
						
						var crit = getCrit();
						lastFiveSRate(crit);
						lastStockLocatons(itemId);
						$('#discP').trigger('input');
					}
				}
			});

			$('#deptDropdown').on('change', function(){

				if($(this).val() == 'new'){

					$('#warehouseModal').modal('show');
				}
			});

			$('#partyDropdown').on('change', function(){

				if($(this).val() == 'new'){

					$('#accountModal').modal('show');
				}
			});

			$('#level3').on('change', function() {

				var level3 = $(this).val();
				$('#selectedLevel1').text('');
				$('#selectedLevel2').text('');
				
				if (level3 && level3 !== "" && level3 !== null) {
					$('#selectedLevel2').text(' ' + $(this).find('option:selected').data('level2'));
					$('#selectedLevel1').text(' ' + $(this).find('option:selected').data('level1'));
				}
			});

			$('#btnAdd').on('click', function(e) {
				e.preventDefault();

				$('.singleProduct').addClass('display-hide');
				$('.gridItem').addClass('display-hide');
				var error = validateSingleProductAdd();
				if (!error) {

					var barCode = $('#barCode').val();
					var itemDesc = $('#itemDropdown').find('option:selected').text();
					var itemId = $('#itemDropdown').val();
					var itemBarCode = $('#itemDropdown').find('option:selected').data('barcode');
					var qty = $('#qty').val();
					var rate = $('#rate').val();
					var amount = $('#amount').val();
					var discP = $('#discP').val();
					var disc = $('#disc').val();

					var brandId = $('#itemDropdown').find('option:selected').data('bid');
					var brandDesc =  $('#itemDropdown').find('option:selected').data('brand');
					var itemShortCode = $('#itemDropdown').find('option:selected').data('shortcode');
					
					disc = (disc == '' || disc == null) ? 0 : disc;

					// reset the values of the annoying fields
					$('#barCode').val('');
					$('#itemDropdown').val('').trigger('change');
					$('#itemIdDropdown').val('').trigger('change');
					$('#qty').val('');
					$('#rate').val('');
					$('#amount').val('');
					$('#discP').val('');
					$('#disc').val('');

					appendToTable('', barCode, itemDesc, itemId, qty, discP, disc, rate, amount, brandDesc, brandId, itemShortCode, itemBarCode);
					calculateLowerTotal(qty, amount, disc);
					$('#barCode').focus();
				} else {
					
					$('.singleProduct').removeClass('display-hide');
				}
			});

			$('#purchaseTable').on('click', '.btnRowRemove', function(e) {
				e.preventDefault();

				var qty = $.trim($(this).closest('tr').find('td.qty').text());
				var amount = $.trim($(this).closest('tr').find('td.amount').text());
				var rate = $.trim($(this).closest('tr').find('td.rate').text());
				var disc = $.trim($(this).closest('tr').find('td.disc').text());
				var discP = $.trim($(this).closest('tr').find('td.discP').text());

				calculateLowerTotal("-"+qty, "-"+amount, "-"+discount);
				$(this).closest('tr').remove();
			});

			$('#purchaseTable').on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				// getting values of the cruel row
				var barCode = $.trim($(this).closest('tr').find('td.barCodes').text());
				var itemId = $.trim($(this).closest('tr').find('td.itemDesc').data('item_id'));
				var qty = $.trim($(this).closest('tr').find('td.qty').text());
				var rate = $.trim($(this).closest('tr').find('td.rate').text());
				var amount = $.trim($(this).closest('tr').find('td.amount').text());
				var disc = $.trim($(this).closest('tr').find('td.disc').text());
				var discP = $.trim($(this).closest('tr').find('td.discP').text());
				
				//$('#itemIdDropdown').select2('val', itemId);
				$('#itemDropdown').select2('val', itemId);
				$('#barCode').val(barCode);
				$('#discP').val(discP);
				$('#disc').val(disc);
				$('#qty').val(qty);
				$('#rate').val(parseFloat(rate).toFixed(0));
				$('#amount').val(parseFloat(amount).toFixed(0));
				calculateLowerTotal("-"+qty, "-"+amount,"-"+discount);
				// now we have get all the value of the row that is being deleted. so remove that cruel row
				$(this).closest('tr').remove();	// yahoo removed
			});


			$('.btnSingleProductClose').on('click', function(e){

				e.preventDefault();
				$('.singleProduct').addClass('display-hide');
			});

			$('.btnGridItemClose').on('click', function(e){

				e.preventDefault();
				$('.gridItem').addClass('display-hide');
			});

			$('#qty, #rate').on('input', function(e){

				e.preventDefault();
				calculateUpperValues();
			});

			$('#discP').on('input', function() {

				var discP = $(this).val();
				var qty = $('#qty').val();
				var rate = $('#rate').val();
				var amount = qty * rate ;
				if(amount !== 0 && discP !== 0){
					
					var discount = amount*discP/100;
					$('#disc').val( parseFloat(discount).toFixed(0));
				}else{

					$('#disc').val('0');
				}
				calculateUpperValues();
			});

			$('#disc').on('input', function() {

				var discount = $(this).val();
				var qty = $('#qty').val();
				var rate = $('#rate').val();
				var amount = qty * rate ;
				if(amount !== 0 &&  discount !== 0){

					var discP = discount*100/amount;
					$('#disc').val( parseFloat(discP).toFixed(0));
				}else{

					$('#disc').val('0');
				}
				calculateUpperValues();
			});

			$('#discount').on('input', function() {

				var discount = $(this).val();
				var totalAmount = $('.totalAmount').text();
				var discountAmount = 0;
				if (discount !=0 && totalAmount != 0){
				
					discountAmount = totalAmount*discount/100;
				}
				
				$('#discAmount').val(discountAmount);
				calculateLowerTotal(0, 0, 0);
			});

			$('#discAmount').on('input', function() {
				
				var discountAmount = $(this).val();
				var totalAmount = $('.totalAmount').text();
				var discP = 0;
				if (discountAmount != 0 && totalAmount != 0){
				
					discP = discountAmount*100/totalAmount;
				}
				
				$('#discount').val(parseFloat(discP).toFixed(0));
				calculateLowerTotal(0, 0, 0);
			});

			$('#expense').on('input', function() {
				
				var expPercent = $(this).val();
				var totalAmount = $('.totalAmount').text();
				var expAmount = 0;
				if (expPercent != 0 && totalAmount != 0){
				
					expAmount = totalAmount*expPercent/100;
				}
				
				$('#expAmount').val(parseFloat(expAmount).toFixed(0));
				calculateLowerTotal(0, 0, 0);
			});

			$('#expAmount').on('input', function() {
				
				var expAmount = $(this).val();
				var totalAmount = $('.totalAmount').text();
				var expPercent = 0;
				if (expAmount != 0 && totalAmount != 0){
				
					expPercent = expAmount*100/totalAmount;
				}
				
				$('#expense').val(parseFloat(expPercent).toFixed(0));
				calculateLowerTotal(0, 0, 0);
			});

			$('#tax').on('input', function() {

				var taxPercent = $(this).val();
				var totalAmount = $('.totalAmount').text();
				var taxAmount = 0;
				if (taxPercent != 0 && totalAmount != 0){
				
					taxAmount = totalAmount*taxPercent/100;
				}
				
				$('#taxAmount').val(parseFloat(taxAmount).toFixed(0));
				calculateLowerTotal(0, 0, 0);
			});

			$('#taxAmount').on('input', function() {
				
				var taxAmount = $(this).val();
				var totalAmount = $('.totalAmount').text();
				var taxPercent = 0;
				if (taxAmount != 0 && totalAmount != 0){
				
					taxPercent = taxAmount*100/totalAmount;
				}
				
				$('#tax').val(parseFloat(taxPercent).toFixed(0));
				calculateLowerTotal(0, 0, 0);
			});

			// when addMoreInf is clicked
			$('.btnNext').on('click', function(e) {

				if($('#purchaseForm').valid()) {
		           	
		           	e.preventDefault(); 
		           	$('.alert-danger').hide();
		            $("a[href='#other-tab']").trigger('click');
					$('.btnNext').hide();
					$('.btnPrev').show();
		        }
			});

			$('.btnPrev').on('click', function(e) {

				e.preventDefault();
				$("a[href='#main-tab']").trigger('click');
				$('.btnNext').show();
				$('.btnPrev').hide();
			});

			$('a[href="#main-tab"]').on('click',function(e){
			  	
			  	e.preventDefault();
			  	$('.btnNext').show();
				$('.btnPrev').hide();
			});

			$('a[href="#other-tab"]').on('click',function(e){
			  	
			  	e.preventDefault();
			  	$('.btnNext').hide();
				$('.btnPrev').show();
			});

			$('.btnSaveWarehouse').on('click', function(e) {

				e.preventDefault();
		        if($('#formAddWarehouse').valid()) {
		            
		            self.initSaveWarehouse('modal');
		        }
			});

			$('.btnSaveAccount').on('click', function(){

				if($('#formAddAccount').valid()) {
		            
		            self.initSaveAccount('modal');
		        }
			});

			$('.btnSaveItem').on('click', function(){

				if($('#formAddModalItem').valid()) {
		            
		            self.initSaveItem('modal');
		        }
			});

			$('.btnResetWarehouse').on('click', function(e){

				e.preventDefault();
				customForm.resetForm({'form' : 'formAddWarehouse'});
			});

			$('.btnResetAccount').on('click', function(e){

				e.preventDefault();
				customForm.resetForm({'form' : 'formAddAccount'});
			});

			$('.btnResetItem').on('click', function(e){

				e.preventDefault();
				customForm.resetForm({'form' : 'formAddModalItem'});
			});

			$('#vrnoa').on('change', function() {
				
				var vrnoa = $(this).val();
				obj = {};
				obj['url'] = 'purchase/fetch';
				obj['data'] = {'vrnoa' : vrnoa , 'company_id' : $('#cid').val()},
				obj['form'] = 'purchaseForm';
				obj['maxId'] = 'vrnoaHidden';
				obj['maxHidId'] = 'maxVrnoaHidden';

				var data = voucherForm.fetch(obj);
				console.log(data);
				if(data){

					$.each(data, function(index, elem) {
                        
	                    appendToTable('', elem.item_barcode ,elem.item_name, elem.item_id, Math.abs(elem.s_qty),Math.abs(elem.discount1),Math.abs(elem.damount), parseFloat(elem.s_rate).toFixed(0), parseFloat(elem.s_net).toFixed(0),elem.brand_name,elem.bid,elem.short_code,elem.item_barcode);
	                    calculateLowerTotal(Math.abs(elem.s_qty), elem.s_net,elem.damount);
	                });
				}
			});

			self.fetchRequestedVr();
		},

		// makes the voucher ready to save
		initSave : function() {

			var stockMain = voucherForm.getSaveObj({'form' : 'purchaseForm'});	// returns the account detail object to save into database
			var voucherDetailObj = voucherForm.getSaveDetailObj({'table' : 'purchaseTable'});
			var ledgerObject = getSaveLedgerObj();

			var obj = {};
			obj['url'] = 'purchase/save';
			obj['data'] = {'stockmain' : JSON.stringify(stockMain), 'stockdetail' : JSON.stringify(voucherDetailObj.stock_detail), 'vrnoa' : stockMain.vrnoa, 'ledger' : JSON.stringify(ledgerObject) ,'voucher_type_hidden':$('#voucherTypeHidden').val(), 'etype':'purchase' },
			obj['form'] = 'purchaseForm';
			voucherForm.saveForm(obj);		// saves the detail into the database				
		},

		initSaveWarehouse : function(saveType) {

			var warehouseObj = customForm.getSaveObj({'form' : 'formAddWarehouse'});	// returns the account detail object to save into database
			
			var obj = {};
			obj['url'] = 'warehouse/save';
			obj['data'] = { 'warehouse_detail' : warehouseObj, 'voucher_type_hidden' : $('#voucherTypeHidden').val()};
			obj['saveType'] = saveType;
			obj['form'] = 'formAddWarehouse';
			obj['attr'] = 'Warehouse';
			obj['appendForm'] = 'purchaseForm';
			obj['appendInputName'] = 'name';
			obj['appendSelectName'] = 'godown_id';
			obj['modal'] = 'accountModal'
			customForm.saveForm(obj);		// saves the detail into the database				
		},

		initSaveAccount : function(saveType) {

			var accountObj = customForm.getSaveObj({'form' : 'formAddAccount'});	// returns the account detail object to save into database
			
			var obj = {};
			obj['url'] = 'parties/save';
			obj['data'] = { 'account_detail' : accountObj, 'voucher_type_hidden' : $('#voucherTypeHidden').val()};
			obj['saveType'] = saveType;
			obj['form'] = 'formAddAccount';
			obj['attr'] = 'Account';
			obj['appendForm'] = 'purchaseForm';
			obj['appendInputName'] = 'name';
			obj['appendSelectName'] = 'party_id';
			obj['modal'] = 'accountModal'
			customForm.saveForm(obj);		// saves the detail into the database				
		},

		initSaveItem : function(saveType) {

			var itemObj = customForm.getSaveObj({'form' : 'formAddModalItem'});	// returns the account detail object to save into database
			
			var obj = {};
			obj['url'] = 'items/save';
			obj['data'] = { 'item_detail' : itemObj, 'voucher_type_hidden' : $('#voucherTypeHidden').val()};
			obj['saveType'] = saveType;
			obj['form'] = 'formAddModalItem';
			obj['attr'] = 'Item';
			obj['appendForm'] = 'purchaseForm';
			obj['appendInputName'] = 'item_des';
			obj['appendSelectName'] = 'item_dropdown';
			obj['modal'] = 'itemModal'
			customForm.saveForm(obj);		// saves the detail into the database				
		},

		fetchRequestedVr : function () {

			var vrnoa = common.getQueryStringVal('vrnoa');
			vrnoa = parseInt(vrnoa);
			$('#vrnoa').val(vrnoa);
			$('#vrnoaHidden').val(vrnoa);
			if (!isNaN(vrnoa)) {
				
				fetch(vrnoa);
			}else{
				
				getMaxVrno();
				getMaxVrnoa();
			}
		},

		bindPurchaseFormValidation : function(){

			var form = $('#purchaseForm');
	        var error = $('.alert-danger', form);
	        var success = $('.alert-success', form);

	        form.validate({

	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",  // validate all fields including form hidden input
	            rules: {
	                vrnoa: {
	                    min: 1,
	                    required: true
	                },
	                party_id: {
	                    required: true
	                },
	                godown_id: {
	                    required: true
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	                success.hide();
	                error.show();
	                App.scrollTo(error, -200);
	            },

	            errorPlacement: function (error, element) { // render error placement for each input type

	                var icon = $(element).parent('.input-icon').children('i');
	                icon.removeClass('fa-check').addClass("fa-warning");  
	                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	                if (element.attr("data-error-container")) { 

	                    error.appendTo(element.attr("data-error-container"));
	                }else{

	                    error.insertAfter(element);
	                }
	            },

	            highlight: function (element) { // hightlight error inputs
	                
	                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                
	            },

	            success: function (label, element) {

	                var icon = $(element).parent('.input-icon').children('i');
	                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	                icon.removeClass("fa-warning").addClass("fa-check");
	            },

	            submitHandler: function (form) {

	                success.show();
	                error.hide();
	                form[0].submit(); // submit the form
	            }
	        });
		},

		bindWarehouseFormValidation : function(){

			var form = $('#formAddWarehouse');
	        var error = $('.alert-danger', form);
	        var success = $('.alert-success', form);

	        form.validate({

	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",  // validate all fields including form hidden input
	            rules: {
	                warehouseId: {
	                    min: 1,
	                    required: true
	                },
	                name: {
	                    minlength: 2,
	                    required: true
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	                success.hide();
	                error.show();
	                App.scrollTo(error, -200);
	            },

	            errorPlacement: function (error, element) { // render error placement for each input type

	                var icon = $(element).parent('.input-icon').children('i');
	                icon.removeClass('fa-check').addClass("fa-warning");  
	                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	                if (element.attr("data-error-container")) { 

	                    error.appendTo(element.attr("data-error-container"));
	                }else{

	                    error.insertAfter(element);
	                }
	            },

	            highlight: function (element) { // hightlight error inputs
	                
	                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                
	            },

	            success: function (label, element) {

	                var icon = $(element).parent('.input-icon').children('i');
	                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	                icon.removeClass("fa-warning").addClass("fa-check");
	            },

	            submitHandler: function (form) {

	                success.show();
	                error.hide();
	                form[0].submit(); // submit the form
	            }
	        });
		},

		bindAccountFormValidation : function(){

			var form = $('#formAddAccount');
	        var error = $('.alert-danger', form);
	        var success = $('.alert-success', form);

	        form.validate({

	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",  // validate all fields including form hidden input
	            rules: {
	            	accountId: {
	                    min: 1,
	                    required: true
	                },
	                name: {
	                    minlength: 2,
	                    required: true
	                },
	                level3: {
	                    required: true
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	                success.hide();
	                error.show();
	                App.scrollTo(error, -200);
	            },

	            errorPlacement: function (error, element) { // render error placement for each input type

	                var icon = $(element).parent('.input-icon').children('i');
	                icon.removeClass('fa-check').addClass("fa-warning");  
	                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	                if (element.attr("data-error-container")) { 

	                    error.appendTo(element.attr("data-error-container"));
	                }else{

	                    error.insertAfter(element);
	                }
	            },

	            highlight: function (element) { // hightlight error inputs
	                
	                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                
	            },

	            success: function (label, element) {

	                var icon = $(element).parent('.input-icon').children('i');
	                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	                icon.removeClass("fa-warning").addClass("fa-check");
	            },

	            submitHandler: function (form) {

	                success.show();
	                error.hide();
	                form[0].submit(); // submit the form
	            }
	        });
		},

		bindItemFormValidation : function(){

			var form = $('#formAddModalItem');
	        var error = $('.alert-danger', form);
	        var success = $('.alert-success', form);

	        form.validate({

	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block help-block-error', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",  // validate all fields including form hidden input
	            rules: {
	                itemId: {
	                    min: 1,
	                    required: true
	                },
	                catid: {
	                    required: true
	                },
	                item_des: {
	                    minlength: 3,
	                    required: true
	                },
	                short_code: {
	                	required: true
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	                success.hide();
	                error.show();
	                App.scrollTo(error, -200);
	            },

	            errorPlacement: function (error, element) { // render error placement for each input type

	                var icon = $(element).parent('.input-icon').children('i');
	                icon.removeClass('fa-check').addClass("fa-warning");  
	                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	                if (element.attr("data-error-container")) { 

	                    error.appendTo(element.attr("data-error-container"));
	                }else{

	                    error.insertAfter(element);
	                }
	            },

	            highlight: function (element) { // hightlight error inputs
	                
	                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },

	            unhighlight: function (element) { // revert the change done by hightlight
	                
	            },

	            success: function (label, element) {

	                var icon = $(element).parent('.input-icon').children('i');
	                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	                icon.removeClass("fa-warning").addClass("fa-check");
	            },

	            submitHandler: function (form) {

	                success.show();
	                error.hide();
	                form[0].submit(); // submit the form
	            }
	        });
		}
	};
};

var purchase = new Purchase();
purchase.init();