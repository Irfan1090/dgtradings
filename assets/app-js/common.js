  Array.prototype.chunk = function ( n ) {
            if ( !this.length ) {
                return [];
            }
            return [ this.slice( 0, n ) ].concat( this.slice(n).chunk(n) );
        };
var common = {

	hideLoader : false,

	init 	: function (){

		common.bindUI();
		common.bindPartiesModalDataTable();
	},
   
    comm_func : function(){
        var sr = 0;
        $('table thead tr:eq(1) td').each(function(index,elem){
            sr ++;
        })
        return sr;
    },  exports : function(com){
        var arr = [];
        var i;
      for (i=0; i <= com; i++) {
          arr.push(i);
      };
        return arr;
    },

	bindUI 	: function (){


	    $('.num').keypress(function (e) {
	        
	        common.blockKeys(e);
	    });

		$.ajaxSetup({ cache: false });

		// bind application wide loader
		$(document).ajaxStart(function() {

			$(".loader").show();
		});
		$(document).ajaxComplete(function(event, xhr, settings) {

			$(".loader").hide();

		});

        $('.modal').on('shown', function (){

            $(this).find('input:first').focus();
        });

		$('body').on('change','#level3_namee', function (){
            var l3 = $(this).find('option:selected').text();
            var l2 = $(this).find('option:selected').data('level2');
			var l1 = $(this).find('option:selected').data('level1');
            $('#lthird').text(l3);
            $('#lsecond').text(l2);
            $('#lfirst').text(l1);
		});

		$('.select2').select2();

        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });

        $('.menu-toggler').on('click', function(){
            
            
            if($('.page-sidebar-menu').hasClass("page-sidebar-menu-closed")){

                common.changeSideMenu(0);
            }else{

                common.changeSideMenu(1);
            }
        })
	},

	getQueryStringVal : function ( key ){
		
		var key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
		var match = location.search.match(new RegExp("[?&]"+key+"=([^&]+)(&|$)"));
		return match && decodeURIComponent(match[1].replace(/\+/g, " "));
	},

    getNumVal : function(el){

        return isNaN(parseFloat(el.val())) ? 0 : parseFloat(el.val());
    },

    getNumText : function(el){

        return isNaN(parseFloat(el.text())) ? 0 : parseFloat(el.text());
    },

	reloadWindow : function() {

		var loc = window.location.href,
		index = loc.indexOf('#');

		if (index > 0) {
		  	
		  	window.location = loc.substring(0, index);
		}else{

            window.location = self.location;
        }
	},

    reloadForm : function() {

        var loc = window.location.href,
        index = loc.indexOf('#'),
        indexQueryString = loc.indexOf('?');

        if (index > 0) {
            
            window.location = loc.substring(0, index);
        }else if(indexQueryString > 0){

            window.location = loc.substring(0, indexQueryString);  
        }else{
           
            window.location = self.location; 
        }
    },

    redirectUrl : function(url){

        window.location = baseUrl+url;
    },

	blockKeys : function(e){

		var numericKeys = [];
        var pressedKey  = e.which;

        for (i = 48; i < 58; i++) {
            numericKeys.push(i);
        }

        numericKeys.push(46);
        numericKeys.push(8);
        numericKeys.push(0);

        if (!(numericKeys.indexOf(pressedKey ) >= 0)) {

            e.preventDefault();
        }
	},

	bindPartiesModalDataTable : function(){

		var table = $('#partiesModalTable');

        table.DataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ records",
                "infoEmpty": "No records found",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Show _MENU_",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                },
                "processing": "Loading..."
            },

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
            "pagingType": "bootstrap_extended",

            "lengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "All"] // change per page values here
            ],
            // set the initial value
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": baseUrl+"parties/partiesSearchTable", // ajax source
            },
            "pageLength": 10,
            "columnDefs": [{  // set default column settings
                'orderable': false,
                'targets': [4]
            }, {
                "searchable": false,
                "targets": [4]
            },{
                "class": 'text-center',
                "targets": [4]
            }],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });
	},

    changeSideMenu : function(isClosed){

        $.ajax({
            url : baseUrl + 'dashboard/changeSideMenu',
            type : 'POST',
            data : {is_closed : isClosed},
            dataType : 'JSON',
            success : function(data) {

                console.log(data)
            }, error : function(xhr, status, error) {

                console.log(xhr.responseText);
            }
        });
    }
}

$(function(){

	common.init();
});