<div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                        <div class="theme-panel hidden-xs hidden-sm">
                            <div class="toggler"> </div>
                            <div class="toggler-close"> </div>
                     <!--        <div class="theme-options">
                                <div class="theme-option theme-colors clearfix">
                                    <span> THEME COLOR </span>
                                    <ul>
                                        <li class="color-default current tooltips" data-style="default" data-container="body" data-original-title="Default"> </li>
                                        <li class="color-darkblue tooltips" data-style="darkblue" data-container="body" data-original-title="Dark Blue"> </li>
                                        <li class="color-blue tooltips" data-style="blue" data-container="body" data-original-title="Blue"> </li>
                                        <li class="color-grey tooltips" data-style="grey" data-container="body" data-original-title="Grey"> </li>
                                        <li class="color-light tooltips" data-style="light" data-container="body" data-original-title="Light"> </li>
                                        <li class="color-light2 tooltips" data-style="light2" data-container="body" data-html="true" data-original-title="Light 2"> </li>
                                    </ul>
                                </div>
                                <div class="theme-option">
                                    <span> Theme Style </span>
                                    <select class="layout-style-option form-control input-sm">
                                        <option value="square" selected="selected">Square corners</option>
                                        <option value="rounded">Rounded corners</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Layout </span>
                                    <select class="layout-option form-control input-sm">
                                        <option value="fluid" selected="selected">Fluid</option>
                                        <option value="boxed">Boxed</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Header </span>
                                    <select class="page-header-option form-control input-sm">
                                        <option value="fixed" selected="selected">Fixed</option>
                                        <option value="default">Default</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Top Menu Dropdown</span>
                                    <select class="page-header-top-dropdown-style-option form-control input-sm">
                                        <option value="light" selected="selected">Light</option>
                                        <option value="dark">Dark</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Mode</span>
                                    <select class="sidebar-option form-control input-sm">
                                        <option value="fixed">Fixed</option>
                                        <option value="default" selected="selected">Default</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Menu </span>
                                    <select class="sidebar-menu-option form-control input-sm">
                                        <option value="accordion" selected="selected">Accordion</option>
                                        <option value="hover">Hover</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Style </span>
                                    <select class="sidebar-style-option form-control input-sm">
                                        <option value="default" selected="selected">Default</option>
                                        <option value="light">Light</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Position </span>
                                    <select class="sidebar-pos-option form-control input-sm">
                                        <option value="left" selected="selected">Left</option>
                                        <option value="right">Right</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Footer </span>
                                    <select class="page-footer-option form-control input-sm">
                                        <option value="fixed">Fixed</option>
                                        <option value="default" selected="selected">Default</option>
                                    </select>
                                </div>
                            </div> -->
                        </div>
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Admin Dashboard
                            <small>statistics, charts, recent events and reports</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="1349">0</span>
                                        </div>
                                        <div class="desc"> New Feedbacks </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                                    <div class="visual">
                                        <i class="fa fa-bar-chart-o"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="12,5">0</span>M$ </div>
                                        <div class="desc"> Total Profit </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                                    <div class="visual">
                                        <i class="fa fa-shopping-cart"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="549">0</span>
                                        </div>
                                        <div class="desc"> New Orders </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
                                    <div class="visual">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number"> +
                                            <span data-counter="counterup" data-value="89"></span>% </div>
                                        <div class="desc"> Brand Popularity </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <!-- END DASHBOARD STATS 1-->
                        <div class="row">
                            <!-- <div class="col-lg-6 col-xs-12 col-sm-12"> -->
                                <!-- BEGIN PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-bar-chart font-dark hide"></i>
                                            <span class="caption-subject font-dark bold uppercase">Site Visits</span>
                                            <span class="caption-helper">weekly stats...</span>
                                        </div>
                                        <div class="actions">
                                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                                <label class="btn red btn-outline btn-circle btn-sm active">
                                                    <input type="radio" name="options" class="toggle" id="option1">New</label>
                                                <label class="btn red btn-outline btn-circle btn-sm">
                                                    <input type="radio" name="options" class="toggle" id="option2">Returning</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div id="site_statistics_loading">
                                            <img src="<?= base_url(); ?>/assets/global/img/loading.gif" alt="loading" /> </div>
                                        <div id="site_statistics_content" class="display-none">
                                            <div id="site_statistics" class="chart"> </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PORTLET-->
                            <!-- </div> -->
                            <div class="col-lg-6 col-xs-12 col-sm-12">
                                <!-- BEGIN PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-share font-red-sunglo hide"></i>
                                            <span class="caption-subject font-dark bold uppercase">Revenue</span>
                                            <span class="caption-helper">monthly stats...</span>
                                        </div>
                                        <div class="actions">
                                            <div class="btn-group">
                                                <a href="" class="btn dark btn-outline btn-circle btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Filter Range
                                                    <span class="fa fa-angle-down"> </span>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li>
                                                        <a href="javascript:;"> Q1 2014
                                                            <span class="label label-sm label-default"> past </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"> Q2 2014
                                                            <span class="label label-sm label-default"> past </span>
                                                        </a>
                                                    </li>
                                                    <li class="active">
                                                        <a href="javascript:;"> Q3 2014
                                                            <span class="label label-sm label-success"> current </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"> Q4 2014
                                                            <span class="label label-sm label-warning"> upcoming </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div id="site_activities_loading">
                                            <img src="<?= base_url(); ?>/assets/global/img/loading.gif" alt="loading" /> </div>
                                        <div id="site_activities_content" class="display-none">
                                            <div id="site_activities" style="height: 228px;"> </div>
                                        </div>
                                        <div style="margin: 20px 0 10px 30px">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                                                    <span class="label label-sm label-success"> Revenue: </span>
                                                    <h3>$13,234</h3>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                                                    <span class="label label-sm label-info"> Tax: </span>
                                                    <h3>$134,900</h3>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                                                    <span class="label label-sm label-danger"> Shipment: </span>
                                                    <h3>$1,134</h3>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                                                    <span class="label label-sm label-warning"> Orders: </span>
                                                    <h3>235090</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PORTLET-->
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-6 col-xs-12 col-sm-12">
                                <div class="portlet light portlet-fit bordered">
                                  <!--   <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-directions font-green hide"></i>
                                            <span class="caption-subject bold font-dark uppercase "> Activities</span>
                                            <span class="caption-helper">Horizontal Timeline</span>
                                        </div>
                                        <div class="actions">
                                            <div class="btn-group">
                                                <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li>
                                                        <a href="javascript:;"> Action 1</a>
                                                    </li>
                                                    <li class="divider"> </li>
                                                    <li>
                                                        <a href="javascript:;">Action 2</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">Action 3</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">Action 4</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div> -->
                                    <!-- <div class="portlet-body"> -->
                                        <!-- <div class="cd-horizontal-timeline mt-timeline-horizontal" data-spacing="60"> -->
                                            <!-- <div class="timeline"> -->
                                                <!-- <div class="events-wrapper"> -->
                                                   <!--  <div class="events">
                                                        <ol>
                                                            <li>
                                                                <a href="#0" data-date="16/01/2014" class="border-after-red bg-after-red selected">16 Jan</a>
                                                            </li>
                                                            <li>
                                                                <a href="#0" data-date="28/02/2014" class="border-after-red bg-after-red">28 Feb</a>
                                                            </li>
                                                            <li>
                                                                <a href="#0" data-date="20/04/2014" class="border-after-red bg-after-red">20 Mar</a>
                                                            </li>
                                                            <li>
                                                                <a href="#0" data-date="20/05/2014" class="border-after-red bg-after-red">20 May</a>
                                                            </li>
                                                            <li>
                                                                <a href="#0" data-date="09/07/2014" class="border-after-red bg-after-red">09 Jul</a>
                                                            </li>
                                                            <li>
                                                                <a href="#0" data-date="30/08/2014" class="border-after-red bg-after-red">30 Aug</a>
                                                            </li>
                                                            <li>
                                                                <a href="#0" data-date="15/09/2014" class="border-after-red bg-after-red">15 Sep</a>
                                                            </li>
                                                            <li>
                                                                <a href="#0" data-date="01/11/2014" class="border-after-red bg-after-red">01 Nov</a>
                                                            </li>
                                                            <li>
                                                                <a href="#0" data-date="10/12/2014" class="border-after-red bg-after-red">10 Dec</a>
                                                            </li>
                                                            <li>
                                                                <a href="#0" data-date="19/01/2015" class="border-after-red bg-after-red">29 Jan</a>
                                                            </li>
                                                            <li>
                                                                <a href="#0" id="test_pest" data-date="03/03/2015" class="border-after-red bg-after-red">3 Mar - 1</a>
                                                            </li>
                                                        </ol>
                                                        <span class="filling-line bg-red" aria-hidden="true"></span>
                                                    </div> -->
                                                    <!-- .events -->
                                                <!-- </div> -->
                                                <!-- .events-wrapper -->
                                               <!--  <ul class="cd-timeline-navigation mt-ht-nav-icon">
                                                    <li>
                                                        <a href="#0" class="prev inactive btn btn-outline red md-skip">
                                                            <i class="fa fa-chevron-left"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#0" class="next btn btn-outline red md-skip">
                                                            <i class="fa fa-chevron-right"></i>
                                                        </a>
                                                    </li>
                                                </ul> -->
                                                <!-- .cd-timeline-navigation -->
                                            <!-- </div> -->
                                            <!-- .timeline -->
                                           <!--  <div class="events-content">
                                                <ol>
                                                    <li class="selected" data-date="16/01/2014">
                                                        <div class="mt-title">
                                                            <h2 class="mt-content-title">New User</h2>
                                                        </div>
                                                        <div class="mt-author">
                                                            <div class="mt-avatar">
                                                                <img src="<?= base_url(); ?>/assets/pages/media/users/avatar80_3.jpg" />
                                                            </div>
                                                            <div class="mt-author-name">
                                                                <a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
                                                            </div>
                                                            <div class="mt-author-datetime font-grey-mint">16 January 2014 : 7:45 PM</div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="mt-content border-grey-steel">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, mi felis, aliquam at iaculis mi felis, aliquam
                                                                at iaculis finibus eu ex. Integer efficitur tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur odio non est rhoncus volutpat.</p>
                                                            <a href="javascript:;" class="btn btn-circle red btn-outline">Read More</a>
                                                            <a href="javascript:;" class="btn btn-circle btn-icon-only blue">
                                                                <i class="fa fa-plus"></i>
                                                            </a>
                                                            <a href="javascript:;" class="btn btn-circle btn-icon-only green pull-right">
                                                                <i class="fa fa-twitter"></i>
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li data-date="28/02/2014">
                                                        <div class="mt-title">
                                                            <h2 class="mt-content-title">Sending Shipment</h2>
                                                        </div>
                                                        <div class="mt-author">
                                                            <div class="mt-avatar">
                                                                <img src="<?= base_url(); ?>/assets/pages/media/users/avatar80_3.jpg" />
                                                            </div>
                                                            <div class="mt-author-name">
                                                                <a href="javascript:;" class="font-blue-madison">Hugh Grant</a>
                                                            </div>
                                                            <div class="mt-author-datetime font-grey-mint">28 February 2014 : 10:15 AM</div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="mt-content border-grey-steel">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
                                                                dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
                                                                odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
                                                                dignissim luctus risus sed sodales.</p>
                                                            <a href="javascript:;" class="btn btn-circle btn-outline green-jungle">Download Shipment List</a>
                                                            <div class="btn-group dropup pull-right">
                                                                <button class="btn btn-circle blue-steel dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> Actions
                                                                    <i class="fa fa-angle-down"></i>
                                                                </button>
                                                                <ul class="dropdown-menu pull-right" role="menu">
                                                                    <li>
                                                                        <a href="javascript:;">Action </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:;">Another action </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:;">Something else here </a>
                                                                    </li>
                                                                    <li class="divider"> </li>
                                                                    <li>
                                                                        <a href="javascript:;">Separated link </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li data-date="20/04/2014">
                                                        <div class="mt-title">
                                                            <h2 class="mt-content-title">Blue Chambray</h2>
                                                        </div>
                                                        <div class="mt-author">
                                                            <div class="mt-avatar">
                                                                <img src="<?= base_url(); ?>/assets/pages/media/users/avatar80_1.jpg" />
                                                            </div>
                                                            <div class="mt-author-name">
                                                                <a href="javascript:;" class="font-blue">Rory Matthew</a>
                                                            </div>
                                                            <div class="mt-author-datetime font-grey-mint">20 April 2014 : 10:45 PM</div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="mt-content border-grey-steel">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
                                                                dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
                                                                odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
                                                                dignissim luctus risus sed sodales.</p>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
                                                                qui ut. laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut. </p>
                                                            <a href="javascript:;" class="btn btn-circle red">Read More</a>
                                                        </div>
                                                    </li>
                                                    <li data-date="20/05/2014">
                                                        <div class="mt-title">
                                                            <h2 class="mt-content-title">Timeline Received</h2>
                                                        </div>
                                                        <div class="mt-author">
                                                            <div class="mt-avatar">
                                                                <img src="<?= base_url(); ?>/assets/pages/media/users/avatar80_2.jpg" />
                                                            </div>
                                                            <div class="mt-author-name">
                                                                <a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
                                                            </div>
                                                            <div class="mt-author-datetime font-grey-mint">20 May 2014 : 12:20 PM</div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="mt-content border-grey-steel">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
                                                                dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
                                                                odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
                                                                dignissim luctus risus sed sodales.</p>
                                                            <a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
                                                        </div>
                                                    </li>
                                                    <li data-date="09/07/2014">
                                                        <div class="mt-title">
                                                            <h2 class="mt-content-title">Event Success</h2>
                                                        </div>
                                                        <div class="mt-author">
                                                            <div class="mt-avatar">
                                                                <img src="<?= base_url(); ?>/assets/pages/media/users/avatar80_1.jpg" />
                                                            </div>
                                                            <div class="mt-author-name">
                                                                <a href="javascript:;" class="font-blue-madison">Matt Goldman</a>
                                                            </div>
                                                            <div class="mt-author-datetime font-grey-mint">9 July 2014 : 8:15 PM</div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="mt-content border-grey-steel">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde.</p>
                                                            <a href="javascript:;"
                                                                class="btn btn-circle btn-outline purple-medium">View Summary</a>
                                                            <div class="btn-group dropup pull-right">
                                                                <button class="btn btn-circle green dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> Actions
                                                                    <i class="fa fa-angle-down"></i>
                                                                </button>
                                                                <ul class="dropdown-menu pull-right" role="menu">
                                                                    <li>
                                                                        <a href="javascript:;">Action </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:;">Another action </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:;">Something else here </a>
                                                                    </li>
                                                                    <li class="divider"> </li>
                                                                    <li>
                                                                        <a href="javascript:;">Separated link </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li data-date="30/08/2014">
                                                        <div class="mt-title">
                                                            <h2 class="mt-content-title">Conference Call</h2>
                                                        </div>
                                                        <div class="mt-author">
                                                            <div class="mt-avatar">
                                                                <img src="<?= base_url(); ?>/assets/pages/media/users/avatar80_1.jpg" />
                                                            </div>
                                                            <div class="mt-author-name">
                                                                <a href="javascript:;" class="font-blue-madison">Rory Matthew</a>
                                                            </div>
                                                            <div class="mt-author-datetime font-grey-mint">30 August 2014 : 5:45 PM</div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="mt-content border-grey-steel">
                                                            <img class="timeline-body-img pull-left" src="<?= base_url(); ?>/assets/pages/media/blog/5.jpg" alt="">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
                                                                qui ut. laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut. </p>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
                                                                qui ut. laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut. </p>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
                                                                qui ut. laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut. </p>
                                                            <a href="javascript:;" class="btn btn-circle red">Read More</a>
                                                        </div>
                                                    </li>
                                                    <li data-date="15/09/2014">
                                                        <div class="mt-title">
                                                            <h2 class="mt-content-title">Conference Decision</h2>
                                                        </div>
                                                        <div class="mt-author">
                                                            <div class="mt-avatar">
                                                                <img src="<?= base_url(); ?>/assets/pages/media/users/avatar80_5.jpg" />
                                                            </div>
                                                            <div class="mt-author-name">
                                                                <a href="javascript:;" class="font-blue-madison">Jessica Wolf</a>
                                                            </div>
                                                            <div class="mt-author-datetime font-grey-mint">15 September 2014 : 8:30 PM</div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="mt-content border-grey-steel">
                                                            <img class="timeline-body-img pull-right" src="<?= base_url(); ?>/assets/pages/media/blog/6.jpg" alt="">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
                                                                qui ut.</p>
                                                            <a href="javascript:;" class="btn btn-circle green-sharp">Read More</a>
                                                        </div>
                                                    </li>
                                                    <li data-date="01/11/2014">
                                                        <div class="mt-title">
                                                            <h2 class="mt-content-title">Timeline Received</h2>
                                                        </div>
                                                        <div class="mt-author">
                                                            <div class="mt-avatar">
                                                                <img src="<?= base_url(); ?>/assets/pages/media/users/avatar80_2.jpg" />
                                                            </div>
                                                            <div class="mt-author-name">
                                                                <a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
                                                            </div>
                                                            <div class="mt-author-datetime font-grey-mint">1 November 2014 : 12:20 PM</div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="mt-content border-grey-steel">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
                                                                dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
                                                                odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
                                                                dignissim luctus risus sed sodales.</p>
                                                            <a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
                                                        </div>
                                                    </li>
                                                    <li data-date="10/12/2014">
                                                        <div class="mt-title">
                                                            <h2 class="mt-content-title">Timeline Received</h2>
                                                        </div>
                                                        <div class="mt-author">
                                                            <div class="mt-avatar">
                                                                <img src="<?= base_url(); ?>/assets/pages/media/users/avatar80_2.jpg" />
                                                            </div>
                                                            <div class="mt-author-name">
                                                                <a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
                                                            </div>
                                                            <div class="mt-author-datetime font-grey-mint">10 December 2015 : 12:20 PM</div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="mt-content border-grey-steel">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
                                                                dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
                                                                odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
                                                                dignissim luctus risus sed sodales.</p>
                                                            <a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
                                                        </div>
                                                    </li>
                                                    <li data-date="19/01/2015">
                                                        <div class="mt-title">
                                                            <h2 class="mt-content-title">Timeline Received</h2>
                                                        </div>
                                                        <div class="mt-author">
                                                            <div class="mt-avatar">
                                                                <img src="<?= base_url(); ?>/assets/pages/media/users/avatar80_2.jpg" />
                                                            </div>
                                                            <div class="mt-author-name">
                                                                <a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
                                                            </div>
                                                            <div class="mt-author-datetime font-grey-mint">19 January 2015 : 12:20 PM</div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="mt-content border-grey-steel">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
                                                                dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
                                                                odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
                                                                dignissim luctus risus sed sodales.</p>
                                                            <a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
                                                        </div>
                                                    </li>
                                                    <li data-date="03/03/2015">
                                                        <div class="mt-title">
                                                            <h2 class="mt-content-title">Timeline Received</h2>
                                                        </div>
                                                        <div class="mt-author">
                                                            <div class="mt-avatar">
                                                                <img src="<?= base_url(); ?>/assets/pages/media/users/avatar80_2.jpg" />
                                                            </div>
                                                            <div class="mt-author-name">
                                                                <a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
                                                            </div>
                                                            <div class="mt-author-datetime font-grey-mint">3 March 2015 : 12:20 PM</div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="mt-content border-grey-steel">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
                                                                dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
                                                                odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
                                                                dignissim luctus risus sed sodales.</p>
                                                            <a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
                                                        </div>
                                                    </li>
                                                </ol>
                                            </div> -->
                                            <!-- .events-content -->
                                        <!-- </div> -->
                                    <!-- </div> -->
                                </div>
                            </div>
                           
                        </div>
                      
                        
                       
                       
                        
                    </div>