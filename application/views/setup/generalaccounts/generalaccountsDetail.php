<h5><strong>Basic Information:</strong></h5>
<div class="row">
	<dynamic name="spid" <?= $spid;?>>
    	<div class="col-md-2"><strong><?= $idname; ?> Id:</strong></div>
    	<div class="col-md-4"><?=($result['spid']) ? $result['spid'] : '-'; ?></div>
	</dynamic>
	<dynamic name="account_id" <?= $account_id;?> >
    	<div class="col-md-2"><strong>Account Code:</strong></div>
    	<div class="col-md-4"><?=($result['account_id']) ? $result['account_id'] : '-'; ?></div>
	</dynamic>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
	<dynamic name="name" <?= $name;?> >
    	<div class="col-md-2"><strong>Name:</strong></div>
    	<div class="col-md-4"><?=($result['name']) ? $result['name'] : '-'; ?></div>
	</dynamic>
    <dynamic name="mobile" <?= $mobile;?> >
    	<div class="col-md-2"><strong>Mobile#:</strong></div>
    	<div class="col-md-4"><?=($result['mobile']) ? $result['mobile'] : '-'; ?></div>
    </dynamic>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <dynamic name="address"<?= $address;?> >
    	<div class="col-md-2"><strong>Address:</strong></div>
    	<div class="col-md-4"><?=($result['address']) ? $result['address'] : '-'; ?></div>
    </dynamic>
    <dynamic name="limit"<?= $limit;?> >
    	<div class="col-md-2"><strong>Credit Limit:</strong></div>
    	<div class="col-md-4"><?=($result['limit']) ? $result['limit'] : '-'; ?></div>
	</dynamic>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
	<dynamic name="l3_name" <?= $l3_name;?> >
    <div class="col-md-2"><strong>Account Type:</strong></div>
    <div class="col-md-4"><?=($result['l3_name']) ? $result['l3_name'] : '-'; ?></div>
	</dynamic>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<h5><strong>Detailed Information:</strong></h5>
<div class="row">
    <dynamic name="contact_person" <?= $contact_person;?>  >
    	<div class="col-md-2"><strong>Contact Person:</strong></div>
    	<div class="col-md-4"><?=($result['contact_person']) ? $result['contact_person'] : '-'; ?></div>
	</dynamic>
    <dynamic name="cnic"  <?= $cnic;?> >
    	<div class="col-md-2"><strong>CNIC:</strong></div>
    	<div class="col-md-4"><?=($result['cnic']) ? $result['cnic'] : '-'; ?></div>
	</dynamic>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <dynamic name="ntn" <?= $ntn;?> >
    	<div class="col-md-2"><strong>NTN:</strong></div>
    	<div class="col-md-4"><?=($result['ntn']) ? $result['ntn'] : '-'; ?></div>
	</dynamic>
    <dynamic name="phone" <?= $phone;?> >
    	<div class="col-md-2"><strong>Phone:</strong></div>
    	<div class="col-md-4"><?=($result['phone']) ? $result['phone'] : '-'; ?></div>
	</dynamic>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <dynamic name="email" <?= $email;?> >
    	<div class="col-md-2"><strong>Email:</strong></div>
    	<div class="col-md-4"><?=($result['email']) ? $result['email'] : '-'; ?></div>
	</dynamic>
    <dynamic name="fax" <?= $fax;?> >
    	<div class="col-md-2"><strong>Fax:</strong></div>
    	<div class="col-md-4"><?=($result['fax']) ? $result['fax'] : '-'; ?></div>
	</dynamic>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <dynamic name="city" <?= $city;?> >
    	<div class="col-md-2"><strong>City:</strong></div>
    	<div class="col-md-4"><?=($result['city']) ? $result['city'] : '-'; ?></div>
	</dynamic>
	<dynamic name="cityarea" <?= $cityarea;?> >
    	<div class="col-md-2"><strong>City Area:</strong></div>
    	<div class="col-md-4"><?=($result['cityarea']) ? $result['cityarea'] : '-'; ?></div>
	</dynamic>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
	<dynamic name="country" <?= $country;?> >
    	<div class="col-md-2"><strong>Country:</strong></div>
    	<div class="col-md-4"><?=($result['country']) ? $result['country'] : '-'; ?></div>
	</dynamic>
	<dynamic name="etype" <?= $etype;?> >
    	<div class="col-md-2"><strong>Type:</strong></div>
    	<div class="col-md-4"><?=($result['etype']) ? $result['etype'] : '-'; ?></div>
    </dynamic>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Default Date:</strong></div>
    <div class="col-md-4"><?=($result['default_date']) ? date('d M Y', strtotime(substr($result['default_date'], 0, 11))) : '-'; ?></div>
    <div class="col-md-2"><strong>Created At:</strong></div>
    <div class="col-md-4"><?=($result['created_at']) ? date('d M Y', strtotime(substr($result['created_at'], 0, 11))) : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>