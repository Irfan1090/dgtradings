<div class="page-content">
    
    <!-- Begin: Demo Datatable 1 -->
    <div class="portlet light portlet-fit portlet-datatable bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-settings font-dark font-blue-custom"></i>
                <span class="caption-subject font-dark font-blue-custom sbold uppercase">Warehouses Listing</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    
                    <a href="<?= base_url(); ?>Warehouse/addEditWarehouse" class="dt-button btn green buttons-html5 btn-outline" ><span>Add New Warehouse</span></a>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <div class="table-actions-wrapper">
                    <span> </span>
                    <a class="dt-button buttons-html5 btn red btn-outline disabled btnMultiDelete"><span>Delete</span></a>    
                    <a class="tool-action dt-button buttons-csv buttons-html5 btn purple btn-outline" tabindex="0" data-action="4"><span>CSV</span></a>
                    <a class="tool-action dt-button buttons-excel buttons-html5 btn green btn-outline" tabindex="0" data-action="3"><span>Excel</span></a> 
                    <a class="tool-action dt-button buttons-pdf buttons-html5 btn blue btn-outline" tabindex="0" data-action="2"><span>Pdf</span></a> 
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable" id="warehouseTable">
                    <thead>
                        <tr role="row" class="heading">
                            
                            <th width="2%">
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                    <span></span>
                                </label>
                            </th>
                            <th width=""> Sr&nbsp;# </th>
                            <th width=""> Name </th>
                            <th width=""> Description </th>
                            <th width=""> Created at </th>
                            <th style="min-width:105px;"> Actions </th>
                        </tr>
                        <tr role="row" class="filter">
                            <td> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="did"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="name"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="description"> </td>
                            <td>
                                <div class="input-group date date-picker margin-bottom-5" data-date-format="dd M yyyy">
                                    <input type="text" class="form-control form-filter input-sm" readonly name="created_at_from" placeholder="From">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                                <div class="input-group date date-picker" data-date-format="dd M yyyy">
                                    <input type="text" class="form-control form-filter input-sm" readonly name="created_at_to" placeholder="To">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </td>
                            <td>
                                <div class="text-center">
                                    <div class="margin-bottom-5">
                                        <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                            <i class="fa fa-search"></i> Search</button>
                                    </div>
                                    <button class="btn btn-sm red btn-outline filter-cancel">
                                        <i class="fa fa-times"></i> Reset</button>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody> </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailPopup" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>Warehouse Details</strong></h4>
            </div>
            <div class="modal-body">
                <!-- <div class="row"><div class="col-md-12"><br></div></div>  -->
                <div class="row">
                    <div class="col-md-12">
                        <div id="loader" class="text-center" style="display: none;">
                            <br>
                            <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>
                        </div>
                    </div>
                </div>
                
                <div class="customModalBody"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
$(document).ready(function(){

    var grid = new Datatable();
    grid.init({
        src: $("#warehouseTable"),
        onSuccess: function (grid, response) {
            // grid:        grid object
            // response:    json object of server side ajax response
            // execute some code after table records loaded
        },
        onError: function (grid) {
            // execute some code on network or other general error  
        },
        onDataLoad: function(grid) {
            // execute some code on ajax data load
        },
        loadingMessage: 'Loading...',
        dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
            
            // save datatable state(pagination, sort, etc) in cookie.
            "bStateSave": true, 

             // save custom filters to the state
            "fnStateSaveParams":    function ( oSettings, sValue ) {
                $("#warehouseTable tr.filter .form-control").each(function() {
                    sValue[$(this).attr('name')] = $(this).val();
                });
               
                return sValue;
            },

            // read the custom filters from saved state and populate the filter inputs
            "fnStateLoadParams" : function ( oSettings, oData ) {
                //Load custom filters
                $("#warehouseTable tr.filter .form-control").each(function() {
                    var element = $(this);
                    if (oData[element.attr('name')]) {
                        element.val( oData[element.attr('name')] );
                    }
                });
                
                return true;
            },

            "lengthMenu": [
                [10, 20, 50, 100, 150, -1],
                [10, 20, 50, 100, 150, "All"] // change per page values here
            ],
            "pageLength": 10, // default record count per page
            "ajax": {
                "url": baseUrl+"warehouse/getWarehouses", // ajax source
            },
            "ordering": true,
            "order": [
                [1, "asc"]
            ],// set first column as a default sort by asc
            "columnDefs": [{  // set default column settings
                'orderable': false,
                'targets': [0,5]
            }, {
                "searchable": false,
                "targets": [0,5]
            },
            {
                "className": 'text-center',
                "targets": [0,5]
            }], buttons: [
                { extend: 'print', exportOptions: {columns: [1,2,3,4]}, className: 'btn default' },
                { extend: 'copy', exportOptions: {columns: [1,2,3,4]}, className: 'btn default' },
                { extend: 'pdf', exportOptions: {columns: [1,2,3,4]}, className: 'btn default' },
                { extend: 'excel', exportOptions: {columns: [1,2,3,4]}, className: 'btn default' },
                { extend: 'csv', exportOptions: {columns: [1,2,3,4]}, className: 'btn default' },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function ( e, dt, node, config ) {
                        dt.ajax.reload();
                        //alert('Datatable reloaded!');
                    }
                }
            ],
        }
    });

    $('a.tool-action').on('click', function() {

        var action = $(this).attr('data-action');
        grid.getDataTable().button(action).trigger();
    });

    $("#warehouseTable").keypress(function(e) {

        if(e.which == 13) {
           
            $(".filter-submit").click();
        }
    });

    $('#warehouseTable').find('.group-checkable').change(function () {

        var checked = jQuery(this).is(":checked");
        if (checked) {
            
            $('.btnMultiDelete').removeClass('disabled');
        } else {
            
            $('.btnMultiDelete').addClass('disabled');
        }
    });


    $('#warehouseTable').on('click', 'tr .checkboxes', function () {


        var checkedVals = $('.checkboxes:checkbox:checked').map(function() {
            return this.value;
        }).get();

        if(checkedVals.length > 0){

            $('.btnMultiDelete').removeClass('disabled');
        }else{

            $('.btnMultiDelete').addClass('disabled');
        }
    });

    $('#warehouseTable').on('click', '.btnDelete', function(e){

        e.preventDefault();
        var dId = $(this).attr("data-did");

        swal({
            title: "Are you sure you want to delete this warehouse?",
            //text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger btn-outline",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: baseUrl+"warehouse/deleteWarehouses",
                    dataType: "json",
                    data: {d_ids : dId},
                    success: function(data){
                        if(data){

                            swal("Deleted!", "Warehouse deleted successfully!", "success");
                            grid.getDataTable().button(5).trigger();
                        }else{

                            swal("Error", "An internal error occur, please try again later.", "error");
                        }
                    }
                });
            }
        });
    });

    $('.btnMultiDelete').on('click', function(e){

        e.preventDefault();
        var checkedVals = $('.checkboxes:checkbox:checked').map(function() {
            return this.value;
        }).get();

        swal({
            title: "Are you sure you want to delete "+checkedVals.length+" selected warehouses?",
            //text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger btn-outline",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: baseUrl+"warehouse/deleteWarehouses",
                    dataType: "json",
                    data: {d_ids : checkedVals},
                    success: function(data){
                        if(data){

                            swal("Deleted!", checkedVals.length+" warehouses deleted successfully!", "success");
                            grid.getDataTable().button(5).trigger();
                        }else{

                            swal("Error", "An internal error occur, please try again later.", "error");
                        }
                    }
                });
            }
        });
    });

    $('#warehouseTable').on('click', '.detailPopup', function(e){

        e.preventDefault();
        $('.customModalBody').html('');
        $('#loader').show();
        var dId = $(this).attr("data-did");
        $.ajax({
            type: "POST",
            url: baseUrl+"warehouse/getWarehouseDetail",
            dataType: "json",
            data: {d_id : dId},
            success: function(html){
                
                $('#loader').hide();
                $('.customModalBody').append(html);
            }
        });
    });
});
</script>