<div class="page-content">
    <!-- <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Dashboard</span>
            </li>
        </ul>
    </div> -->
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->

    <!-- BEGIN VALIDATION STATES-->

    <div class="portlet light portlet-fit portlet-form bordered" style="">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-bubble font-blue-custom"></i>
                <span class="caption-subject font-blue-custom bold uppercase">Add New Warehouse</span>
            </div>
            <div class="actions">
                <button class="btn buttons-html5 dt-button green btn-outline btnSave"><i class="fa fa-save"></i> Save F10</button>
                <button class="btn buttons-html5 dt-button green btn-outline btnSaveAndMore"><i class="fa fa-save"></i> Save and New F11</button>
                <button class="btn buttons-html5 dt-button yellow btn-outline btnReset"><i class="fa fa-refresh"></i> Reset F5</button>
            </div>
        </div>
        <div class="portlet-body">
            <form action="#" id="formAddWarehouse" class="form-horizontal">
                <div class="form-body">
                     
                    <div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                    <div class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                    <div class="alert error-message display-hide">
                        <button class="close" data-close="alert"></button><span class="error-message-body"> You have some form errors. Please check below. </span></div>
                    <div class="alert success-message display-hide">
                        <button class="close" data-close="alert"></button><span class="success-message-body"> Your form validation is successful! </span></div>
                    
                    <div class="row margin-top-20">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6">ID(Auto)
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-6">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="number" class="form-control" name="warehouseId" id="warehouseId" min="0" /> 
                                        <input type="hidden" class="saveColumn" id="warehouseIdHidden" name="did">
                                        <input type="hidden" id="maxWarehouseIdHidden">
                                        <input type="hidden" id="voucherTypeHidden" value="new">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Name
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="text" class="form-control saveColumn" name="name" /> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Description</label>
                                <div class="col-md-8">
                                    <textarea class="form-control saveColumn" name="description" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn green btnSave2"><i class="fa fa-save"></i> Save F10</button>
                            <button class="btn green btnSaveAndMore2"><i class="fa fa-save"></i> Save and New F11</button>
                            <button class="btn yellow btnReset2"><i class="fa fa-refresh"></i> Reset F5</button>
                            <a href="<?= base_url(); ?>warehouse" data-url="warehouse" class="btn default btnBack"><i class="fa fa-step-backward"></i> Cancel F2</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END VALIDATION STATES-->
</div>