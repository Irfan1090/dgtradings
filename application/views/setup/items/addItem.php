<div class="page-content">
    <div class="portlet light portlet-fit portlet-form bordered" style="">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-bubble font-blue-custom"></i>
                <span class="caption-subject font-blue-custom bold uppercase">Add New Item</span>
            </div>
            <div class="actions">
                
                <button class="btn buttons-html5 dt-button green btn-outline btnSave"><i class="fa fa-save"></i> Save F10</button>
                <button class="btn buttons-html5 dt-button green btn-outline btnSaveAndMore"><i class="fa fa-save"></i> Save and New F11</button>
                <button class="btn buttons-html5 dt-button yellow btn-outline btnReset"><i class="fa fa-refresh"></i> Reset F5</button>
            </div>
        </div>
        <div class="portlet-body">
            <div class="tab-custom">
                <form action="#" id="formAddItem" class="form-horizontal">
                    <div class="form-body">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#basicInfo" data-toggle="tab" aria-expanded="true">Basic Information</a></li>
                            <li><a href="#detailInfo" data-toggle="tab" aria-expanded="true">Detail Information</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="basicInfo">
                                 
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                                <div class="alert error-message display-hide">
                                    <button class="close" data-close="alert"></button><span class="error-message-body"> You have some form errors. Please check below. </span></div>
                                <div class="alert success-message display-hide">
                                    <button class="close" data-close="alert"></button><span class="success-message-body"> Your form validation is successful! </span></div>
                                
                                <div class="row margin-top-20">
                                    
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">ID(Auto)
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <div class="input-group">
                                                            <div class="input-icon right">
                                                                <i class="fa"></i>
                                                                <input type="number" class="form-control" name="itemId" id="itemId" min="0" /> 
                                                                <input type="hidden" id="maxItemIdHidden">
                                                                <input type="hidden" class="saveColumn" id="itemIdHidden" name="item_id">
                                                                <input type="hidden" id="voucherTypeHidden" value="new">
                                                            </div>
                                                            <span class="input-group-addon blue">
                                                                <a href="#itemsLookup" class="" data-toggle="modal">
                                                                    <i class="fa fa-search font-blue"></i></a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Item Code
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="input-icon right">
                                                            <i class="fa"></i>
                                                            <select id="itemCode" class="form-control select2 saveColumn" name="item_code" data-error-container="#select2_error">
                                                                <option value="" disabled="" selected="">Choose Item Code</option>
                                                                <?php foreach ($items as $item): ?>
                                                                    <option value="<?php echo $item['item_id']; ?>"><?php echo $item['item_code']; ?></option>
                                                                <?php endforeach ?>
                                                            </select>
                                                            <div id="select2_error"> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Category
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="input-icon right">
                                                            <i class="fa"></i>
                                                            <select id="single" class="form-control select2 saveColumn" name="catid" data-error-container="#select2_error">
                                                                <option value="" disabled="" selected="">Choose Category</option>
                                                                <?php foreach ($categories as $category): ?>
                                                                    <option value="<?php echo $category['catid']; ?>"><?php echo $category['name']; ?></option>
                                                                <?php endforeach ?>
                                                            </select>
                                                            <div id="select2_error"> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Subcategory</label>
                                                    <div class="col-md-8">
                                                        <div class="input-icon right">
                                                            <i class="fa"></i>
                                                            <select id="single" class="form-control select2 saveColumn" name="subcatid" data-error-container="#select2_error">
                                                                <option value="" disabled="" selected="">Choose Subcategory</option>
                                                                <?php foreach ($subcategories as $subcategory): ?>
                                                                    <option value="<?php echo $subcategory['subcatid']; ?>"><?php echo $subcategory['name']; ?></option>
                                                                <?php endforeach ?>
                                                            </select>
                                                            <div id="select2_error"> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Brand</label>
                                                    <div class="col-md-8">
                                                        <div class="input-icon right">
                                                            <i class="fa"></i>
                                                            <select id="single" class="form-control select2 saveColumn" name="bid" data-error-container="#select2_error">
                                                                <option value="" disabled="" selected="">Choose Brand</option>
                                                                <?php foreach ($brands as $brand): ?>
                                                                    <option value="<?php echo $brand['bid']; ?>"><?php echo $brand['name']; ?></option>
                                                                <?php endforeach ?>
                                                            </select>
                                                            <div id="select2_error"> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Made</label>
                                                    <div class="col-md-8">
                                                        <div class="input-icon right">
                                                            <i class="fa"></i>
                                                            <select id="single" class="form-control select2 saveColumn" name="made_id" data-error-container="#select2_error">
                                                                <option value="" disabled="" selected="">Choose Made</option>
                                                                <?php foreach ($mades as $made): ?>
                                                                    <option value="<?php echo $made['made_id']; ?>"><?php echo $made['name']; ?></option>
                                                                <?php endforeach ?>
                                                            </select>
                                                            <div id="select2_error"> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Short Code</label>
                                                    <div class="col-md-8">
                                                        <div class="input-icon right">
                                                            <i class="fa"></i>
                                                            <input type="text" class="form-control saveColumn" name="short_code" /> </div>
                                                    </div>
                                                </div>  
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Bar Code</label>
                                                    <div class="col-md-8">
                                                        <div class="input-icon right">
                                                            <i class="fa"></i>
                                                            <input type="text" class="form-control saveColumn" name="item_barcode" /> </div>
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                <label class="control-label col-md-2">Description</label>
                                                <div class="col-md-10">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control saveColumn" name="item_des" /> 
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">UOM</label>
                                                    <div class="col-md-8">
                                                        <div class="input-icon right">
                                                            <i class="fa"></i>
                                                            <input type="text" class="form-control saveColumn" name="uom" /> </div>
                                                    </div>
                                                </div>  
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Purchase Rate</label>
                                                    <div class="col-md-8">
                                                        <div class="input-icon right">
                                                            <i class="fa"></i>
                                                            <input type="text" class="form-control saveColumn" name="cost_price" /> </div>
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Sale Rate1</label>
                                                    <div class="col-md-8">
                                                        <div class="input-icon right">
                                                            <i class="fa"></i>
                                                            <input type="text" class="form-control saveColumn" name="srate" /> </div>
                                                    </div>
                                                </div>  
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Default Date</label>
                                                    <div class="col-md-8">
                                                        <div class="input-icon right">
                                                            <i class="fa"></i>
                                                            <div class="input-group date date-picker margin-bottom-5" data-date-format="dd M yyyy">
                                                                <input type="text" class="form-control form-filter saveColumn" readonly name="open_date" placeholder="From">
                                                                <span class="input-group-btn">
                                                                    <button class="btn btn-sm default" type="button">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="<?=base_url();?>assets/uploads/items/no-image.png" id="imgUrl" alt="" />
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
                                            </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> Select image </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="photo" class="saveColumn" id="photos"> </span>
                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="tab-pane" id="detailInfo">
                                <div class="row margin-top-20">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Min Level</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control saveColumn" name="min_level" /> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Max Level</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control saveColumn" name="max_level" /> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Sale Price2</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control saveColumn" name="srate1" /> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Sale Price3</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control saveColumn" name="srate2" /> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Sale Price4</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control saveColumn" name="srate3" /> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Purchase Discount%</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control saveColumn" name="item_pur_discount" /> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Sale Discount%</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control saveColumn" name="item_discount" /> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Remarks</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control saveColumn" name="description" /> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn blue btnNext"><i class="fa fa-forward"></i></button>
                                <button class="btn blue btnPrev" style="display: none;"><i class="fa fa-backward"></i></button>
                                <button class="btn green btnSave2"><i class="fa fa-save"></i> Save F10</button>
                                <button class="btn green btnSaveAndMore2"><i class="fa fa-save"></i> Save and New F11</button>
                                <button class="btn yellow btnReset2"><i class="fa fa-refresh"></i> Reset F5</button>
                                <a href="<?= base_url(); ?>items" data-url="items" class="btn default btnBack"><i class="fa fa-step-backward"></i> Cancel F2</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END VALIDATION STATES-->
</div>

<div class="modal fade" id="categoryModal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>Add New Category</strong></h4>
            </div>
            <div class="modal-body">
                <form action="#" id="formAddCategory" class="form-horizontal">
                    <div class="form-body">
                         
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                        <div class="alert error-message display-hide">
                            <button class="close" data-close="alert"></button><span class="error-message-body"> You have some form errors. Please check below. </span></div>
                        <div class="alert success-message display-hide">
                            <button class="close" data-close="alert"></button><span class="success-message-body"> Your form validation is successful! </span></div>
                        
                        <div class="row margin-top-20">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Name
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-6">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="hidden" class="saveColumn" id="catIdHidden" value="25000" name="catid">
                                            <input type="hidden" id="voucherTypeHidden" value="new">
                                            <input type="text" class="form-control saveColumn" name="name" /> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Description</label>
                                    <div class="col-md-6">
                                        <textarea class="form-control saveColumn" name="description" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn green btnSaveCategory"><i class="fa fa-save"></i> Save</button>
                <button class="btn yellow btnResetCategory"><i class="fa fa-refresh"></i> Reset</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="subCategoryModal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>Add New Subcategory</strong></h4>
            </div>
            <div class="modal-body">
                <form action="#" id="formAddSubCategory" class="form-horizontal">
                    <div class="form-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                        <div class="alert error-message display-hide">
                            <button class="close" data-close="alert"></button><span class="error-message-body"> You have some form errors. Please check below. </span></div>
                        <div class="alert success-message display-hide">
                            <button class="close" data-close="alert"></button><span class="success-message-body"> Your form validation is successful! </span></div>
                        
                        <div class="row margin-top-20">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Name
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-6">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="hidden" class="saveColumn" id="subCatIdHidden" value="25000" name="subcatid">
                                            <input type="hidden" id="voucherTypeHidden" value="new">
                                            <input type="text" class="form-control saveColumn" name="name" /> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="single" class="control-label col-md-3">Category</label>
                                    <div class="col-md-6">
                                        <select id="single" class="form-control select2 saveColumn" name="catid">
                                            <option value="" disabled="" selected="">Choose Category</option>
                                            <?php foreach ($categories as $category): ?>
                                                <option value="<?php echo $category['catid']; ?>" ><?php echo $category['name'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Description</label>
                                    <div class="col-md-6">
                                        <textarea class="form-control saveColumn" name="description" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn green btnSaveSubCategory"><i class="fa fa-save"></i> Save</button>
                <button class="btn yellow btnResetSubCategory"><i class="fa fa-refresh"></i> Reset</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="brandModal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>Add New Brand</strong></h4>
            </div>
            <div class="modal-body">
                <form action="#" id="formAddBrand" class="form-horizontal">
                    <div class="form-body">
                         
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                        <div class="alert error-message display-hide">
                            <button class="close" data-close="alert"></button><span class="error-message-body"> You have some form errors. Please check below. </span></div>
                        <div class="alert success-message display-hide">
                            <button class="close" data-close="alert"></button><span class="success-message-body"> Your form validation is successful! </span></div>
                        
                        <div class="row margin-top-20">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Name
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-6">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="hidden" class="saveColumn" id="brandIdHidden" value="25000" name="bid">
                                            <input type="hidden" id="voucherTypeHidden" value="new">
                                            <input type="text" class="form-control saveColumn" name="name" /> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Description</label>
                                    <div class="col-md-6">
                                        <textarea class="form-control saveColumn" name="description" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn green btnSaveBrand"><i class="fa fa-save"></i> Save</button>
                <button class="btn yellow btnResetBrand"><i class="fa fa-refresh"></i> Reset</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="madeModal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>Add New Made</strong></h4>
            </div>
            <div class="modal-body">
                <form action="#" id="formAddMade" class="form-horizontal">
                    <div class="form-body">
                         
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                        <div class="alert error-message display-hide">
                            <button class="close" data-close="alert"></button><span class="error-message-body"> You have some form errors. Please check below. </span></div>
                        <div class="alert success-message display-hide">
                            <button class="close" data-close="alert"></button><span class="success-message-body"> Your form validation is successful! </span></div>
                        
                        <div class="row margin-top-20">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Name
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-6">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="hidden" class="saveColumn" id="madeIdHidden" value="25000" name="made_id">
                                            <input type="hidden" id="voucherTypeHidden" value="new">
                                            <input type="text" class="form-control saveColumn" name="name" /> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Description</label>
                                    <div class="col-md-6">
                                        <textarea class="form-control saveColumn" name="description" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn green btnSaveMade"><i class="fa fa-save"></i> Save</button>
                <button class="btn yellow btnResetMade"><i class="fa fa-refresh"></i> Reset</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>