<h5><strong>Basic Information:</strong></h5>
<div class="row">
    <div class="col-md-2"><strong>Item Id:</strong></div>
    <div class="col-md-4"><?=($result['item_id']) ? $result['item_id'] : '-'; ?></div>
    <div class="col-md-2"><strong>Item Code:</strong></div>
    <div class="col-md-4"><?=($result['item_code']) ? $result['item_code'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Category:</strong></div>
    <div class="col-md-4"><?=($result['cat_name']) ? $result['cat_name'] : '-'; ?></div>
    <div class="col-md-2"><strong>Subcategory:</strong></div>
    <div class="col-md-4"><?=($result['sub_cat_name']) ? $result['sub_cat_name'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Brand:</strong></div>
    <div class="col-md-4"><?=($result['brand_name']) ? $result['brand_name'] : '-'; ?></div>
    <div class="col-md-2"><strong>Made:</strong></div>
    <div class="col-md-4"><?=($result['made_name']) ? $result['made_name'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Shor Code:</strong></div>
    <div class="col-md-4"><?=($result['short_code']) ? $result['short_code'] : '-'; ?></div>
    <div class="col-md-2"><strong>Bar Code:</strong></div>
    <div class="col-md-4"><?=($result['item_barcode']) ? $result['item_barcode'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Description:</strong></div>
    <div class="col-md-10"><?=($result['item_des']) ? $result['item_des'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Uom:</strong></div>
    <div class="col-md-4"><?=($result['uom']) ? $result['uom'] : '-'; ?></div>
    <div class="col-md-2"><strong>Purchase Price:</strong></div>
    <div class="col-md-4"><?=($result['cost_price']) ? $result['cost_price'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Sale Price1:</strong></div>
    <div class="col-md-4"><?=($result['srate']) ? $result['srate'] : '-'; ?></div>
    <div class="col-md-2"><strong>Default Date:</strong></div>
    <div class="col-md-4"><?=($result['open_date']) ? $result['open_date'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<h5><strong>Detailed Information:</strong></h5>
<div class="row">
    <div class="col-md-2"><strong>Min Level:</strong></div>
    <div class="col-md-4"><?=($result['min_level']) ? $result['min_level'] : '-'; ?></div>
    <div class="col-md-2"><strong>Max Level:</strong></div>
    <div class="col-md-4"><?=($result['max_level']) ? $result['max_level'] : '-'; ?></div>
</div>
<div class="row">
    <div class="col-md-2"><strong>Sale Price2:</strong></div>
    <div class="col-md-4"><?=($result['srate1']) ? $result['srate1'] : '-'; ?></div>
    <div class="col-md-2"><strong>Sale Price3:</strong></div>
    <div class="col-md-4"><?=($result['srate2']) ? $result['srate2'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Sale Price4:</strong></div>
    <div class="col-md-4"><?=($result['srate3']) ? $result['srate3'] : '-'; ?></div>
    <div class="col-md-2"><strong>Purchase Discount%:</strong></div>
    <div class="col-md-4"><?=($result['item_pur_discount']) ? $result['item_pur_discount'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Sale Discount%:</strong></div>
    <div class="col-md-4"><?=($result['item_discount']) ? $result['item_discount'] : '-'; ?></div>
    <div class="col-md-2"><strong>Remarks:</strong></div>
    <div class="col-md-4"><?=($result['description']) ? $result['description'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>