<div class="row">
    <div class="col-md-2"><strong>Customer Type:</strong></div>
    <div class="col-md-4"><?=($result['ctypeid']) ? $result['ctypeid'] : '-'; ?></div>
    <div class="col-md-2"><strong>Name:</strong></div>
    <div class="col-md-4"><?=($result['name']) ? $result['name'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Created At:</strong></div>
    <div class="col-md-4"><?=($result['created_at']) ? date('d M Y', strtotime(substr($result['created_at'], 0, 11))) : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>