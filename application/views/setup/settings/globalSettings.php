<div class="page-content">
    <!-- <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Dashboard</span>
            </li>
        </ul>
    </div> -->
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->

    <!-- BEGIN VALIDATION STATES-->

    <div class="portlet light portlet-fit portlet-form bordered" style="">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-bubble font-blue-custom"></i>
                <span class="caption-subject font-blue-custom bold uppercase">Global Settings</span>
            </div>
            <div class="actions">
                
                <button class="btn buttons-html5 dt-button green btn-outline btnSave"><i class="fa fa-save"></i> Save F10</button>
                <button class="btn buttons-html5 dt-button yellow btn-outline btnReset"><i class="fa fa-refresh"></i> Reset F5</button>
            </div>
        </div>
        <div class="portlet-body">
            <div class="tab-custom">
                <form action="#" id="formAddGlobalSettings" class="form-horizontal">
                    <div class="form-body">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#generalSettings" data-toggle="tab" aria-expanded="true">General Settings</a></li>
                            <li><a href="#accountSettings" data-toggle="tab" aria-expanded="true">Account Settings</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="generalSettings">
                                 
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                                <div class="alert error-message display-hide">
                                    <button class="close" data-close="alert"></button><span class="error-message-body"> You have some form errors. Please check below. </span></div>
                                <div class="alert success-message display-hide">
                                    <button class="close" data-close="alert"></button><span class="success-message-body"> Your form validation is successful! </span></div>
                                
                                <div class="panel panel-info"> 
                                    <div class="panel-heading"> 
                                        <h3 class="panel-title">Item Name Settings</h3> 
                                    </div> 
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="control-label">First Name</label>
                                                <select id="single" class="form-control select2 saveColumn" name="item_f_name">
                                                    <option value="" disabled="" selected="">Choose First Name</option>
                                                    <option value="1" <?=($settings['item_f_name'] == 1) ? 'selected' : '';?>>Category</option>
                                                    <option value="2" <?=($settings['item_f_name'] == 2) ? 'selected' : '';?>>Sub Category</option>
                                                    <option value="3" <?=($settings['item_f_name'] == 3) ? 'selected' : '';?>>Brand</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Middle Name</label>
                                                <select id="single" class="form-control select2 saveColumn" name="item_m_name">
                                                    <option value="" disabled="" selected="">Choose Middle Name</option>
                                                    <option value="1" <?=($settings['item_m_name'] == 1) ? 'selected' : '';?>>Category</option>
                                                    <option value="2" <?=($settings['item_m_name'] == 2) ? 'selected' : '';?>>Sub Category</option>
                                                    <option value="3" <?=($settings['item_m_name'] == 3) ? 'selected' : '';?>>Brand</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Last Name</label>
                                                <select id="single" class="form-control saveColumn select2" name="item_l_name">
                                                    <option value="" disabled="" selected="">Choose Last Name</option>
                                                    <option value="1" <?=($settings['item_l_name'] == 1) ? 'selected' : '';?>>Category</option>
                                                    <option value="2" <?=($settings['item_l_name'] == 2) ? 'selected' : '';?>>Sub Category</option>
                                                    <option value="3" <?=($settings['item_l_name'] == 3) ? 'selected' : '';?>>Brand</option>
                                                </select>
                                            </div>
                                            <div class="col-md-1">
                                                <label class="control-label"></label>
                                                <input type="button" class="form-control btn blue btn-outline btnClearSettings" value="Clear">
                                            </div>
                                        </div>
                                    </div> 
                                </div>

                                <div class="panel panel-info"> 
                                    <div class="panel-heading"> 
                                        <h3 class="panel-title">Freight Settings</h3> 
                                    </div> 
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="control-label">Sale Voucher</label>
                                                <select id="single" class="form-control saveColumn select2" name="sale_frate">
                                                    <option value="" disabled="" selected="">Choose Status</option>
                                                    <option value="1" <?=($settings['sale_frate'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['sale_frate'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Sale Return</label>
                                                <select id="single" class="form-control saveColumn select2" name="sale_return_frate">
                                                    <option value="" disabled="" selected="">Choose Status</option>
                                                    <option value="1" <?=($settings['sale_return_frate'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['sale_return_frate'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Purchase Vouchers</label>
                                                <select id="single" class="form-control saveColumn select2" name="purchase_frate">
                                                    <option value="" disabled="" selected="">Choose Status</option>
                                                    <option value="1" <?=($settings['purchase_frate'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['purchase_frate'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Purhcase Return</label>
                                                <select id="single" class="form-control saveColumn select2" name="purchase_return_frate">
                                                    <option value="" disabled="" selected="">Choose Status</option>
                                                    <option value="1" <?=($settings['purchase_return_frate'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['purchase_return_frate'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div> 
                                </div>

                                <div class="panel panel-info"> 
                                    <div class="panel-heading"> 
                                        <h3 class="panel-title">Other Settings</h3> 
                                    </div> 
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="control-label">Date Setting</label>
                                                <select id="single" class="form-control saveColumn select2" name="date_setting">
                                                    <option value="" disabled="" selected="">Choose Setting</option>
                                                    <option value="1" <?=($settings['date_setting'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['date_setting'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Print Prompt After Save</label>
                                                <select id="single" class="form-control saveColumn select2" name="prompt_after_save">
                                                    <option value="" disabled="" selected="">Choose Print Prompt Setting</option>
                                                    <option value="1" <?=($settings['prompt_after_save'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['prompt_after_save'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Ledger Entry</label>
                                                <select id="single" class="form-control saveColumn select2" name="ledger_entry">
                                                    <option value="" disabled="" selected="">Choose Ledger Entry Type</option>
                                                    <option value="1" <?=($settings['ledger_entry'] == 1) ? 'selected' : '';?>>Sigle</option>
                                                    <option value="2" <?=($settings['ledger_entry'] == 2) ? 'selected' : '';?>>For each item</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Ledger Narration</label>
                                                <select id="single" class="form-control saveColumn select2" name="ledger_narration">
                                                    <option value="" disabled="" selected="">Choose Ledger Narration</option>
                                                    <option value="1" <?=($settings['ledger_narration'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['ledger_narration'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="control-label">Invoicing Without Stock</label>
                                                <select id="single" class="form-control saveColumn select2" name="invoicing_without_stock">
                                                    <option value="" disabled="" selected="">Choose Status</option>
                                                    <option value="1" <?=($settings['invoicing_without_stock'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['invoicing_without_stock'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">No of Descimals</label>
                                                <select id="single" class="form-control saveColumn select2" name="no_of_decimals">
                                                    <option value="" disabled="" selected="">Choose No Of Decimals</option>
                                                    <option value="1" <?=($settings['no_of_decimals'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['no_of_decimals'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Sale Print Default Setting</label>
                                                <select id="single" class="form-control saveColumn select2" name="sale_print_setting">
                                                    <option value="" disabled="" selected="">Choose Print Setting</option>
                                                    <option value="1" <?=($settings['sale_print_setting'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['sale_print_setting'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Barcode scan activity</label>
                                                <select id="single" class="form-control saveColumn select2" name="barcode_scan_activity">
                                                    <option value="" disabled="" selected="">Choose Barcode Setting</option>
                                                    <option value="1" <?=($settings['barcode_scan_activity'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['barcode_scan_activity'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="control-label">Default Stock Location</label>
                                                <select id="single" class="form-control saveColumn select2" name="stock_location">
                                                    <option value="" disabled="" selected="">Choose Stock Location </option>
                                                    <option value="1" <?=($settings['stock_location'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['stock_location'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Default Thermal Printer</label>
                                                <select id="single" class="form-control saveColumn select2" name="thermal_printer">
                                                    <option value="" disabled="" selected="">Choose Thermal Print Setting</option>
                                                    <option value="1" <?=($settings['thermal_printer'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['thermal_printer'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Default A4 Printer</label>
                                                <select id="single" class="form-control saveColumn select2" name="a4_printer">
                                                    <option value="" disabled="" selected="">Choose A4 Print Setting</option>
                                                    <option value="1" <?=($settings['a4_printer'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['a4_printer'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Default Barcode Printer</label>
                                                <select id="single" class="form-control saveColumn select2" name="barcode_printer">
                                                    <option value="" disabled="" selected="">Choose Barcode Print Setting</option>
                                                    <option value="1" <?=($settings['barcode_printer'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['barcode_printer'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-md-3">
                                                <label class="control-label">Default Credit Card</label>
                                                <select id="single" class="form-control saveColumn select2" name="credit_card">
                                                    <option value="" disabled="" selected="">Choose Default Credit Card</option>
                                                    <option value="1" <?=($settings['credit_card'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['credit_card'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                            <div class="tab-pane" id="accountSettings">
                                <div class="panel panel-info"> 
                                    <div class="panel-heading"> 
                                        <h3 class="panel-title">Account Mapping</h3> 
                                    </div> 
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="control-label">Customer</label>
                                                <select id="single" class="form-control saveColumn select2" name="customer">
                                                    <option value="" disabled="" selected="">Choose </option>
                                                    <option value="1" <?=($settings['customer'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['customer'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Supplier</label>
                                                <select id="single" class="form-control saveColumn select2" name="suplier">
                                                    <option value="" disabled="" selected="">Choose </option>
                                                    <option value="1" <?=($settings['suplier'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['suplier'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Bank</label>
                                                <select id="single" class="form-control saveColumn select2" name="bank">
                                                    <option value="" disabled="" selected="">Choose </option>
                                                    <option value="1" <?=($settings['bank'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['bank'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Expense</label>
                                                <select id="single" class="form-control saveColumn select2" name="expense">
                                                    <option value="" disabled="" selected="">Choose </option>
                                                    <option value="1" <?=($settings['expense'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['expense'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="control-label">Income</label>
                                                <select id="single" class="form-control saveColumn select2" name="income">
                                                    <option value="" disabled="" selected="">Choose </option>
                                                    <option value="1" <?=($settings['income'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['income'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Employee</label>
                                                <select id="single" class="form-control saveColumn select2" name="employee">
                                                    <option value="" disabled="" selected="">Choose </option>
                                                    <option value="1" <?=($settings['employee'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['employee'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div> 
                                </div>

                                <div class="panel panel-info"> 
                                    <div class="panel-heading"> 
                                        <h3 class="panel-title">Other Account Default Settings</h3> 
                                    </div> 
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="control-label">Cash Account</label>
                                                <select id="single" class="form-control saveColumn select2" name="cash">
                                                    <option value="" disabled="" selected="">Choose </option>
                                                    <option value="1" <?=($settings['cash'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['cash'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Discount Account</label>
                                                <select id="single" class="form-control saveColumn select2" name="discount">
                                                    <option value="" disabled="" selected="">Choose </option>
                                                    <option value="1" <?=($settings['discount'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['discount'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Freight Account</label>
                                                <select id="single" class="form-control saveColumn select2" name="freight">
                                                    <option value="" disabled="" selected="">Choose </option>
                                                    <option value="1" <?=($settings['freight'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['freight'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Tax Account</label>
                                                <select id="single" class="form-control saveColumn select2" name="tax">
                                                    <option value="" disabled="" selected="">Choose </option>
                                                    <option value="1" <?=($settings['tax'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['tax'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="control-label">Point Of Sale Cash Account</label>
                                                <select id="single" class="form-control saveColumn select2" name="sale_cash">
                                                    <option value="" disabled="" selected="">Choose </option>
                                                    <option value="1" <?=($settings['sale_cash'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['sale_cash'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Employee Salary Account</label>
                                                <select id="single" class="form-control saveColumn select2" name="employee_salary">
                                                    <option value="" disabled="" selected="">Choose </option>
                                                    <option value="1" <?=($settings['employee_salary'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['employee_salary'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Default Inventory Account</label>
                                                <select id="single" class="form-control saveColumn select2" name="default_inventory">
                                                    <option value="" disabled="" selected="">Choose </option>
                                                    <option value="1" <?=($settings['default_inventory'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['default_inventory'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Default Income Account</label>
                                                <select id="single" class="form-control saveColumn select2" name="default_income">
                                                    <option value="" disabled="" selected="">Choose </option>
                                                    <option value="1" <?=($settings['default_income'] == 1) ? 'selected' : '';?>>Add</option>
                                                    <option value="2" <?=($settings['default_income'] == 2) ? 'selected' : '';?>>Less</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn green btnSave2"><i class="fa fa-save"></i> Save F10</button>
                                <button class="btn yellow btnReset2"><i class="fa fa-refresh"></i> Reset F5</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END VALIDATION STATES-->
</div>