 <?php   $controller = $arr['controller'];
            $big = $arr['big'];
            $small = $controller;
            $sbig = $arr['sbig'];
    ?>
<div class="page-content">

        <!--
                _______________________FOR EVERY NEW DEVELOPER________________________________________________________________________________________________________________
               /   
    IMPORTANT NOTE: (((( ATTRUBUTE listattribute IS NECESSARY  TO GET LISTING PAGE SHOWN )))) ((((( DEFAULT DATE DOES NOT NEED TO HAVE LISTATTRIBUTE  )))))
        /        :::   AND WITH (   THAT IS NECESSARY FOR LISTING PAGE) listattribute='Name' 
       /                YOU CAN GIVE NEW ATTRIBUTE TO AVOID ANY AMBIGOUS COLUMN NAME ERROR e.g ( ambigousname='party.name' )
      /___________AND NAME ATTRIBUTE VALUE ALWAYS SHOULD BE THE DATABASE COLUMN NAME__________________________________________________________________
     -->

    <!-- ____________________________________________________________________REQUIRE ATTRIBUTE INFORMATION____________________________________________________-->
      
    <!-- ( require="" ) ATTRIBUTE IN BELOW EXAMPLE FIRST PARAMETER IS ( 'minlength' )
                                                        /__ E.G IF GIVEN VALUE IS 5 THEN WRITING 'UMAR' WILL BE INVALID BECAUSE LENGTH OF 'U'M'A'R' IS 4 NOT 5 
                                                       /___ AND 2ND COMMA SEPARATED PARAMETER IS ('min') e.g VALUE SHOULD NOT BE LESS THAN GIVEN VALUE 
                                                      /_____IF GIVEN VALUE IS 1 THEN WRITING 0 WILL BE INVALID BECAUSE 0 IS SMALLER THAN 1 
                                                    /_______IF YOU don't want to give ('min') PROPERTY THEN GIVE JUST ('minlength') VALUE 
                                                        IF YOU JUST WRITE ( require ) attribute having no value then by default value will become ( require="1" means 'minlength' value )
                                                        If YOU GIVE (' min' ) PROPERTY TO ALPHANUMERIC FIELD THEN IT WILL BE INVALID IT WILL JUST ACCEPT THE NUMERIC VALUE NOT ALPHANUMERIC -->
    
    <!-- ____________________________________________________________________POSITION ATTRIBUTE INFORMATION____________________________________________________-->
      
    <!-- ( position="8" ) ATTRIBUTE IN BELOW EXAMPLE ACCORDING TO POSITION VALUE AT LISTING PAGE IT WILL BE PLACED AT 8th POSITION OR COLUMN -->

    <div class="portlet light portlet-fit portlet-form bordered" style="">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-bubble font-blue-custom"></i>
                <span class="caption-subject font-blue-custom bold uppercase">Add New <?= $big?></span>
            </div>
            <div class="actions">
                
                <button class="btn buttons-html5 dt-button green btn-outline btnSave"><i class="fa fa-save"></i> Save F10</button>
                <button class="btn buttons-html5 dt-button green btn-outline btnSaveAndMore"><i class="fa fa-save"></i> Save and New F11</button>
                <button class="btn buttons-html5 dt-button yellow btn-outline btnReset"><i class="fa fa-refresh"></i> Reset F5</button>
            </div>
        </div>
        <div class="portlet-body">
            <div class="tab-custom">
                <form action="#" id="formAdd<?=$big?>" class="form-horizontal">
                    <div class="form-body">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#basicInfo" data-toggle="tab" aria-expanded="true"><?= $big?></a></li>
                            <!-- <li><a href="#detailInfo" data-toggle="tab" aria-expanded="true">Detail Information</a></li> -->
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="basicInfo">
                                 
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                                <div class="alert error-message display-hide">
                                    <button class="close" data-close="alert"></button><span class="error-message-body"> You have some form errors. Please check below. </span></div>
                                <div class="alert success-message display-hide">
                                    <button class="close" data-close="alert"></button><span class="success-message-body"> Your form validation is successful! </span></div>
                                
                                <div class="row margin-top-20">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-6">ID(Auto)
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="number" class="form-control" require="1,1" name="<?= $small;?>Id" id="<?= $small;?>Id" min="0" /> 
                                                        <input type="hidden" id="max<?= $big;?>IdHidden">
                                                        <input type="hidden" class="saveColumn fetch" listattribute="Sr#" id="<?= $small;?>IdHidden" name="spid">
                                                        <input type="hidden" id="voucherTypeHidden" value="new">
                                                        <input type="hidden" class='saveColumn' id="etype" value="<?= $small?>" name="etype">
                                                    </div>
                                                    <span class="input-group-addon blue">
                                                        <a href="#<?= $small?>Lookup" class="" data-toggle="modal">
                                                            <i class="fa fa-search font-blue"></i></a>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-offset-2 col-md-6">
                                        <div class="form-group">
                                            <label for="single" class="control-label col-md-4"><?= $sbig;?> Id </label>
                                            <div class="col-md-8">
                                                <select id="single" class="form-control select2 ">
                                                    <option value="" disabled="" selected="" listattribute="Chart Of Account" position="8" name="account_id">Choose <?= $sbig;?> Id</option>

                                                    <?php foreach ($parties as $party): ?>
                                                        <option value="<?php echo $party['spid']; ?>" ><?php echo $party['account_id'] ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Name
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i><!--want to know about ( require ) attribute? then please READ INFO GIVEN AT PAGE START  -->
                                                    <input type="text" class="form-control fetch saveColumn" position="8" require listattribute="Name" ambigousname="party.name -as- party_name,true" name="name" /> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Urdu Name</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control fetch saveColumn" listattribute="Urdu Name" name="uname" /> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Address</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <textarea class="form-control fetch saveColumn" listattribute="Address" name="address" rows="3"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Urdu Address</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <textarea class="form-control fetch saveColumn" listattribute="Urdu Address" name="uaddress" rows="3"></textarea>
                                                </div>
                                            </div>
                                        </div>     
                                    </div>
                                </div>
                                       <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Credit Card #</label>
                                            <div class="col-md-8"><!-- want to know about ( require ) attribute? then please READ INFO GIVEN AT PAGE START  -->
                                                <input type="text" class='form-control  saveColumn fetch' placeholder="Credit Card" listattribute="Credit Card #" name="credit_card" />

                                            </div>
                                        </div>  
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Mobile #</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control fetch saveColumn" listattribute="Mobile #" name="mobile" /> </div>
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Active</label>
                                            <div class="col-md-8">
                                                <input type="checkbox" id="status"  name="active" class="make-switch fetch saveColumn" listattribute="Status" checked data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="control-label col-md-4">Deposit Limit (Alert)</label>
                                        <div class="col-md-8">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" class="form-control fetch saveColumn" listattribute="Deposit Limit(Alert Above)" name="limit" ambigousname="party.limit -as- credit_limit,true" /> 
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                     <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="control-label col-md-4">Deposit Limit (Stop)</label>
                                        <div class="col-md-8">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" class="form-control fetch saveColumn" listattribute="Deposit Limit(Stop Above)" name="limit_stop" /> 
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                    <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="control-label col-md-4">Opening Balance</label>
                                        <div class="col-md-8">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" class="form-control fetch saveColumn num" listattribute="Opening Balance"  name="op_balance" /> 
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                     <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="control-label col-md-4">Credit</label>
                                        <div class="col-md-1">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="radio" class="form-control fetch saveColumn" listattribute="Opening Balance Type" checked="true" style='width:20px;height:20px;' saveattr="credit" name="op_type" /> 
                                            </div>
                                        </div>
                                        <label class="control-label col-md-2">Debit</label>                            <!-- saveattr custom attribute for radio buttons in customForm.js............. -->
                                        <div class="col-md-2">                                                           <!--     \  \   \    \ -->
                                            <div class="input-icon">                                                     <!--      \  \   \    \      -->          
                                                <i class="fa"></i>                                                       <!--       \  \   \    \    -->             
                                                <input  type="radio" class="form-control fetch saveColumn" style='width:20px;height:20px;' saveattr="debit" name="op_type"/> 
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Acc Type
                                                <span class="required"> * </span>
                                            </label>            <!-- THIS VALUE DOES NOT NEED TO BE SAVED -->
                                            <div class="col-md-8"> <!-- YOU CAN USE ( as='anyname' ) ATTRIBUTE IN JUST LIKE BELOW CASES OTHERWISE YOU WILL NOT GET CORRECT DATA OR FACE PROBLEMS IN LISTING PAGE-->
                                                <div class="input-icon right">  
                                                    <i class="fa"></i>   <!-- !!! IMPORTANT ( 'as' ) ATTRIBUTE IS USED FOR DATA SHOWING IN DATA LISTING PAGE AND IN SEARCHING FIELD AS NAME DUE TO AMBIGUITY  BETWEEN e.gL party.name and level3.name
                                                         WHEN WE GET DATABASE COLUMNS FOR VIEW ALL LISTING PAGE -->
                                                   <input type="hidden" value='2' class="form-control fetch saveColumn"  name="level3" /> 
                                                   <input type="hidden" value='2' class="form-control" listattribute="Acc Type" ambigousname="level3.name -as- l3_name,true" name="name" /> 
                                                   <span> <code>.</code> Accounts Mapping Assets <br/> <code>.</code> Current Assets <br/> <code>.</code> Debitors</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label col-md-4">Default Date</label>
                                        <div class="col-md-8">
                                            <div class="input-icon right"><!--want to know about ( require ) attribute? then please READ INFO GIVEN AT PAGE START  -->
                                                <i class="fa"></i>      <!-- !!!!! IMPORTANT Default date doesn't need to have listattribute  -->
                                                <div class="input-group date date-picker margin-bottom-5" data-date-format="dd M yyyy">
                                                    <input type="text" class="form-control form-filter fetch saveColumn" require="8" readonly name="default_date" placeholder="From">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-sm default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn blue btnPrev" style="display: none;"><i class="fa fa-backward"></i></button>
                                <button class="btn green btnSave2"><i class="fa fa-save"></i> Save F10</button>
                                <button class="btn green btnSaveAndMore2"><i class="fa fa-save"></i> Save and New F11</button>
                                <button class="btn yellow btnReset2"><i class="fa fa-refresh"></i> Reset F5</button>
                                <a href="<?= base_url(); ?><?= $small?>" data-url="<?= $small?>" class="btn default btnBack"><i class="fa fa-step-backward"></i> Cancel F2</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END VALIDATION STATES-->
</div>