<div class="page-content">
    <!-- <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Dashboard</span>
            </li>
        </ul>
    </div> -->
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->

    <!-- BEGIN VALIDATION STATES-->

    <div class="portlet light portlet-fit portlet-form bordered" style="">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-bubble font-blue-custom"></i>
                <span class="caption-subject font-blue-custom bold uppercase">Add New Customer</span>
            </div>
            <div class="actions">
                
                <button class="btn buttons-html5 dt-button green btn-outline btnSave"><i class="fa fa-save"></i> Save F10</button>
                <button class="btn buttons-html5 dt-button green btn-outline btnSaveAndMore"><i class="fa fa-save"></i> Save and New F11</button>
                <button class="btn buttons-html5 dt-button yellow btn-outline btnReset"><i class="fa fa-refresh"></i> Reset F5</button>
            </div>
        </div>
        <div class="portlet-body">
            <div class="tab-custom">
                <form action="#" id="formAddCustomers" class="form-horizontal">
                    <div class="form-body">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#basicInfo" data-toggle="tab" aria-expanded="true">Customers</a></li>
                            <li><a href="#detailInfo" data-toggle="tab" aria-expanded="true">Detail Information</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="basicInfo">
                                 
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                                <div class="alert error-message display-hide">
                                    <button class="close" data-close="alert"></button><span class="error-message-body"> You have some form errors. Please check below. </span></div>
                                <div class="alert success-message display-hide">
                                    <button class="close" data-close="alert"></button><span class="success-message-body"> Your form validation is successful! </span></div>
                                
                                <div class="row margin-top-20">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-6">ID(Auto)
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="number" class="form-control" name="customersId" id="customersId" min="0" /> 
                                                        <input type="hidden" id="maxCustomersIdHidden">
                                                        <input type="hidden" class="saveColumn" id="customersIdHidden" name="spid">
                                                        <input type="hidden" id="voucherTypeHidden" value="new">
                                                        <input type="hidden" class='saveColumn' id="etype" value="customers" name="etype">
                                                    </div>
                                                    <span class="input-group-addon blue">
                                                        <a href="#customersLookup" class="" data-toggle="modal">
                                                            <i class="fa fa-search font-blue"></i></a>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-offset-2 col-md-6">
                                        <div class="form-group">
                                            <label for="single" class="control-label col-md-4">Customers Id </label>
                                            <div class="col-md-8">
                                                <select id="single" class="form-control select2">
                                                    <option value="" disabled="" selected="">Choose Customer Id</option>
                                                    <?php foreach ($customers as $customer): ?>
                                                        <option value="<?php echo $customer['spid']; ?>" ><?php echo $customer['account_id'] ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Name
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control fetch saveColumn" name="name" /> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Urdu Name</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control fetch saveColumn" name="uname" /> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Address</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <textarea class="form-control fetch saveColumn" name="address" rows="3"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Urdu Address</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <textarea class="form-control fetch saveColumn" name="uaddress" rows="3"></textarea>
                                                </div>
                                            </div>
                                        </div>     
                                    </div>
                                </div>
                                       <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Customer Type </label>
                                            <div class="col-md-8">
                                                 <select id="customertype" class="form-control select2" name="custom_type">
                                                    <option value="" disabled="" selected="">Choose Customer Type</option>
                                                    <?php foreach ($customertypes as $customertype): ?>
                                                        <option value="<?php echo $customertype['ctypeid']; ?>" ><?php echo $customertype['name'] ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Mobile #</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control fetch saveColumn" name="mobile" /> </div>
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Active</label>
                                            <div class="col-md-8">
                                                <input type="checkbox" id="status"  name="active" class="make-switch fetch saveColumn" checked data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="control-label col-md-4">Credit Limit (Alert)</label>
                                        <div class="col-md-8">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" class="form-control fetch saveColumn" name="limit" /> 
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                     <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="control-label col-md-4">Credit Limit (Stop)</label>
                                        <div class="col-md-8">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" class="form-control fetch saveColumn" name="limit_stop" /> 
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                    <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="control-label col-md-4">Opening Balance</label>
                                        <div class="col-md-8">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="number" class="form-control fetch saveColumn"  name="op_balance" min="0" /> 
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                     <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="control-label col-md-4">Credit</label>
                                        <div class="col-md-1">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="radio" class="form-control fetch saveColumn" checked="true" style='width:20px;height:20px;' saveattr="credit" name="op_type" /> 
                                            </div>
                                        </div>
                                        <label class="control-label col-md-2">Debit</label>                            <!-- saveattr custom attribute for radio buttons............. -->
                                        <div class="col-md-2">
                                            <div class="input-icon">
                                                <i class="fa"></i>
                                                <input  type="radio" class="form-control fetch saveColumn" style='width:20px;height:20px;' saveattr="debit" name="op_type"/> 
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Acc Type
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                   <input type="hidden" value='2' class="form-control saveColumn" name="level3" /> 
                                                   <span> <code>.</code> Accounts Mapping Assets <br/> <code>.</code> Current Assets <br/> <code>.</code> Debitors</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label col-md-4">Default Date</label>
                                        <div class="col-md-8">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <div class="input-group date date-picker margin-bottom-5" data-date-format="dd M yyyy">
                                                    <input type="text" class="form-control form-filter fetch saveColumn" readonly name="default_date" placeholder="From">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-sm default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="detailInfo">
                                <div class="row margin-top-20">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Contact Person</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control fetch saveColumn" name="contact_person" /> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Email</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control fetch saveColumn" name="email" /> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">CNIC</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control fetch saveColumn" name="cnic" /> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Phone</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control fetch saveColumn" name="phone" /> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">NTN</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control fetch saveColumn" name="ntn" /> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Fax</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control fetch saveColumn" name="fax" /> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">City</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class='form-control  saveColumn' placeholder="City" name="city" id="city" list='cities'>
                                                    <datalist id="cities">
                                                        <?php  foreach ($cities as $city): ?>
                                                            <?php  if ($city['city'] !== ''): ?>
                                                                <option value="<?php  echo $city['city']; ?>">
                                                            <?php  endif ?>
                                                        <?php  endforeach ?>
                                                    </datalist>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">City Area</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class='form-control saveColumn' placeholder="City Area" name="cityarea" id="cityarea" list='areas'>
                                                    <datalist id="areas">
                                                        <?php  foreach ($cityareas as $cityarea): ?>
                                                            <?php  if ($cityarea['cityarea'] !== ''): ?>
                                                                <option value="<?php  echo $cityarea['cityarea']; ?>">
                                                            <?php  endif ?>
                                                        <?php  endforeach ?>
                                                    </datalist>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Country</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class='form-control saveColumn' placeholder="Country" name="country" id="country" list='countries'>
                                                    <datalist id="countries">
                                                        <?php  foreach ($countries as $country): ?>
                                                            <?php  if ($country['country'] !== ''): ?>
                                                                <option value="<?php  echo $country['country']; ?>">
                                                            <?php  endif ?>
                                                        <?php  endforeach ?>
                                                    </datalist>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Type</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class='form-control saveColumn' placeholder="Type" name="type" id="type" list='types'>
                                                    <datalist id="types">
                                                        <?php  foreach ($typess as $typee): ?>
                                                            <?php  if ($typee['etype'] !== ''): ?>
                                                                <option value="<?php  echo $typee['etype']; ?>">
                                                            <?php  endif ?>
                                                        <?php  endforeach ?>
                                                    </datalist>
                                                </div>
                                            </div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn blue btnNext"><i class="fa fa-forward"></i></button>
                                <button class="btn blue btnPrev" style="display: none;"><i class="fa fa-backward"></i></button>
                                <button class="btn green btnSave2"><i class="fa fa-save"></i> Save F10</button>
                                <button class="btn green btnSaveAndMore2"><i class="fa fa-save"></i> Save and New F11</button>
                                <button class="btn yellow btnReset2"><i class="fa fa-refresh"></i> Reset F5</button>
                                <a href="<?= base_url(); ?>customers" data-url="customers" class="btn default btnBack"><i class="fa fa-step-backward"></i> Cancel F2</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END VALIDATION STATES-->
</div>