<div class="page-content">
    
    <!-- Begin: Demo Datatable 1 -->
    <div class="portlet light portlet-fit portlet-datatable bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-settings font-dark font-blue-custom"></i>
                <span class="caption-subject font-dark font-blue-custom sbold uppercase">Customers Listing</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    
                    <a href="<?= base_url(); ?>customers/addEditCustomers" class="dt-button btn green buttons-html5 btn-outline" ><span>Add New Customers</span></a>
                </div>
                <!-- <div class="btn-group">
                    <a class="btn red btn-outline btn-lg" href="javascript:;" data-toggle="dropdown">
                        <i class="fa fa-share"></i>
                        <span class="hidden-xs"> Tools </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a href="javascript:;"> Export to Excel </a>
                        </li>
                        <li>
                            <a href="javascript:;"> Export to CSV </a>
                        </li>
                        <li>
                            <a href="javascript:;"> Export to XML </a>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <a href="javascript:;"> Print Invoices </a>
                        </li>
                    </ul>
                </div> -->
                <!-- <div class="actions"> -->
                   
                
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <div class="table-actions-wrapper">
                    <span> </span>
                    <a class="dt-button buttons-html5 btn red btn-outline disabled btnMultiDelete"><span>Delete</span></a>    
                    <a class="tool-action dt-button buttons-csv buttons-html5 btn purple btn-outline" tabindex="0" data-action="4"><span>CSV</span></a>
                    <a class="tool-action dt-button buttons-excel buttons-html5 btn green btn-outline" tabindex="0" data-action="3"><span>Excel</span></a> 
                    <a class="tool-action dt-button buttons-pdf buttons-html5 btn blue btn-outline" tabindex="0" data-action="2"><span>Pdf</span></a> 
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable" id="customersTable">
                    <thead>
                        <tr role="row" class="heading">
                            
                            <th width="2%">
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                    <span></span>
                                </label>
                            </th>
                            <th width="8%"> Sr&nbsp;# </th>
                            <th width=""> Name </th>
                            <th width=""> Urdu Name </th>
                            <th width=""> Mobile&nbsp;# </th>
                            <th width=""> Credit Limit</th>
                            <th width=""> Acc. &nbsp;Type </th>
                            <th width=""> Contact Person</th>
                            <th width=""> Email</th>
                            <th width=""> CNIC</th>
                            <th width=""> Phone</th>
                            <th width=""> NTN</th>
                            <th width=""> Fax</th>
                            <th width=""> Address</th>
                            <th width=""> Urdu Address</th>
                            <th width=""> City</th>
                            <th width=""> City Area</th>
                            <th width=""> Country</th>
                            <th width=""> Type</th>
                            <th width=""> Account Id</th>
                            <th width=""> Status </th>
                            <th style="min-width: 100px !important;"> Default Date </th>
                            <th style="min-width: 100px !important;"> Created At </th>
                            <th style="min-width: 150px !important;"> Actions </th>
                        </tr>
                        <tr role="row" class="filter">
                            <td> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="pid"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="name"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="uname"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="mobile"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="credit_limit"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="l3_name"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="contact_person"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="email"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="cnic"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="phone"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="ntn"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="fax"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="address"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="uaddress"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="city"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="city_area"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="country"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="etype"> </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="account_id"> </td>
                            <td>
                                <select name="status" class="form-control form-filter input-sm">
                                    <option value="">Select...</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </td>
                            <td>
                                <div class="input-group date date-picker margin-bottom-5" data-date-format="dd M yyyy">
                                    <input type="text" class="form-control form-filter input-sm" readonly name="default_date_from" placeholder="From">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                                <div class="input-group date date-picker" data-date-format="dd M yyyy">
                                    <input type="text" class="form-control form-filter input-sm" readonly name="default_date_to" placeholder="To">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </td>
                            <td>
                                <div class="input-group date date-picker margin-bottom-5" data-date-format="dd M yyyy">
                                    <input type="text" class="form-control form-filter input-sm" readonly name="created_at_from" placeholder="From">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                                <div class="input-group date date-picker" data-date-format="dd M yyyy">
                                    <input type="text" class="form-control form-filter input-sm" readonly name="created_at_to" placeholder="To">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </td>
                            <td>
                                <div class="text-center">
                                    <div class="margin-bottom-5">
                                        <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                            <i class="fa fa-search"></i> Search</button>
                                    </div>
                                    <button class="btn btn-sm red btn-outline filter-cancel">
                                        <i class="fa fa-times"></i> Reset</button>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody> </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailPopup" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>Customer Details</strong></h4>
            </div>
            <div class="modal-body">
                <!-- <div class="row"><div class="col-md-12"><br></div></div>  -->
                <div class="row">
                    <div class="col-md-12">
                        <div id="loader" class="text-center" style="display: none;">
                            <br>
                            <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>
                        </div>
                    </div>
                </div>
                
                <div class="customModalBody"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
$(document).ready(function(){

    var grid = new Datatable();
    grid.init({
        src: $("#customersTable"),
        onSuccess: function (grid, response) {
            // grid:        grid object
            // response:    json object of server side ajax response
            // execute some code after table records loaded
        },
        onError: function (grid) {
            // execute some code on network or other general error  
        },
        onDataLoad: function(grid) {
            // execute some code on ajax data load
        },
        loadingMessage: 'Loading...',
        dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
            
            // save datatable state(pagination, sort, etc) in cookie.
            "bStateSave": true, 

             // save custom filters to the state
            "fnStateSaveParams":    function ( oSettings, sValue ) {
                $("#customersTable tr.filter .form-control").each(function() {
                    sValue[$(this).attr('name')] = $(this).val();
                });
               
                return sValue;
            },

            // read the custom filters from saved state and populate the filter inputs
            "fnStateLoadParams" : function ( oSettings, oData ) {
                //Load custom filters
                $("#customersTable tr.filter .form-control").each(function() {
                    var element = $(this);
                    if (oData[element.attr('name')]) {
                        element.val( oData[element.attr('name')] );
                    }
                });
                
                return true;
            },

            "lengthMenu": [
                [10, 20, 50, 100, 150, -1],
                [10, 20, 50, 100, 150, "All"] // change per page values here
            ],
            "pageLength": 10, // default record count per page
            "ajax": {
                "url": baseUrl+"customers/getCustomers", // ajax source
            },
            "ordering": true,
            "order": [
                [1, "asc"]
            ],// set first column as a default sort by asc
            "columnDefs": [{  // set default column settings
                'orderable': false,
                'targets': [0,23]
            }, {
                "searchable": false,
                "targets": [0,23]
            },
            {
                "className": 'text-center',
                "targets": [0,23]
            }], buttons: [
                { extend: 'print', exportOptions: {columns: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]}, className: 'btn default' },
                { extend: 'copy', exportOptions: {columns: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]}, className: 'btn default' },
                { extend: 'pdf',  orientation: 'landscape', exportOptions: {columns: [1,2,3,4,5,6,7,9,10,11]}, className: 'btn default' },
                { extend: 'excel', exportOptions: {columns: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]}, className: 'btn default' },
                { extend: 'csv', exportOptions: {columns: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]}, className: 'btn default' },
                {
                    text: 'Reload',
                    className: 'btn default',
                    action: function ( e, dt, node, config ) {
                        dt.ajax.reload();
                        //alert('Datatable reloaded!');
                    }
                }
            ],
        }
    });


    $('a.tool-action').on('click', function() {

        var action = $(this).attr('data-action');
        grid.getDataTable().button(action).trigger();
    });

    $("#customersTable").keypress(function(e) {

        if(e.which == 13) {
           
            $(".filter-submit").click();
        }
    });

    $('#customersTable').find('.group-checkable').change(function () {

        var checked = jQuery(this).is(":checked");
        if (checked) {
            
            $('.btnMultiDelete').removeClass('disabled');
        } else {
            
            $('.btnMultiDelete').addClass('disabled');
        }
    });


    $('#customersTable').on('click', 'tr .checkboxes', function () {


        var checkedVals = $('.checkboxes:checkbox:checked').map(function() {
            return this.value;
        }).get();

        if(checkedVals.length > 0){

            $('.btnMultiDelete').removeClass('disabled');
        }else{

            $('.btnMultiDelete').addClass('disabled');
        }
    });

    $('#customersTable').on('click', '.btnDelete', function(e){

        e.preventDefault();
        var pId = $(this).attr("data-spid");

        swal({
            title: "Are you sure you want to delete this account?",
            //text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger btn-outline",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: baseUrl+"customers/deleteCustomers",
                    dataType: "json",
                    data: {sp_ids : pId},
                    success: function(data){
                        if(data){

                            swal("Deleted!", "Customer deleted successfully!", "success");
                            grid.getDataTable().button(5).trigger();
                        }else{

                            swal("Error", "An internal error occur, please try again later.", "error");
                        }
                    }
                });
            }
        });
    });

    $('.btnMultiDelete').on('click', function(e){

        e.preventDefault();
        var checkedVals = $('.checkboxes:checkbox:checked').map(function() {
            return this.value;
        }).get();

        swal({
            title: "Are you sure you want to delete "+checkedVals.length+" selected Customers?",
            //text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger btn-outline",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: baseUrl+"customers/deleteCustomers",
                    dataType: "json",
                    data: {p_ids : checkedVals},
                    success: function(data){
                        if(data){

                            swal("Deleted!", checkedVals.length+" Customers deleted successfully!", "success");
                            grid.getDataTable().button(5).trigger();
                        }else{

                            swal("Error", "An internal error occur, please try again later.", "error");
                        }
                    }
                });
            }
        });
    });

    $('#customersTable').on('click', '.detailPopup', function(e){

        e.preventDefault();
        $('.customModalBody').html('');
        $('#loader').show();
        var pId = $(this).attr("data-spid");
        $.ajax({
            type: "POST",
            url: baseUrl+"customers/getCustomersDetail",
            dataType: "json",
            data: {sp_id : pId},
            success: function(html){
                
                $('#loader').hide();
                $('.customModalBody').append(html);
            }
        });
    });

    $('#customersTable').on('click', '.btnVerify', function(){

        var spId = $(this).attr("data-spid");
        swal({
            title: "Are you sure you want to active this account?",
            //text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success btn-outline",
            confirmButtonText: "Yes, active it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: baseUrl+"customers/changeCustomersStatus",
                    dataType: "json",
                    data: {sp_id : spId, active : 1},
                    success: function(data){
                        if(data){

                            swal("Actived!", "Account actived successfully!", "success");
                            grid.getDataTable().button(5).trigger();
                        }else{

                            swal("Error", "An internal error occur, please try again later.", "error");
                        }
                    }
                });
            }
        });
    });

    $('#customersTable').on('click', '.btnUnverify', function(){

        var spId = $(this).attr("data-spid");
        swal({
            title: "Are you sure you want to inactive this account?",
            //text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger btn-outline",
            confirmButtonText: "Yes, inactive it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: baseUrl+"customers/changeCustomersStatus",
                    dataType: "json",
                    data: {sp_id : spId, active : 0},
                    success: function(data){
                        if(data){

                            swal("Inactived!", "Customer inactived successfully!", "success");
                            grid.getDataTable().button(5).trigger();
                        }else{

                            swal("Error", "An internal error occur, please try again later.", "error");
                        }
                    }
                });
            }
        });
    });
});
</script>