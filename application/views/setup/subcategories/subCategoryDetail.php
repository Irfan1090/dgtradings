<div class="row">
    <div class="col-md-2"><strong>Subcategory Id:</strong></div>
    <div class="col-md-4"><?=($result['subcatid']) ? $result['subcatid'] : '-'; ?></div>
    <div class="col-md-2"><strong>Category Name:</strong></div>
    <div class="col-md-4"><?=($result['cat_name']) ? $result['cat_name'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Name:</strong></div>
    <div class="col-md-4"><?=($result['name']) ? $result['name'] : '-'; ?></div>
    <div class="col-md-2"><strong>Description:</strong></div>
    <div class="col-md-4"><?=($result['description']) ? $result['description'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
	<div class="col-md-2"><strong>Created At:</strong></div>
    <div class="col-md-4"><?=($result['created_at']) ? date('d M Y', strtotime(substr($result['created_at'], 0, 11))) : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>