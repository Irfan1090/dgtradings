<div class="row">
    <div class="col-md-2"><strong>L3:</strong></div>
    <div class="col-md-4"><?=($result['l3']) ? $result['l3'] : '-'; ?></div>
    <div class="col-md-2"><strong>level2 Name:</strong></div>
    <div class="col-md-4"><?=($result['l2_name']) ? $result['l2_name'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Name:</strong></div>
    <div class="col-md-4"><?=($result['name']) ? $result['name'] : '-'; ?></div>
    <div class="col-md-2"><strong>Created At:</strong></div>
    <div class="col-md-4"><?=($result['created_at']) ? date('d M Y', strtotime(substr($result['created_at'], 0, 11))) : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>