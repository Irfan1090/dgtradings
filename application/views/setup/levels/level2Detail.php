<div class="row">
    <div class="col-md-2"><strong>L2:</strong></div>
    <div class="col-md-4"><?=($result['l2']) ? $result['l2'] : '-'; ?></div>
    <div class="col-md-2"><strong>level1 Name:</strong></div>
    <div class="col-md-4"><?=($result['l1_name']) ? $result['l1_name'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Name:</strong></div>
    <div class="col-md-4"><?=($result['name']) ? $result['name'] : '-'; ?></div>
    <div class="col-md-2"><strong>Created At:</strong></div>
    <div class="col-md-4"><?=($result['created_at']) ? date('d M Y', strtotime(substr($result['created_at'], 0, 11))) : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>