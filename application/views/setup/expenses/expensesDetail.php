<h5><strong>Basic Information:</strong></h5>
<div class="row">
    <div class="col-md-2"><strong>Supplier Id:</strong></div>
    <div class="col-md-4"><?=($result['spid']) ? $result['spid'] : '-'; ?></div>
    <div class="col-md-2"><strong>Account Code:</strong></div>
    <div class="col-md-4"><?=($result['account_id']) ? $result['account_id'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Name:</strong></div>
    <div class="col-md-4"><?=($result['name']) ? $result['name'] : '-'; ?></div>
    <div class="col-md-2"><strong>Mobile#:</strong></div>
    <div class="col-md-4"><?=($result['mobile']) ? $result['mobile'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Address:</strong></div>
    <div class="col-md-4"><?=($result['address']) ? $result['address'] : '-'; ?></div>
    <div class="col-md-2"><strong>Credit Limit:</strong></div>
    <div class="col-md-4"><?=($result['limit']) ? $result['limit'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Account Type:</strong></div>
    <div class="col-md-4"><?=($result['l3_name']) ? $result['l3_name'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<h5><strong>Detailed Information:</strong></h5>
<div class="row">
    <div class="col-md-2"><strong>Contact Person:</strong></div>
    <div class="col-md-4"><?=($result['contact_person']) ? $result['contact_person'] : '-'; ?></div>
    <div class="col-md-2"><strong>CNIC:</strong></div>
    <div class="col-md-4"><?=($result['cnic']) ? $result['cnic'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>NTN:</strong></div>
    <div class="col-md-4"><?=($result['ntn']) ? $result['ntn'] : '-'; ?></div>
    <div class="col-md-2"><strong>Phone:</strong></div>
    <div class="col-md-4"><?=($result['phone']) ? $result['phone'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Email:</strong></div>
    <div class="col-md-4"><?=($result['email']) ? $result['email'] : '-'; ?></div>
    <div class="col-md-2"><strong>Fax:</strong></div>
    <div class="col-md-4"><?=($result['fax']) ? $result['fax'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>City:</strong></div>
    <div class="col-md-4"><?=($result['city']) ? $result['city'] : '-'; ?></div>
    <div class="col-md-2"><strong>City Area:</strong></div>
    <div class="col-md-4"><?=($result['cityarea']) ? $result['cityarea'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Country:</strong></div>
    <div class="col-md-4"><?=($result['country']) ? $result['country'] : '-'; ?></div>
    <div class="col-md-2"><strong>Type:</strong></div>
    <div class="col-md-4"><?=($result['etype']) ? $result['etype'] : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>
<div class="row">
    <div class="col-md-2"><strong>Default Date:</strong></div>
    <div class="col-md-4"><?=($result['default_date']) ? date('d M Y', strtotime(substr($result['default_date'], 0, 11))) : '-'; ?></div>
    <div class="col-md-2"><strong>Created At:</strong></div>
    <div class="col-md-4"><?=($result['created_at']) ? date('d M Y', strtotime(substr($result['created_at'], 0, 11))) : '-'; ?></div>
</div>
<div class="row"><div class="col-md-12"><br></div></div>