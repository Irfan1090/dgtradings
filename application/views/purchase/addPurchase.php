<div class="page-content">
    <div class="portlet light portlet-fit portlet-form bordered" style="">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-bubble font-blue-custom"></i>
                <span class="caption-subject font-blue-custom bold uppercase">Purchase Voucher</span>
            </div>
            <div class="actions">
                
                <button class="btn buttons-html5 dt-button green btn-outline btnSave"><i class="fa fa-save"></i> Save F10</button>
                <button class="btn buttons-html5 dt-button yellow btn-outline btnReset"><i class="fa fa-refresh"></i> Reset F5</button>
            </div>
        </div>
        <div class="portlet-body">
            <div class="tab-custom">
                <form action="#" id="purchaseForm" class="">
                    <div class="form-body">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#main-tab" data-toggle="tab"> <i class="fa fa-shopping-cart"></i> Main </a>
                            </li>
                            <li>
                                <a href="#other-tab" data-toggle="tab"> <i class="fa fa-cogs"></i> Other </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="main-tab"><br>
                                <div class="row margin-top-20">
                                    
                                    <div class="col-md-10">
                                        <div class="alert alert-danger display-hide">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                        <div class="alert alert-success display-hide">
                                            <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                        <div class="alert error-message display-hide">
                                            <button class="close" data-close="alert"></button><span class="error-message-body"> You have some form errors. Please check below. </span></div>
                                        <div class="alert success-message display-hide">
                                            <button class="close" data-close="alert"></button><span class="success-message-body"> Your form validation is successful! </span></div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-4">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon brad-left-top"> Sr# </span>
                                                        <input type="number" class="form-control" id="vrnoa" >
                                                        <input type="hidden" id="maxVrnoaHidden">
                                                        <input type="hidden" id="vrnoaHidden" name="vrnoa" class="saveColumn">
                                                        <input type="hidden" id="voucherTypeHidden">
                                                        <input type="hidden" name="uid" id="uId" class="saveColumn" value="<?php echo $this->session->userdata('uid'); ?>">
                                                        <input type="hidden" name="uname" id="uName" value="<?php echo $this->session->userdata('uname'); ?>">
                                                        <input type="hidden" name="company_id" id="cId" class="saveColumn" value="<?php echo $this->session->userdata('company_id'); ?>">
                                                        <input type="hidden" name="cName" id="cName" value="<?php echo $this->session->userdata('company_name'); ?>">
                                                        <input type="hidden" id="purchaseId" value="<?php echo (isset($setting_configur[0]['purchase'])) ? $setting_configur[0]['purchase'] : ''; ?>">
                                                        <input type="hidden" id="discountId" value="<?php echo (isset($setting_configur[0]['discount'])) ? $setting_configur[0]['discount'] : ''; ?>">
                                                        <input type="hidden" id="itemDiscountId" value="<?php echo (isset($setting_configur[0]['itemdiscount'])) ? $setting_configur[0]['itemdiscount'] : ''; ?>">
                                                        <input type="hidden" id="expenseId" value="<?php echo (isset($setting_configur[0]['expenses'])) ? $setting_configur[0]['expenses'] : ''; ?>">
                                                        <input type="hidden" id="taxId" value="<?php echo (isset($setting_configur[0]['tax'])) ? $setting_configur[0]['tax'] : ''; ?>">
                                                        <input type="hidden" id="cashId" value="<?php echo (isset($setting_configur[0]['cash'])) ? $setting_configur[0]['cash'] : ''; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-xs-4">
                                                <div class="input-group">
                                                    <span for="" class='input-group-addon brad-left-top-bottom'>Vr#</span>
                                                    <input type="text" class="form-control" id="vrno" readonly='true'>
                                                    <input type="hidden" id="maxVrnoHidden">
                                                    <input type="hidden" name="vrno" class="saveColumn" id="vrnoHidden">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-xs-12">
                                                <div class="input-group date date-picker margin-bottom-5" data-date-format="dd M yyyy">
                                                    <span for="" class='input-group-addon brad-left-top-bottom'>Date</span>
                                                    <input class="form-control form-control-inline brad-right-top-bottom saveColumn" type="text" id="currentDate" name="vrdate" value="<?=date('d M Y')?>">
                                                </div>
                                            </div><!-- end of col -->
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label>Party Name</label>
                                                    <select class="form-control select2 saveColumn" name="party_id" id="partyDropdown" data-error-container="#party_error">
                                                        <option value="" disabled="" selected="">Choose party</option>
                                                        <?php foreach ($parties as $key => $party): ?>
                                                            <?php if($key == 0){ ?>
                                                                <option value="<?php echo $party['pid']; ?>"><?php echo $party['name']; ?></option>
                                                            <?php }else{ ?>
                                                            <option value="<?php echo $party['pid']; ?>" data-city="<?php echo $party['city']; ?>" data-address="<?php echo $party['address']; ?>" data-cityarea="<?php echo $party['cityarea']; ?>" data-mobile="<?php echo $party['mobile']; ?>"  ><?php echo $party['name']; ?></option>
                                                            <?php } ?>
                                                        <?php endforeach ?>
                                                    </select>
                                                    <div id="party_error"> </div>
                                                </div>        
                                            </div>
                                            <div class="col-lg-3">  
                                                <div class="form-group">                                              
                                                    <label>Warehouse</label>
                                                    <select class="form-control select2" name="godown_id" id="deptDropdown" data-error-container="#dept_error">
                                                        <option value="" selected="" disabled="">Choose Warehouse</option>
                                                        <?php foreach ($departments as $department): ?>
                                                          <?php $selectedDepartment = ($department['name'] == "MAIN") ? "selected":""; ?>
                                                            <option <?php echo $selectedDepartment; ?> value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                    <div id="dept_error"> </div>
                                                </div>
                                            </div>                    
                                            <div class="col-lg-5">
                                                <label for="" class='label-control'>Remarks</label>
                                                <input type="text" class='form-control saveColumn brad-right-bottom' placeholder='Remarks' name="remarks" id="remarks">
                                            </div><!-- end of col -->
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-lg-12 col-xs-12">
                                                <div class="portlet bottom-0">
                                                    <div class="portlet-title append-grid1">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="alert product-error-message display-hide singleProduct">
                                                                    <button class="close btnSingleProductClose"></button> You have some errors. Please check below. 
                                                                </div>
                                                                <div class="alert product-error-message display-hide gridItem">
                                                                    <button class="close btnGridItemClose"></button> No data found to save, Please add some items. 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" id="barCode">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <div class="form-group">
                                                                    <select class="form-control select2" name="itemId" id="itemIdDropdown">
                                                                        <option value="" disabled="" selected="">Item Code</option>
                                                                        <?php foreach ($items as $key => $item): ?>
                                                                            <?php if($key == 0){ ?>
                                                                            <option value="<?php echo $item['item_id']; ?>"><?php echo $item['item_des']; ?></option>
                                                                            <?php }else{ ?>
                                                                            <option value="<?php echo $item['item_id']; ?>" data-srate="<?php echo $item['srate']; ?>" data-bid="<?php echo $item['bid']; ?>" data-size="<?php echo $item['size']; ?>" data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?> " data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>" data-barcode="<?php echo $item['item_barcode']; ?>"><?php echo $item['short_code']; ?></option>
                                                                            <?php } ?>
                                                                        <?php endforeach ?>
                                                                    </select> 
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <div class="form-group">
                                                                    <select class="form-control select2" name="item_dropdown" id="itemDropdown" >
                                                                        <option value="" disabled="" selected="">Item description</option>
                                                                        <?php foreach ($items as $key => $item): ?>
                                                                            <?php if($key == 0){ ?>
                                                                            <option value="<?php echo $item['item_id']; ?>"><?php echo $item['item_des']; ?></option>
                                                                            <?php }else{ ?>
                                                                            <option value="<?php echo $item['item_id']; ?>" data-srate="<?php echo $item['srate']; ?>" data-bid="<?php echo $item['bid']; ?>" data-shortcode="<?php echo $item['short_code']; ?>" data-size="<?php echo $item['size']; ?>" data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>" data-barcode="<?php echo $item['item_barcode']; ?>" ><?php echo $item['item_des']; ?></option>
                                                                            <?php } ?>
                                                                        <?php endforeach ?>
                                                                    </select>
                                                                </div>
                                                                <input type='hidden' id="sizeHidden" class="">
                                                            </div>
                                                            <div class="col-lg-1 col-xs-2">
                                                                <div class="form-group">
                                                                    <input type="text" class='form-control num' placeholder='Qty' name="qty" id="qty">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-1 col-xs-2">
                                                                <div class="form-group">
                                                                    <input type="text" class='form-control num' placeholder='Rate' name="rate" id="rate">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-1 col-xs-2">
                                                                <div class="form-group">
                                                                    <input type="text" class='form-control num' placeholder='Disc%' name="discP" id="discP">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-1 col-xs-2">
                                                                <div class="form-group">
                                                                    <input type="text" class='form-control num' placeholder='Disc' name="disc" id="disc">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-xs-4">
                                                                <div class="form-group">
                                                                    <input type="text" class='form-control num' placeholder='Amount' name="amount" id="amount" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-1 col-xs-2">
                                                                <a href="" class='btn btn-outline bdr-round blue' id="btnAdd"> <i class="fa fa-plus"></i> </a>
                                                                <div class="tools"> </div>
                                                            </div><!-- end of col -->
                                                        </div><!-- end of row -->
                                                    </div><!-- portlet -->
                                                    <div class="portlet-body">
                                                        <table class="table table-striped table-bordered table-hover" id="purchaseTable">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width:30px;"> Sr# </th>
                                                                    <th> Barcode </th>
                                                                    <th> Item Detail </th>
                                                                    <th> Qty </th>
                                                                    <th> Rate </th>
                                                                    <th> Disc% </th>
                                                                    <th> Discount </th>
                                                                    <th> Amount</th>
                                                                    <th> Sticker</th>
                                                                    <th class='text-center' style="width:110px;"> Action </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="purchaseRows">
                                                              
                                                            </tbody>
                                                            <tfoot class="tfoot_tbl">
                                                                <tr>
                                                                    <td class="text-right" colspan="3"><b>Totals</b></td>
                                                                    <td class="text-right totalQty"></td>
                                                                    <td class="text-right totalRate"></td>
                                                                    <td class="text-right totalDiscP"></td>
                                                                    <td class="text-right totalDisc"></td>
                                                                    <td class="text-right totalAmount"></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <form action="">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-lg-2 col-xs-2">
                                                                            <label for="" class='label-control'>Disc%</label>
                                                                            <input type="text" class='form-control saveColumn' name="discp" id="discount">
                                                                        </div>
                                                                        <div class="col-lg-2 col-xs-2">
                                                                            <label for="" class='label-control'>Discount Amt</label>
                                                                            <input type="text" class='form-control saveColumn' name="discount" id="discAmount">
                                                                        </div>
                                                                        <div class="col-lg-2 col-xs-2">
                                                                            <label for="" class='label-control'>Exp%</label>
                                                                            <input type="text" class='form-control saveColumn' name="exppercent" id="expense">
                                                                        </div>
                                                                        <div class="col-lg-2 col-xs-2">
                                                                            <label for="" class='label-control'>Exp Amnt</label>
                                                                            <input type="text" class='form-control saveColumn' name="expense" id="expAmount">
                                                                        </div>
                                                                        <div class="col-lg-2 col-xs-2">
                                                                            <label for="" class='label-control'>Tax%</label>
                                                                            <input type="text" class='form-control saveColumn' name="taxpercent" id="tax">
                                                                        </div>
                                                                        <div class="col-lg-2 col-xs-2">
                                                                            <label for="" class='label-control'>Tax Amount</label>
                                                                            <input type="text" class='form-control saveColumn' name="tax" id="taxAmount">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-lg-2 col-xs-1">
                                                                            <label for="" class='label-control'>Paid Amount</label>
                                                                            <input type="text" class='form-control saveColumn' name="paid" id="paid">
                                                                        </div>
                                                                        <div class="col-lg-2 col-xs-1">
                                                                            <label for="" class='label-control'>Net Amount</label>
                                                                            <input type="text" class='form-control saveColumn' id='netAmount' name="namount" readonly="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-lg-3 col-xs-4">
                                                                            <label for="" class='label-control'>Previous Balance</label>
                                                                            <div class="md-radio-inline">
                                                                                <div class="md-radio">
                                                                                    <input type="radio" id="radio6" name="radio2" class="md-radiobtn">
                                                                                    <label for="radio6">
                                                                                        <span class="inc"></span>
                                                                                        <span class="check"></span>
                                                                                        <span class="box"></span> Yes 
                                                                                    </label>
                                                                                </div>
                                                                                <div class="md-radio">
                                                                                    <input type="radio" id="radio7" name="radio2" class="md-radiobtn" checked="">
                                                                                    <label for="radio7">
                                                                                        <span class="inc"></span>
                                                                                        <span class="check"></span>
                                                                                        <span class="box"></span> No
                                                                                    </label>
                                                                                </div><!-- md-radio -->
                                                                            </div><!-- md-radio-inline-->
                                                                        </div><!-- end of col -->
                                                                    </div><!-- end of row -->
                                                                </div><!-- form-group -->
                                                            </form><!-- end of form -->
                                                        </div><!-- end of col -->
                                                    </div><!-- end of row -->
                                                </div><!-- portlet -->
                                            </div><!-- end of col -->
                                        </div><!-- end of row -->
                                    </div>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h4 class="text-info">Other Information</h4>
                                                <div class="row lFiveRateDisp disp ten-padding">
                                                    <label><strong>Last 5 Purchase Rates</strong></label>
                                                    <table class="lastRateTable table-bordered" id="lastRateTable ">
                                                        <thead>
                                                            <tr>
                                                                <th style='width:90px;  font-size: 12px;'>Date</th>
                                                                <th style='width:60px;  font-size: 12px;'>Rate</th>
                                                                <th style='width:60px;  font-size: 12px;'>Qty</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="rateTbody" style="font-size: 11px !important;">
                                                              
                                                        </tbody>
                                                    </table>
                                                </div><!-- end of row -->
                                                <div class="row lastStockLocationTableDisp disp ten-padding">
                                                    <label><strong>Stock Positions</strong></label>
                                                        <table class="lastStockLocationTable table-bordered" id="lastStockLocationTable">
                                                                <thead>
                                                                    <tr>
                                                                        <th style='width:90px;  font-size: 12px;'>Location</th>
                                                                        <th style='width:60px;  font-size: 12px;'>Qty</th>
                                                                    </tr>
                                                                </thead>
                                                                
                                                                <tbody class="rateTbody" style="font-size: 11px !important;">
                                                                    
                                                                </tbody>
                                                        </table>
                                                </div>
                                            </div><!-- end of col -->
                                        </div><!-- end of row -->
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="other-tab"><br>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-2">                                                
                                                    <label class="label-control">Received By</label>
                                                    <input class='form-control' type='text' list="receivers" id='receiversList'>
                                                    <datalist id='receivers'>
                                                      <?php foreach ($receivers as $receiver): ?>
                                                        <option value="<?php echo $receiver['received_by']; ?>"></option>
                                                      <?php endforeach ?>
                                                    </datalist>                                                
                                                </div>
                                                <div class="col-lg-2">                                                
                                                    <label>Through</label>
                                                    <select class="form-control select2" id="transporterDropdown">
                                                      <option value="" disabled="" selected="">Choose transporter</option>
                                                        <?php foreach ($transporters as $transporter): ?>
                                                          <option value="<?php echo $transporter['transporter_id']; ?>"><?php echo $transporter['name']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>  
                                                </div>
                                                <div class="col-lg-2">
                                                    <label for="" class="label-control">Customer Name</label>                                                    
                                                    <input type="text" class="form-control" id="customerName" >      
                                                </div>
                                                <div class="col-lg-2">
                                                    <label for="" class="label-control">Mobile</label>                                                    
                                                    <input type="text" class="form-control" id="customerMob" >                                                    
                                                </div>
                                                <div class="col-lg-1">                                                
                                                    <label class="label-control">Inv#</label>
                                                    <input type="text" class="form-control num" id="invNo">                                                
                                                </div>
                                                <div class="col-lg-2">                                                
                                                    <label class="label-control">Due Date</label>
                                                    <div class="input-group date date-picker margin-bottom-5" data-date-format="dd M yyyy">
                                                        
                                                        <input class="form-control form-control-inline brad-right-top-bottom" type="text" id="dueDate">
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-sm default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                                    </div>                                            
                                                </div>                                           
                                                <div class="col-lg-1">                                                
                                                    <label class="label-control">PO#</label>
                                                    <input type="text" class="form-control" id="orderNo">    
                                                </div><!-- end of col -->
                                            </div><!-- end of row -->
                                        </div><!-- form-group -->
                                    </div><!-- end of col -->
                                </div><!--end of row -->
                            </div><!--  tab-pane -->
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn blue btnNext"><i class="fa fa-forward"></i></button>
                                <button class="btn blue btnPrev" style="display: none;"><i class="fa fa-backward"></i></button>
                                <button class="btn green btnSave2"><i class="fa fa-save"></i> Save F10</button>
                                <button class="btn yellow btnReset2"><i class="fa fa-refresh"></i> Reset F5</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END VALIDATION STATES-->
</div>

<div class="modal fade" id="warehouseModal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>Add New Warehouse</strong></h4>
            </div>
            <div class="modal-body">
                <form action="#" id="formAddWarehouse" class="form-horizontal">
                    <div class="form-body">
                         
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                        <div class="alert error-message display-hide">
                            <button class="close" data-close="alert"></button><span class="error-message-body"> You have some form errors. Please check below. </span></div>
                        <div class="alert success-message display-hide">
                            <button class="close" data-close="alert"></button><span class="success-message-body"> Your form validation is successful! </span></div>
                        
                        <div class="row margin-top-20">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Name
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-6">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="hidden" class="saveColumn" id="dIdHidden" value="25000" name="did">
                                            <input type="hidden" id="voucherTypeHidden" value="new">
                                            <input type="text" class="form-control saveColumn" name="name" /> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Description</label>
                                    <div class="col-md-6">
                                        <textarea class="form-control saveColumn" name="description" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn green btnSaveWarehouse"><i class="fa fa-save"></i> Save</button>
                <button class="btn yellow btnResetWarehouse"><i class="fa fa-refresh"></i> Reset</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="accountModal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>Add New Account</strong></h4>
            </div>
            <div class="modal-body">
                <form action="#" id="formAddAccount" class="form-horizontal">
                    <div class="form-body">
                         
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                        <div class="alert error-message display-hide">
                            <button class="close" data-close="alert"></button><span class="error-message-body"> You have some form errors. Please check below. </span></div>
                        <div class="alert success-message display-hide">
                            <button class="close" data-close="alert"></button><span class="success-message-body"> Your form validation is successful! </span></div>
                        
                        <div class="row margin-top-20">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Name
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-6">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="hidden" class="saveColumn" id="itemIdHidden" value="25000" name="pid">
                                            <input type="hidden" id="voucherTypeHidden" value="new">
                                            <input type="text" class="form-control saveColumn" name="name" /> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Acc Type
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-6">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <select id="level3" class="form-control select2 saveColumn" name="level3" data-error-container="#select2_error">
                                                <option value="" disabled="" selected="">Choose Account Type</option>
                                                <?php foreach ($acctype as $l3): ?>
                                                    <option value="<?php echo $l3['l3']; ?>" data-level2="<?php echo $l3['level2_name']; ?>" data-level1="<?php echo $l3['level1_name']; ?>"><?php echo $l3['level3_name'] ?></option>
                                                <?php endforeach ?>
                                            </select>
                                            <div id="select2_error"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-offset-3 col-lg-6">
                                <span><b>Type 2 &rarr; </b><span id="selectedLevel2"> </span></span> 
                                <span><b>Type 1 &rarr; </b><span id="selectedLevel1"> </span></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn green btnSaveAccount"><i class="fa fa-save"></i> Save</button>
                <button class="btn yellow btnResetAccount"><i class="fa fa-refresh"></i> Reset</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="itemModal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>Add New Item</strong></h4>
            </div>
            <div class="modal-body">
                <form action="#" id="formAddModalItem" class="form-horizontal">
                    <div class="form-body">
                         
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                        <div class="alert error-message display-hide">
                            <button class="close" data-close="alert"></button><span class="error-message-body"> You have some form errors. Please check below. </span></div>
                        <div class="alert success-message display-hide">
                            <button class="close" data-close="alert"></button><span class="success-message-body"> Your form validation is successful! </span></div>
                        
                        <div class="row margin-top-20">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Description
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="hidden" class="saveColumn" id="itemIdHidden" value="25000" name="item_id">
                                            <input type="hidden" id="voucherTypeHidden" value="new">
                                            <input type="text" class="form-control saveColumn" name="item_des" /> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Category
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-6">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <select id="single" class="form-control select2 saveColumn" name="catid" data-error-container="#select2_error">
                                                <option value="" disabled="" selected="">Choose Category</option>
                                                <?php foreach ($categories as $category): ?>
                                                    <option value="<?php echo $category['catid']; ?>"><?php echo $category['name']; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                            <div id="select2_error"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Subcategory</label>
                                    <div class="col-md-6">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <select id="single" class="form-control select2 saveColumn" name="subcatid" data-error-container="#select2_error">
                                                <option value="" disabled="" selected="">Choose Subcategory</option>
                                                <?php foreach ($subcategories as $subcategory): ?>
                                                    <option value="<?php echo $subcategory['subcatid']; ?>"><?php echo $subcategory['name']; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                            <div id="select2_error"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Brand</label>
                                    <div class="col-md-6">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <select id="single" class="form-control select2 saveColumn" name="bid" data-error-container="#select2_error">
                                                <option value="" disabled="" selected="">Choose Brand</option>
                                                <?php foreach ($brands as $brand): ?>
                                                    <option value="<?php echo $brand['bid']; ?>"><?php echo $brand['name']; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                            <div id="select2_error"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Made</label>
                                    <div class="col-md-6">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <select id="single" class="form-control select2 saveColumn" name="made_id" data-error-container="#select2_error">
                                                <option value="" disabled="" selected="">Choose Made</option>
                                                <?php foreach ($mades as $made): ?>
                                                    <option value="<?php echo $made['made_id']; ?>"><?php echo $made['name']; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                            <div id="select2_error"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Purchase Rate</label>
                                    <div class="col-md-6">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control saveColumn" name="cost_price" /> </div>
                                    </div>
                                </div>  
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Sale Rate1</label>
                                    <div class="col-md-6">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control saveColumn" name="srate" /> </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-6">UOM</label>
                                    <div class="col-md-6">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control saveColumn" name="uom" /> </div>
                                    </div>
                                </div>  
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Bar Code</label>
                                    <div class="col-md-6">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control saveColumn" name="item_barcode" /> </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Short Code</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control saveColumn" name="short_code" /> </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn green btnSaveItem"><i class="fa fa-save"></i> Save</button>
                <button class="btn yellow btnResetItem"><i class="fa fa-refresh"></i> Reset</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>