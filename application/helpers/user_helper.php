<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('login')) { # this is for user login

    function login($loginData) {

        $CI = &get_instance();
        $userName = $CI->db->escape_str($loginData['username']);
        $password = $CI->db->escape_str($loginData['password']);

        $user = $CI->commonModel->find('user', 'uid, is_closed, uname, company_id', array('uname' => $userName, 'pass' => $password));

        if (!$user) {

            return 0;
        } else {
            
            $userArr = array('user_id' => $user[0]['uid'], 'uname' => $user[0]['uname'], 'company_id' => $user[0]['company_id'], 'is_closed' => $user[0]['is_closed']);
            createUserSession($userArr);
            return 1;
        }
    }
}

if (!function_exists('createUserSession')) {

    function createUserSession($userData = false) {
        
        if (!$userData) {

            return false;
        }

        $CI = &get_instance();
        $CI->session->set_userdata($userData);
        return true;
    }
}