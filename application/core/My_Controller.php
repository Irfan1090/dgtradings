<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct() {

        parent::__construct();

        $notAuth_functions = array('index', 'register', 'forgotPassword', 'reset_password', 'logout', 'cron_notification', 'thank_you_registring', 'select_user_language');
        $notAuth_controller = array('auth', 'crons');

        $current_class = $this->router->class;
        //$this->rootPath = $this->config->item('root').$this->config->item('dyno');

        //$this->load->helper('session');
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('commonModel');
        $this->load->library('form_validation');

        if (in_array(trim($current_class), $notAuth_controller) && in_array($this->router->method, $notAuth_functions)) {
            
            /* Continue */
        }else {
            
            $this->authenticate();
            /*$this->user_name = $this->session->userdata('kftgrnpoiu');
            $this->admintype = $this->session->userdata('admintype');*/
        }
    }

    //Authenticate function
    public function authenticate() {

        if (!$this->session->userdata('user_id') || $this->session->userdata('user_id') == '') {
            
            if($this->router->method != 'login'){

                $this->session->unset_userdata(array('admintype', 'kftgrnpoiu', 'try'));
                redirect(base_url('auth'), 'refresh');
            }   
        }
    }
}