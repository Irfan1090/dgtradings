<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Level1 extends MY_Controller {

	private $controller = "level1"; 
    private $small = 'level1'; 
	private $big = "Level1";
	private $sbig = "Level1"; 
	protected $shortname = "Level1"; 
	protected $spid = "l1"; 
   	protected $etype = '';
   	protected $table = 'level1';
   	public $arr; 
	public function __construct()  {

		$this->arr = get_object_vars($this);
        
        parent::__construct();
        $this->arr['urlToNext'] = base_url()."application/views/setup/levels/add".$this->big.".php"; // pick data for js and detail listing
        $this->arr['commonModel'] = $this->commonModel; // we are passing model object to definitions library
        $this->arr['join_val'] = ""; // we are passing join values if needed to definitions library // LISTING PAGE FETCH JOINS
        $this->arr['joins'] = array(); // keys must be alphanumeric AND SHOULD BE DIFFERENT FROM EACH OTHER VOUCHER FETCH JOINS
        //THESE ARE THE PARAMETERS 4th,5th AND SO ON.. AND BEING USED IN getDetail() FUNCTION
        $this->load->library('definitions',$this->arr);
    	$this->layout = 'default';
    }

    public function index()
	{
		$this->definitions->loadDefListView();
	}

	public function addEditLevel1()
	{	
		$data['arr'] = $this->arr;
		$data['jsFiles'] = array('setup/generalaccounts/generalaccounts');
		$this->load->view('setup/levels/add'.$this->big, $data);
	}

	public function getLevel1(){

		$this->definitions->getinfo();
	}

	public function getLevel1Detail(){

		$this->definitions->getDetail();
	}
	public function changeLevel1Status(){

		$this->definitions->changeStatus();
	}
	public function getMaxId() {
		
		$this->definitions->getMaxId();
	}

	public function save(){

		$this->definitions->saveDef();
	}

	public function fetch() {

		$this->definitions->fetchDef();
	}

	public function deleteLevel1(){

		$this->definitions->delDefinitions();
	}
}