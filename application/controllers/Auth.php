<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	public function __construct() {

        parent::__construct();
        $this->load->helper('user');
        $this->layout = 'login';
    }

	public function index()
	{
		$this->load->view('login');
	}

	public function login() {

        $this->form_validation->set_rules('username', 'Username', 'required', array('required' => 'Username is required.'));
        $this->form_validation->set_rules('password', 'Password', 'required', array('required' => 'Password is required.'));

        if ($this->form_validation->run() == TRUE) {

        	$userData = login($this->input->post());
            if ($userData == 0) {

                $this->session->set_flashdata('failure_message', 'Invalid username or password');
            } else {

                redirect(base_url() . 'dashboard');
            }
        } 

        $this->load->view('login');
    }
}
