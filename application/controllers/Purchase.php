<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase extends MY_Controller {

	public function __construct() {

        parent::__construct();
    	$this->layout = 'default';
    }

    public function index()
	{
		$data['parties'] = array(array('pid' => 'new', 'name' => '+ Add New')) + $this->commonModel->fetchAllParties();
		$data['items'] = array(array('item_id' => 'new', 'item_des' => '+ Add New')) + $this->commonModel->fetchAllItem();
		$data['departments'] = array(array('did' => 'new', 'name' => '+ Add New')) + $this->commonModel->find('department', 'did, name');
		$data['setting_configur'] = $this->commonModel->find('setting_configuration', '*');
		$data['receivers'] = $this->commonModel->fetchByCol('stockmain', 'received_by');
		$data['transporters'] = $this->commonModel->fetchAll('transporter');
		$data['acctype'] = $this->commonModel->fetchAllLevel3();

		$data['categories'] = $this->commonModel->find('category', 'catid, name');
		$data['subcategories'] = $this->commonModel->find('subcategory', 'subcatid, name');
		$data['brands'] = $this->commonModel->find('brand', 'bid, name');
		$data['mades'] = $this->commonModel->find('made', 'made_id, name');

		$data['jsFiles'] = array('purchase/addPurchase');
		$this->load->view('purchase/addPurchase', $data);
	}

	public function getMaxVrno() {

		if ($this->input->post()) {

			$companyId = $this->input->post('company_id');
			$result = $this->commonModel->getMaxVrno('stockmain', 'purchase', $companyId) + 1;
			echo json_encode($result);
		}

		exit();
	}

	public function getMaxVrnoa() {

		if ($this->input->post()) {

			$companyId = $this->input->post('company_id');
			$result = $this->commonModel->getMaxVrnoa('stockmain', 'purchase', $companyId) + 1;
			echo json_encode($result);
		}

		exit();
	}

	public function lastFiveSRate() {

		if ($this->input->post()) {

			$crit = $this->input->post('crit');
			$companyId = $this->input->post('company_id');	
			$etype = $this->input->post('etype');
			$date = $this->input->post('date');
			$result = $this->commonModel->lastFiveSRate($crit, $companyId, $etype, $date);

			$response = "";
			if ($result === false) {

				$response = 'false';
			} else {
				
				$response = $result;
			}

			echo json_encode($response);
		}
		exit();
	}

	public function lastStockLocatons() {

		if ($this->input->post()){

			$itemId = $this->input->post('item_id');
			$etype = $this->input->post('etype');
			$result = $this->commonModel->lastStockLocatons($itemId, $etype);

			$response = "";
			if ( $result === false ) {

				$response = 'false';
			} else {
				
				$response = $result;
			}

			echo json_encode($response);
		}
		exit();
	}

	public function save() {

		if ($this->input->post()) {

			$ledger = json_decode($this->input->post('ledger'), true);
			$stockmain = json_decode($this->input->post('stockmain'), true);
			$stockdetail = json_decode($this->input->post('stockdetail'), true);
			$vrnoa = $this->input->post('vrnoa');
			$etype = $this->input->post('etype');
			$voucherTypeHidden = $this->input->post('voucher_type_hidden');


			if ($voucherTypeHidden == 'new') {

				$vrnoa = $this->purchases->getMaxVrnoa($etype, $stockmain['company_id']) + 1;
				$stockmain["vrnoa"] = $vrnoa;
			}
			
			$result = $this->commonModel->saveStockForm('stockmain', 'stockdetail', $stockmain, $stockdetail, $vrnoa, $etype);
			if($result){

				$result = $this->commonModel->saveLedger($ledger, $vrnoa, $etype, $voucherTypeHidden);
			}

			echo json_encode($result);
		}
		exit();
	}

	public function fetch() {

		if ($this->input->post()) {

			$vrnoa = $this->input->post('vrnoa');
			$companyId = $this->input->post('company_id');	
			//$result = $this->commonModel->fetchVoucher($vrnoa, 'purchase', $companyId);
			$select = 'stockmain.date_time, stockmain.cash_tender, stockmain.is_hold,  stockmain.freight, stockdetail.prate, item.short_code, brand.name AS brand_name, brand.bid, stockmain.customer_name, stockmain.mobile, stockdetail.discount AS discount1, stockdetail.damount, stockdetail.prate, stockdetail.uom, stockdetail.etype AS detype, stockdetail.pcs, stockdetail.packing, stockdetail.type, stockmain.party_id_co, stockmain.currency_id, stockdetail.received_by AS received, stockdetail.workdetail, department.name AS dept_name, stockmain.vrno, stockmain.uid, stockmain.paid, stockmain.vrnoa, stockmain.vrdate, stockmain.taxpercent, stockmain.exppercent, stockmain.tax, stockmain.party_id, stockmain.bilty_no AS inv_no, stockmain.bilty_date AS due_date, stockmain.received_by, stockmain.transporter_id, stockmain.remarks, ROUND(stockmain.namount, 2) namount, stockmain.order_vrno AS order_no, ROUND(stockmain.freight, 2) freight, ROUND(stockmain.discp, 2) discp, ROUND(stockmain.discount, 2) discount, ROUND(stockmain.expense, 2) expense, stockmain.vehicle_id AS amnt_paid, stockmain.officer_id, ROUND(stockmain.ddays) AS due_days, stockdetail.item_id, stockdetail.godown_id, ROUND(stockdetail.qty, 2) AS s_qty, ROUND(stockdetail.qtyf, 2) AS s_qtyf, ROUND(stockdetail.rate, 2) AS s_rate, ROUND(stockdetail.amount, 2) AS s_amount, ROUND(stockdetail.damount, 2) AS s_damount, ROUND(stockdetail.discount, 2) AS s_discount, ROUND(stockdetail.netamount, 2) AS s_net, item.item_des AS item_name,item.uom, stockdetail.weight, item.item_barcode, stockmain.company_id';
			$where = array('stockmain.vrnoa' => $vrnoa, 'stockmain.etype' => 'purchase');
			$joins = array('stockdetail', 'item', 'brand', 'department');
			$joinsOn = array('stockmain.stid = stockdetail.stid', 'item.item_id = stockdetail.item_id', 'brand.bid = item.bid', 'department.did = stockdetail.godown_id');
			$result = $this->commonModel->find('stockmain', $select, $where, $joins, $joinsOn);

			echo json_encode($result);
		}
		exit();
	}
}