<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GlobalSettings extends MY_Controller {

	public function __construct() {

        parent::__construct();
    	$this->layout = 'default';
    }

    public function index()
	{
		$this->layout = 'default';
        $data['settings'] = $this->commonModel->find('global_settings', '*')[0];
        $data['jsFiles'] = array('setup/settings/globalSettings');
        $this->load->view('setup/settings/globalSettings', $data);
	}

	public function save(){

		$categoryDetail = $this->input->post('global_settings_detail');

		$result = $this->commonModel->find('global_settings', '*');
		if ($result) {

			$result = $this->commonModel->update('global_settings', $categoryDetail, array('g_id' => $result[0]['g_id']));
			$affect = $this->db->affected_rows();
		} else {
			$result = $this->commonModel->save('global_settings', $categoryDetail);
			$affect = $this->db->affected_rows();
		}

		echo json_encode($affect);

		exit();
	}
}