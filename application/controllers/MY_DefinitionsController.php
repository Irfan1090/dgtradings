<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_DefinitionsController extends MY_Controller {

    function __construct() {
        die('hello');
        parent::__construct();

        $notAuth_functions = array('index', 'register', 'forgotPassword', 'reset_password', 'logout', 'cron_notification', 'thank_you_registring', 'select_user_language');
        $notAuth_controller = array('auth', 'crons');

        $current_class = $this->router->class;
        //$this->rootPath = $this->config->item('root').$this->config->item('dyno');

        //$this->load->helper('session');
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('commonModel');
        //$this->load->helper('functions');
        //$this->load->helper('database_function');
        //$this->load->helper('error');
        $this->load->library('form_validation');

        if (in_array(trim($current_class), $notAuth_controller) && in_array($this->router->method, $notAuth_functions)) {
            
            /* Continue */
        }else {
            
            $this->authenticate();
            /*$this->user_name = $this->session->userdata('kftgrnpoiu');
            $this->admintype = $this->session->userdata('admintype');*/
        }
    }

    //Authenticate function
    public function authenticate() {

        if (!$this->session->userdata('user_id') || $this->session->userdata('user_id') == '') {
            
            if($this->router->method != 'login'){

                $this->session->unset_userdata(array('admintype', 'kftgrnpoiu', 'try'));
                redirect(base_url('auth'), 'refresh');
            }   
        }
    }
public function valgetter($books,$i)
        {
            
            $retarray = [];
            $retcomparray = [];
            $retcomp2array = [];
                    foreach ($books as $key => $value) 
                    {
                        if($value->hasAttribute('listattribute'))
                        { 
                            $at = $value->getAttribute('listattribute');
                            $name = $value->getAttribute('name');
                            $ambigousname = ($value->hasAttribute('ambigousname'))? $value->getAttribute('ambigousname') :$value->getAttribute('name');
                            $trace = ($value->hasAttribute('ambigousname'))? 'ambigous' :'notambigous';
                            if(!empty($at))
                            {   $i++;
                                array_push($retarray,$at,$name);           
                                array_push($retcomp2array,$at,$name,$ambigousname);           
                                array_push($retcomparray,$at,$name,$i);          
                            }
                        }
                    }
            return [$retarray,$retcomparray,$retcomp2array];
        }
    public  function getdata($url)
        {
                    $ch = curl_init();
                    curl_setopt($ch,CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
                    curl_setopt($ch,CURLOPT_POST, true);
                    $result = curl_exec($ch);
                    curl_close($ch);
                    $doc = new DOMDocument();
                    @$doc->loadHTML($result);
                    $doc->saveHTML();
                    $tags = $this->valgetter($doc->getElementsByTagName('input'),0);
                    $i = $tags[1][count($tags[1])-1];
                    $tag = $this->valgetter($doc->getElementsByTagName('select'),$i);
                    $lastelarray = array('Default Date','Created At','Actions');
                    $uses_down = array_merge($tags[2],$tag[2]);
                    $data['uses_compdown'] =  array_chunk(array_merge($tags[1],$tag[1]),3);
                    $uses = array_merge($tags[0],$tag[0],$lastelarray);
                    $data['chunked'] =  array_chunk($uses_down, 3);
                    $data['jscript'] = implode(',', $uses);
                    return $data;
        }
        protected function getinfo()
        {
            $column = $this->spid;
        $sortingOrder = 'DESC'; 
        
        if(isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['column'] != '')
        {
            $column = $_REQUEST['order'][0]['column'];
        }  
        if(isset($_REQUEST['order'][0]['dir']) && $_REQUEST['order'][0]['dir'] == 'asc')
        {
            $sortingOrder = 'ASC';
        }  
        $orderBy = '';
        $local = $this->arr['uses_compdown'];
        foreach ($local as $key => $value) {

            $value2 = $value[2];
            $value1 = $value[1];
            if($column==$value2)
            {   
                $orderBy = $value1;
            }
            else
            {
                $orderBy = $this->spid;
            }
        }
        $orderBy = $orderBy.' '.$sortingOrder; 
        $where = '';
        $arrr = $this->arr['chunked'];
        foreach ($arrr as $key => $value) {
            $req = $value[1];                   // name attribute value....
            $req2 = $value[2];                  // ambigous name if available otherwise it will be equal to name attribute....

            if(isset($_REQUEST[$req]) && $_REQUEST[$req]!='')
            {
                 $oR = ($where != '') ? 'OR' : '';
                $where .= $oR." ".$req2." LIKE '%".$_REQUEST[$req]."%' ";
            }
        }
        if($where != ""){

            $where = " AND(".$where.")";
        }
 
        if(isset($_REQUEST['created_at_from']) && $_REQUEST['created_at_from'] != ''){

            $where .= " AND created_at >= '".date('Y-m-d', strtotime($_REQUEST['created_at_from']))."' ";
        }

        if(isset($_REQUEST['created_at_to']) && $_REQUEST['created_at_to'] != ''){

            $where .= " AND created_at <= '".date('Y-m-d', strtotime($_REQUEST['created_at_to']. ' +1 day'))."' ";
        }

        if(isset($_REQUEST['default_date_from']) && $_REQUEST['default_date_from'] != ''){

            $where .= " AND default_date >= '".date('Y-m-d', strtotime($_REQUEST['default_date_from']))."' ";
        }

        if(isset($_REQUEST['default_date_to']) && $_REQUEST['default_date_to'] != ''){

            $where .= " AND default_date <= '".date('Y-m-d', strtotime($_REQUEST['default_date_to']. ' +1 day'))."' ";
        }
        
        $dbQuery = 'SELECT count(party.spid) as total_records 
                    FROM party
                    INNER JOIN level3 ON level3.l3 = party.level3 
                    WHERE party.pid <> 0 '.$where .' and etype="'.$this->etype.'"';
        $dbTotalRecords = $this->commonModel->executeExactString($dbQuery);
        /* 
        * Paging
        */

        $iTotalRecords = $dbTotalRecords[0]['total_records'];
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $dbQuery = 'SELECT party.'.$this->spid.', party.limit_stop,party.op_balance,party.custom_type,party.op_type,party.name, party.uname, party.mobile, party.`limit`, party.contact_person, party.email, party.cnic, party.phone, party.ntn, party.fax, party.city, party.cityarea, party.country, party.etype, party.etype, party.account_id, party.address, party.uaddress, level3.name AS l3_name, party.active, party.default_date, party.created_at
                    FROM party
                    INNER JOIN level3 ON level3.l3 = party.level3 
                    WHERE party.pid <> 0 '.$where.' and etype="'.$this->etype.'" order by '.$orderBy.' limit '.$iDisplayStart.', '.$iDisplayLength;
        $results = $this->commonModel->executeExactString($dbQuery);
        
        $data = [];
        $da = [];
        $data1 = [];
        $i = -1;
        foreach ($results as $key => $result) {
            $i++;
            $labelClass = ($result['active'] == 1) ? 'success' : 'danger';
            $labelText = ($result['active'] == 1) ? 'Active' : 'Inactive';
            $statusClass = ($result['active'] == 1) ? 'btnUnverify red' : 'btnVerify green';
            $statusIcon = ($result['active'] == 1) ? '<i class="fa fa-clock-o"></i>' : '<i class="fa fa-check"></i>';
            $arrr = $this->arr['chunked'];
            $data1[] = array('<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$result[$this->spid].'"/><span></span></label>');
            $newv = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$result[$this->spid].'"/><span></span></label>';
            foreach ($arrr as $key => $value) {
                $value = ($value[1]==='active')? ( '<span class="label label-sm label-'.$labelClass.'">'.$labelText.'</span>' ) : ( ($result[$value[1]])? $result[$value[1]]:'-' );
                $newv .= $value;
                array_push($data1[$i],$value);
            }

            $data[] = array(
                
                ($result['default_date']) ? date('d M Y', strtotime(substr($result['default_date'], 0, 11))) : '-',
                ($result['created_at']) ? date('d M Y', strtotime(substr($result['created_at'], 0, 11))) : '-',
                '<a class="btn btn-sm blue btn-outline '.$statusClass.'" data-'.$this->spid.'="'.$result[$this->spid].'">'.$statusIcon.'</a>
                <a href="#detailPopup" class="btn btn-sm blue btn-outline detailPopup" data-toggle="modal" data-'.$this->spid.'="'.$result[$this->spid].'"><i class="fa fa-eye"></i></a>
                <a href="'.base_url().$this->controller.'/addEdit'.$this->big.'?id='.$result[$this->spid].'" class="btn btn-sm btn-outline blue"><i class="fa fa-pencil"></i></a>
                <a href="javascript:;" class="btn btn-sm btn-outline red btnDelete" data-'.$this->spid.'="'.$result[$this->spid].'"><i class="fa fa-trash"></i></a>',
           );
            $da[] = array_merge($data1[$i],$data[$i]);
             
        }
        $records["data"] = $da;
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
          
        echo json_encode($records);
        exit();
        }
}