<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Items extends MY_Controller {

	public function __construct() {

        parent::__construct();
    	$this->layout = 'default';
    }

    public function index()
	{
		$this->load->view('setup/items/items');
	}

	public function addEditItem()
	{	
		$data['categories'] = array(array('catid' => 'new', 'name' => '+ Add New')) + $this->commonModel->find('category', 'catid, name');
		$data['subcategories'] = array(array('subcatid' => 'new', 'name' => '+ Add New')) + $this->commonModel->find('subcategory', 'subcatid, name');
		$data['brands'] = array(array('bid' => 'new', 'name' => '+ Add New')) + $this->commonModel->find('brand', 'bid, name');
		$data['mades'] = array(array('made_id' => 'new', 'name' => '+ Add New')) + $this->commonModel->find('made', 'made_id, name');
		$data['items'] = $this->commonModel->fetchAllItem();
		$data['jsFiles'] = array('setup/items/addItem');
		$this->load->view('setup/items/addItem', $data);
	}

	public function getItems(){

		$column = 'default';
	    $sortingOrder = 'DESC'; 
	    
	    if(isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['column'] != '')
	    {
	        $column = $_REQUEST['order'][0]['column'];
	    }  

	    if(isset($_REQUEST['order'][0]['dir']) && $_REQUEST['order'][0]['dir'] == 'asc')
	    {
	        $sortingOrder = 'ASC';
	    }  
	    
	    $orderBy = '';
	    switch($column)
	    {
	        case '1':
	            $orderBy = 'item_id';
	            break;
	        case '2':
	            $orderBy = 'item.item_des';
	            break;
	        case '3':
	            $orderBy = 'item.uom';
	            break;
	        case '4':
	            $orderBy = 'category.name';
	            break;
	        case '5':
	            $orderBy = 'subcategory.name';
	            break;
	        case '6':
	            $orderBy = 'brand.name';
	            break;
	        case '7':
	            $orderBy = 'item.srate';
	            break;
	        case '8':
	            $orderBy = 'item.cost_price';
	            break;
	        case '9':
	            $orderBy = 'active';
	            break;
	        case '10':
	            $orderBy = 'created_at';
	            break;
	        case 'default':
	            $orderBy = 'item_id';
	            break;
	    }
	        
	    $orderBy = $orderBy.' '.$sortingOrder; 
	    $where = '';
	    if(isset($_REQUEST['item_id']) && $_REQUEST['item_id'] != ''){

	        $where .= " item_id LIKE '%".$_REQUEST['item_id']."%' ";
	    }
	    if(isset($_REQUEST['item_des']) && $_REQUEST['item_des'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." item.item_des LIKE '%".$_REQUEST['item_des']."%' ";
	    }
	    if(isset($_REQUEST['uom']) && $_REQUEST['uom'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." item.uom LIKE '%".$_REQUEST['uom']."%' ";
	    }
	    if(isset($_REQUEST['cat_name']) && $_REQUEST['cat_name'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." category.name LIKE '%".$_REQUEST['cat_name']."%' ";
	    }
	    if(isset($_REQUEST['sub_cat_name']) && $_REQUEST['sub_cat_name'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." subcategory.name LIKE '%".$_REQUEST['sub_cat_name']."%' ";
	    }
	    if(isset($_REQUEST['brand_name']) && $_REQUEST['brand_name'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." brand.name LIKE '%".$_REQUEST['brand_name']."%' ";
	    }
	    if(isset($_REQUEST['srate']) && $_REQUEST['srate'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." item.srate LIKE '%".$_REQUEST['srate']."%' ";
	    }
	    if(isset($_REQUEST['prate']) && $_REQUEST['prate'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." item.cost_price LIKE '%".$_REQUEST['prate']."%' ";
	    }
	    if(isset($_REQUEST['status']) && $_REQUEST['status'] != ''){
	        
	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." active = ".$_REQUEST['status'];
	    }
	    
	    if($where != ""){

	        $where = " AND(".$where.")";
	    }

	    if(isset($_REQUEST['created_at_from']) && $_REQUEST['created_at_from'] != ''){

	        $where .= " AND item.created_at >= '".date('Y-m-d', strtotime($_REQUEST['created_at_from']))."' ";
	    }

	    if(isset($_REQUEST['created_at_to']) && $_REQUEST['created_at_to'] != ''){

	        $where .= " AND item.created_at <= '".date('Y-m-d', strtotime($_REQUEST['created_at_to']. ' +1 day'))."' ";
	    }
	    
	    $dbQuery = 'SELECT count(item_id) as total_records
					FROM item
					INNER JOIN category ON category.catid = item.catid
					LEFT JOIN subcategory ON subcategory.subcatid = item.subcatid
					LEFT JOIN brand ON brand.bid = item.bid
					LEFT JOIN made ON made.made_id = item.made_id
					WHERE item_id <> 0 '.$where;

		$dbTotalRecords = $this->commonModel->executeExactString($dbQuery);
		/* 
		* Paging
		*/

		$iTotalRecords = $dbTotalRecords[0]['total_records'];
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);

		$dbQuery = 'SELECT item_id, item_des, uom, category.name AS cat_name, subcategory.name AS sub_cat_name, brand.name AS brand_name, item.srate, item.cost_price AS prate, item.active, item.created_at
			FROM item
			INNER JOIN category ON category.catid = item.catid
			LEFT JOIN subcategory ON subcategory.subcatid = item.subcatid
			LEFT JOIN brand ON brand.bid = item.bid
			LEFT JOIN made ON made.made_id = item.made_id
			WHERE item_id <> 0 '.$where.' order by '.$orderBy.' limit '.$iDisplayStart.', '.$iDisplayLength;

		$results = $this->commonModel->executeExactString($dbQuery);
		
		$data = [];
		foreach ($results as $key => $result) {

			$labelClass = ($result['active'] == 1) ? 'success' : 'danger';
			$labelText = ($result['active'] == 1) ? 'Active' : 'Inactive';
			$statusClass = ($result['active'] == 1) ? 'btnUnverify red' : 'btnVerify green';
			$statusIcon = ($result['active'] == 1) ? '<i class="fa fa-clock-o"></i>' : '<i class="fa fa-check"></i>';

		    $data[] = array(
		    	'<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$result['item_id'].'"/><span></span></label>',
		      	$result['item_id'],
		      	($result['item_des']) ? $result['item_des'] : '-',
		      	($result['uom']) ? $result['uom'] : '-',
		      	($result['cat_name']) ? $result['cat_name'] : '-',
		      	($result['sub_cat_name']) ? $result['sub_cat_name'] : '-',
		      	($result['brand_name']) ? $result['brand_name'] : '-',
		      	($result['srate']) ? $result['srate'] : '-',
		      	($result['prate']) ? $result['prate'] : '-',
		      	'<span class="label label-sm label-'.$labelClass.'">'.$labelText.'</span>',
		      	($result['created_at']) ? date('d M Y', strtotime(substr($result['created_at'], 0, 11))) : '-',
		      	'<a class="btn btn-sm blue btn-outline '.$statusClass.'" data-item_id="'.$result['item_id'].'">'.$statusIcon.'</a>
		      	<a href="#detailPopup" class="btn btn-sm blue btn-outline detailPopup" data-toggle="modal" data-item_id="'.$result['item_id'].'"><i class="fa fa-eye"></i></a>
		      	<a href="'.base_url().'items/addEditItem?id='.$result['item_id'].'" class="btn btn-sm btn-outline blue"><i class="fa fa-pencil"></i></a>
		      	<a href="javascript:;" class="btn btn-sm btn-outline red btnDelete" data-item_id="'.$result['item_id'].'"><i class="fa fa-trash"></i></a>',
		   );
		}
		
		$records["data"] = $data;
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		  
		echo json_encode($records);
  		exit();
	}

	public function deleteItems(){

		if($this->input->post()){

			$itemIds = $this->input->post('item_ids');
			if(is_array($itemIds)){

				foreach ($itemIds as $itemId) {
				
					$this->commonModel->delete('item', array('item_id' => $itemId));
				}
			}else{

				$this->commonModel->delete('item', array('item_id' => $itemIds));
			}	
				
			echo json_encode(1);
		}
		exit();
	}

	public function getItemDetail(){

		if($this->input->post()){

			$itemId = $this->input->post('item_id');
			$select = 'item_id, item_code, category.name cat_name, subcategory.name sub_cat_name, brand.name brand_name, made.name made_name, short_code, item_barcode, item_des, uom, cost_price, srate, open_date, min_level, max_level, srate1, srate2, srate3, item_discount, item_pur_discount, item.description, item.created_at';
			$result = $this->commonModel->find('item', $select, array('item_id' => $itemId), array('category', 'subcategory', 'brand', 'made'), array('category.catid = item.catid', 'subcategory.subcatid = item.subcatid', 'brand.bid = item.bid', 'made.made_id = item.made_id'));
			if($result){
				
				$data['result'] = $result[0];
				$html = $this->load->view('setup/items/itemDetail', $data, true);	
			}else{
				
				$html = "<div class='row'>
							<div class='col-md-12'><div class='alert alert-danger'>
                                <button class='close' data-close='alert'></button> No Item Detail found. 
                            </div>
                        </div>";
			}
			
			echo json_encode($html);
			exit();
		}
		exit();
	}

	public function getMaxId() {
		
		$maxId = $this->commonModel->getMaxId('item', 'item_id') + 1;
		echo $maxId;
		exit();
	}

	public function save(){

		$itemDetail = $this->input->post();
		
		$isValid = $this->commonModel->isAlreadySaved('item', array('item_id <>' => $itemDetail['item_id'], 'item_des' => $itemDetail['item_des']));
		
		if (!$isValid) {

			$isValid = $this->commonModel->isAlreadySaved('item', array('item_id <>' => $itemDetail['item_id'], 'short_code' => $itemDetail['short_code'], 'short_code <>' => ''));
		
			if (!$isValid) {

				if($itemDetail['voucher_type_hidden'] == 'new'){

					$maxId = $this->commonModel->getMaxId('item', 'item_id') + 1;
					$itemDetail['item_id'] = $maxId;
				}
				unset($itemDetail['voucher_type_hidden']);

				$where = array('item_id' => $itemDetail['item_id']);
				unset($itemDetail['item_id']);
				
				if (isset($_FILES['photo']) && $_FILES['photo']['size'] > 0) {

		            $itemDetail['photo'] = $this->uploadPhoto('items');
		        } else {
		            
		            unset($itemDetail['photo']);
		        }

		        $subCatId = (isset($itemDetail['subcatid'])) ? $itemDetail['subcatid'] : 0;
         		$itemDetail['item_code'] = $this->commonModel->genItemString($itemDetail['catid'], $subCatId);
         		$itemDetail['active'] = 1;

				$result = $this->commonModel->saveForm('item', $where, $itemDetail);

				echo json_encode($result);
			}else{

				echo json_encode("duplicateshortcode");
			}
		} else {
			
			echo json_encode("duplicate_name");
		}

		exit();
	}

	public function fetch() {

		if ($this->input->post()) {

			$itemId = $this->input->post('item_id');
			$select = 'item_id, item_code, item_des, catid, subcatid, bid, made_id, short_code, item_barcode, cost_price, srate, srate1, srate2, srate3, open_date, uom, min_level, max_level, item_pur_discount, item_discount, description, photo';
			$result = $this->commonModel->find('item', $select, array('item_id' => $itemId));
			echo json_encode($result);
		}
		exit();
	}

	public function changeItemStatus(){

		if($this->input->post()){

			$itemId = $this->input->post('item_id');
			$activeStatus = $this->input->post('active');
			$result = $this->commonModel->update('item', array('active' => $activeStatus), array('item_id' => $itemId));
			
			echo json_encode($result);
			exit();
		}
		exit();
	}

	public function uploadPhoto($folderName)
    {
        $uploadsDirectory = ($_SERVER['DOCUMENT_ROOT'] . '/dtrading/assets/uploads/'.$folderName.'/');

        $errors = array(1 => 'php.ini max file size exceeded',
            2 => 'html form max file size exceeded',
            3 => 'file upload was only partial',
            4 => 'no file was attached');

        ($_FILES['photo']['error'] == 0)
        or die($errors[$_FILES['photo']['error']]);

        @is_uploaded_file($_FILES['photo']['tmp_name'])
        or die('Not an HTTP upload');

        @getimagesize($_FILES['photo']['tmp_name'])
        or die('Only image uploads are allowed');

        $now = time();
        while (file_exists($uploadFilename = $uploadsDirectory . $now . '-' . $_FILES['photo']['name'])) {
            $now++;
        }

        // now let's move the file to its final location and allocate the new filename to it
        move_uploaded_file($_FILES['photo']['tmp_name'], $uploadFilename) or die('Error uploading file');

        $path_parts = explode('/', $uploadFilename);
        $uploadFilename = $path_parts[count($path_parts) - 1];
        return $uploadFilename;
    }
}