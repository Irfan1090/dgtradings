<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Incomes extends MY_Controller {


    protected $controller = "incomes"; 
    protected $small = 'incomes'; 
	protected $big = "Incomes";
	protected $sbig = "Income"; 
	protected $shortname = "Income"; 
	protected $spid = "spid"; 
	protected $custom_type = "custom_type"; 
   	protected $etype = 'incomes';
   	protected $table = 'party';
   	protected $urlToNext ;
   	public $arr; 
	public function __construct()  {

		$this->arr = get_object_vars($this);
        
        parent::__construct();
        $this->arr['urlToNext'] = base_url()."application/views/setup/".$this->small."/add".$this->big.".php"; // pick data for js and detail listing
        $this->arr['join_val'] = "INNER JOIN level3 ON level3.l3 = ".$this->table.".level3"; // we are passing join values if needed to definitions library
        $this->arr['joins'] = array('val1'=>'level3', 'val2'=>'level3.l3 = party.level3'); // keys must be alphanumeric AND SHOULD BE DIFFERENT FROM EACH OTHER
        //THESE ARE THE PARAMETERS 4th,5th AND SO ON.. AND BEING USED IN getDetail() FUNCTION       
        $this->arr['commonModel'] = $this->commonModel;
        $this->load->library('definitions',$this->arr);
    	$this->layout = 'default';
    }
    public function index()
	{
		$this->definitions->loadDefListView();
	}

	public function addEditIncomes()
	{
		$data['countries'] = $this->commonModel->getDistinctFields('party', 'country');
		$data['typess'] = $this->commonModel->getDistinctFields('party', 'etype');
		$data['cities'] = $this->commonModel->getDistinctFields('party', 'city');
		$data['cityareas'] = $this->commonModel->getDistinctFields('party', 'cityarea');
		$data['custom_types'] = $this->commonModel->getDistinctFields('party', 'custom_type');
		$data['acctype'] = $this->commonModel->fetchAllLevel3();
		$data['parties'] = $this->commonModel->fetchAllParties(-1,$this->etype);
		$data['customertypes'] = $this->commonModel->fetchAll('customertype');
		$data['arr'] = $this->arr;
		$data['jsFiles'] = array('setup/generalaccounts/generalaccounts');
		
		$this->load->view('setup/'.$this->small.'/add'.$this->big, $data);
	}

	public function getIncomes(){

		$this->definitions->getinfo();
	}

	public function deleteIncomes(){

		$this->definitions->delDefinitions();
	}

	public function getIncomesDetail(){

		$this->definitions->getDetail();
	}

	public function changeIncomesStatus(){

		$this->definitions->changeStatus();
	}

	public function incomesSearchTable(){

		$this->definitions->loadDefList();
	}

	public function getMaxId() {
		
		$this->definitions->getMaxId();
	}

	public function save(){

		$this->definitions->saveDef();
	}

	public function fetch() {

		$this->definitions->fetchDef();
	}
}