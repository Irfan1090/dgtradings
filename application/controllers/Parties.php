<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parties extends MY_Controller {

	public function __construct() {

        parent::__construct();
    	$this->layout = 'default';
    }

    public function index()
	{
		$this->load->view('setup/parties/parties');
	}

	public function addEditParty()
	{
		$data['countries'] = $this->commonModel->getDistinctFields('party', 'country');
		$data['typess'] = $this->commonModel->getDistinctFields('party', 'etype');
		$data['cities'] = $this->commonModel->getDistinctFields('party', 'city');
		$data['cityareas'] = $this->commonModel->getDistinctFields('party', 'cityarea');
		$data['acctype'] = $this->commonModel->fetchAllLevel3();
		$data['accounts'] = $this->commonModel->fetchAllParties();
		$data['jsFiles'] = array('setup/parties/addParty');
		
		$this->load->view('setup/parties/addParty', $data);
	}

	public function getParties(){

		$column = 'default';
	    $sortingOrder = 'DESC'; 
	    
	    if(isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['column'] != '')
	    {
	        $column = $_REQUEST['order'][0]['column'];
	    }  

	    if(isset($_REQUEST['order'][0]['dir']) && $_REQUEST['order'][0]['dir'] == 'asc')
	    {
	        $sortingOrder = 'ASC';
	    }  
	    
	    $orderBy = '';
	    switch($column)
	    {
	        case '1':
	            $orderBy = 'pid';
	            break;
	        case '2':
	            $orderBy = 'name';
	            break;
	         case '3':
	            $orderBy = 'uname';
	            break;
	        case '4':
	            $orderBy = 'mobile';
	            break;
	        case '5':
	            $orderBy = 'party.limit';
	            break;
	        case '6':
	            $orderBy = 'l3_name';
	            break;
	        case '7':
	            $orderBy = 'contact_person';
	            break;
	        case '8':
	            $orderBy = 'email';
	            break;
	        case '9':
	            $orderBy = 'cnic';
	            break;
	        case '10':
	            $orderBy = 'phone';
	            break;
	        case '11':
	            $orderBy = 'ntn';
	            break;
	        case '12':
	            $orderBy = 'fax';
	            break;
	        case '13':
	            $orderBy = 'address';
	            break;
	        case '14':
	            $orderBy = 'uaddress';
	            break;
	        case '15':
	            $orderBy = 'city';
	            break;
	        case '16':
	            $orderBy = 'cityarea';
	            break;
	        case '17':
	            $orderBy = 'country';
	            break;
	        case '18':
	            $orderBy = 'etype';
	            break;
	        case '19':
	            $orderBy = 'account_id';
	            break;
	        case '20':
	            $orderBy = 'active';
	            break;
	        case '21':
	            $orderBy = 'default_date';
	            break;
	        case '22':
	            $orderBy = 'created_at';
	            break;
	        case 'default':
	            $orderBy = 'pid';
	            break;
	    }
	        
	    $orderBy = $orderBy.' '.$sortingOrder; 
	    $where = '';
	    if(isset($_REQUEST['pid']) && $_REQUEST['pid'] != ''){

	        $where .= " pid LIKE '%".$_REQUEST['pid']."%' ";
	    }
	    if(isset($_REQUEST['name']) && $_REQUEST['name'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." party.name LIKE '%".$_REQUEST['name']."%' ";
	    }
	    if(isset($_REQUEST['uname']) && $_REQUEST['uname'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." party.uname LIKE '%".$_REQUEST['uname']."%' ";
	    }
	    if(isset($_REQUEST['mobile']) && $_REQUEST['mobile'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." mobile LIKE '%".$_REQUEST['mobile']."%' ";
	    }
	    if(isset($_REQUEST['credit_limit']) && $_REQUEST['credit_limit'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." party.limit LIKE '%".$_REQUEST['credit_limit']."%' ";
	    }
	    if(isset($_REQUEST['l3_name']) && $_REQUEST['l3_name'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." l3_name LIKE '%".$_REQUEST['l3_name']."%' ";
	    }
	    if(isset($_REQUEST['contact_person']) && $_REQUEST['contact_person'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." contact_person LIKE '%".$_REQUEST['contact_person']."%' ";
	    }
	    if(isset($_REQUEST['email']) && $_REQUEST['email'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." email LIKE '%".$_REQUEST['email']."%' ";
	    }
	    if(isset($_REQUEST['cnic']) && $_REQUEST['cnic'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." cnic LIKE '%".$_REQUEST['cnic']."%' ";
	    }
	    if(isset($_REQUEST['phone']) && $_REQUEST['phone'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." phone LIKE '%".$_REQUEST['phone']."%' ";
	    }
	    if(isset($_REQUEST['ntn']) && $_REQUEST['ntn'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." ntn LIKE '%".$_REQUEST['ntn']."%' ";
	    }
	    if(isset($_REQUEST['fax']) && $_REQUEST['fax'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." fax LIKE '%".$_REQUEST['fax']."%' ";
	    }
	    if(isset($_REQUEST['fax']) && $_REQUEST['fax'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." fax LIKE '%".$_REQUEST['fax']."%' ";
	    }
	    if(isset($_REQUEST['address']) && $_REQUEST['address'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." address LIKE '%".$_REQUEST['address']."%' ";
	    }
	    if(isset($_REQUEST['uaddress']) && $_REQUEST['uaddress'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." uaddress LIKE '%".$_REQUEST['uaddress']."%' ";
	    }
	    if(isset($_REQUEST['city']) && $_REQUEST['city'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." city LIKE '%".$_REQUEST['city']."%' ";
	    }
	    if(isset($_REQUEST['cityarea']) && $_REQUEST['cityarea'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." cityarea LIKE '%".$_REQUEST['cityarea']."%' ";
	    }
	    if(isset($_REQUEST['country']) && $_REQUEST['country'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." country LIKE '%".$_REQUEST['country']."%' ";
	    }
	    if(isset($_REQUEST['etype']) && $_REQUEST['etype'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." etype LIKE '%".$_REQUEST['etype']."%' ";
	    }
	    if(isset($_REQUEST['account_id']) && $_REQUEST['account_id'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." account_id LIKE '%".$_REQUEST['account_id']."%' ";
	    }
	    if(isset($_REQUEST['status']) && $_REQUEST['status'] != ''){
	        
	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." active = ".$_REQUEST['status'];
	    }
	    
	    if($where != ""){

	        $where = " AND(".$where.")";
	    }

	    if(isset($_REQUEST['created_at_from']) && $_REQUEST['created_at_from'] != ''){

	        $where .= " AND created_at >= '".date('Y-m-d', strtotime($_REQUEST['created_at_from']))."' ";
	    }

	    if(isset($_REQUEST['created_at_to']) && $_REQUEST['created_at_to'] != ''){

	        $where .= " AND created_at <= '".date('Y-m-d', strtotime($_REQUEST['created_at_to']. ' +1 day'))."' ";
	    }

	    if(isset($_REQUEST['default_date_from']) && $_REQUEST['default_date_from'] != ''){

	        $where .= " AND default_date >= '".date('Y-m-d', strtotime($_REQUEST['default_date_from']))."' ";
	    }

	    if(isset($_REQUEST['default_date_to']) && $_REQUEST['default_date_to'] != ''){

	        $where .= " AND default_date <= '".date('Y-m-d', strtotime($_REQUEST['default_date_to']. ' +1 day'))."' ";
	    }
	    
	    $dbQuery = 'SELECT count(party.pid) as total_records 
	    			FROM party
					INNER JOIN level3 ON level3.l3 = party.level3 
					WHERE party.pid <> 0 '.$where;

		$dbTotalRecords = $this->commonModel->executeExactString($dbQuery);
		/* 
		* Paging
		*/

		$iTotalRecords = $dbTotalRecords[0]['total_records'];
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);

		$dbQuery = 'SELECT party.pid, party.name, party.uname, party.mobile, party.`limit`, party.contact_person, party.email, party.cnic, party.phone, party.ntn, party.fax, party.city, party.cityarea, party.country, party.etype, party.etype, party.account_id, party.address, party.uaddress, level3.name AS l3_name, party.active, party.default_date, party.created_at
					FROM party
					INNER JOIN level3 ON level3.l3 = party.level3 
					WHERE party.pid <> 0 '.$where.' order by '.$orderBy.' limit '.$iDisplayStart.', '.$iDisplayLength;

		$results = $this->commonModel->executeExactString($dbQuery);
		
		$data = [];
		foreach ($results as $key => $result) {
			
			$labelClass = ($result['active'] == 1) ? 'success' : 'danger';
			$labelText = ($result['active'] == 1) ? 'Active' : 'Inactive';
			$statusClass = ($result['active'] == 1) ? 'btnUnverify red' : 'btnVerify green';
			$statusIcon = ($result['active'] == 1) ? '<i class="fa fa-clock-o"></i>' : '<i class="fa fa-check"></i>';

		    $data[] = array(
		    	'<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$result['pid'].'"/><span></span></label>',
		      	$result['pid'],
		      	($result['name']) ? $result['name'] : '-',
		      	($result['uname']) ? $result['uname'] : '-',
		      	($result['mobile']) ? $result['mobile'] : '-',
		      	($result['limit']) ? $result['limit'] : '-',
		      	($result['l3_name']) ? $result['l3_name'] : '-',
		      	($result['contact_person']) ? $result['contact_person'] : '-',
		      	($result['email']) ? $result['email'] : '-',
		      	($result['cnic']) ? $result['cnic'] : '-',
		      	($result['phone']) ? $result['phone'] : '-',
		      	($result['ntn']) ? $result['ntn'] : '-',
		      	($result['fax']) ? $result['fax'] : '-',
		      	($result['address']) ? $result['address'] : '-',
		      	($result['uaddress']) ? $result['uaddress'] : '-',
		      	($result['city']) ? $result['city'] : '-',
		      	($result['cityarea']) ? $result['cityarea'] : '-',
		      	($result['country']) ? $result['country'] : '-',
		      	($result['etype']) ? $result['etype'] : '-',
		      	($result['account_id']) ? $result['account_id'] : '-',
		      	'<span class="label label-sm label-'.$labelClass.'">'.$labelText.'</span>',
		      	($result['default_date']) ? date('d M Y', strtotime(substr($result['default_date'], 0, 11))) : '-',
		      	($result['created_at']) ? date('d M Y', strtotime(substr($result['created_at'], 0, 11))) : '-',
		      	'<a class="btn btn-sm blue btn-outline '.$statusClass.'" data-pid="'.$result['pid'].'">'.$statusIcon.'</a>
		      	<a href="#detailPopup" class="btn btn-sm blue btn-outline detailPopup" data-toggle="modal" data-pid="'.$result['pid'].'"><i class="fa fa-eye"></i></a>
		      	<a href="'.base_url().'parties/addEditParty?id='.$result['pid'].'" class="btn btn-sm btn-outline blue"><i class="fa fa-pencil"></i></a>
		      	<a href="javascript:;" class="btn btn-sm btn-outline red btnDelete" data-pid="'.$result['pid'].'"><i class="fa fa-trash"></i></a>',
		   );
		}
		
		$records["data"] = $data;
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		  
		echo json_encode($records);
  		exit();
	}

	public function deleteParties(){

		if($this->input->post()){

			$pIds = $this->input->post('p_ids');
			if(is_array($pIds)){

				foreach ($pIds as $pId) {
				
					$this->commonModel->delete('party', array('pid' => $pId));
				}
			}else{

				$this->commonModel->delete('party', array('pid' => $pIds));
			}	
				
			echo json_encode(1);
		}
		exit();
	}

	public function getPartyDetail(){

		if($this->input->post()){

			$partyId = $this->input->post('p_id');
			$select = 'pid, active, party.name, mobile, address, level3.name as l3_name, contact_person, email, fax, country, city, cityarea, cnic, phone, ntn, account_id, party.limit, etype, default_date, party.created_at';
			$result = $this->commonModel->find('party', $select, array('pid' => $partyId), 'level3', 'level3.l3 = party.level3');
			if($result){
				
				$data['result'] = $result[0];
				$html = $this->load->view('setup/parties/partyDetail', $data, true);	
			}else{
				
				$html = "<div class='row'>
							<div class='col-md-12'><div class='alert alert-danger'>
                                <button class='close' data-close='alert'></button> No Party Detail found. 
                            </div>
                        </div>";
			}
			
			echo json_encode($html);
			exit();
		}
		exit();
	}

	public function changePartyStatus(){

		if($this->input->post()){

			$partyId = $this->input->post('p_id');
			$activeStatus = $this->input->post('active');
			$result = $this->commonModel->update('party', array('active' => $activeStatus), array('pid' => $partyId));
			
			echo json_encode($result);
			exit();
		}
		exit();
	}

	public function partiesSearchTable(){

		$column = 'default';
	    $sortingOrder = 'DESC'; 
	    
	    if(isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['column'] != '')
	    {
	        $column = $_REQUEST['order'][0]['column'];
	    }  

	    if(isset($_REQUEST['order'][0]['dir']) && $_REQUEST['order'][0]['dir'] == 'asc')
	    {
	        $sortingOrder = 'ASC';
	    }  
	    
	    $orderBy = '';
	    switch($column)
	    {
	        case '0':
	            $orderBy = 'pid';
	            break;
	        case '1':
	            $orderBy = 'name';
	            break;
	         case '2':
	            $orderBy = 'mobile';
	            break;
	        case '3':
	            $orderBy = 'address';
	            break;
	        case 'default':
	            $orderBy = 'pid';
	            break;
	    }
	        
	    $orderBy = $orderBy.' '.$sortingOrder; 
	    $where = '';
	    if(isset($_REQUEST['search']['value']) && $_REQUEST['search']['value'] != ''){

	    	$search = $_REQUEST['search']['value'];
	        $where = " pid LIKE '%".$search."%' OR party.name LIKE '%".$search."%' OR party.mobile LIKE '%".$search."%' OR party.address LIKE '%".$search."%'";
	    }
	    
	    if($where != ""){

	        $where = " AND(".$where.")";
	    }
	    
	    $dbQuery = 'SELECT count(party.pid) as total_records 
	    			FROM party
					INNER JOIN level3 ON level3.l3 = party.level3 
					WHERE party.pid <> 0 '.$where;

		$dbTotalRecords = $this->commonModel->executeExactString($dbQuery);
		/* 
		* Paging
		*/

		$iTotalRecords = $dbTotalRecords[0]['total_records'];
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);

		$dbQuery = 'SELECT party.pid, party.name, party.mobile, party.address, party.active
					FROM party
					INNER JOIN level3 ON level3.l3 = party.level3 
					WHERE party.pid <> 0 '.$where.' order by '.$orderBy.' limit '.$iDisplayStart.', '.$iDisplayLength;

		$results = $this->commonModel->executeExactString($dbQuery);
		
		$data = [];
		foreach ($results as $key => $result) {
			
			$data[] = array(
		    	
		    	$result['pid'],
		      	($result['name']) ? $result['name'] : '-',
		      	($result['mobile']) ? $result['mobile'] : '-',
		      	($result['address']) ? $result['address'] : '-',
		      	'<a href="javascript:;" class="btn btn-sm btn-outline btnPartyEdit blue" data-dismiss="modal" data-pid="'.$result['pid'].'"><i class="fa fa-pencil"></i></a>',
		   );
		}
		
		$records["data"] = $data;
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		  
		echo json_encode($records);
  		exit();
	}

	public function getMaxId() {
		
		$maxId = $this->commonModel->getMaxId('party', 'pid') + 1;
		echo $maxId;
		exit();
	}

	public function save(){

		$accountDetail = $this->input->post('account_detail');
		$isValid = $this->commonModel->isAlreadySaved('party', array('pid <>' => $accountDetail['pid'], 'name' => $accountDetail['name']));
		
		if (!$isValid) {

			$accountDetail['account_id'] = $this->commonModel->genAccStr($accountDetail['level3']);

			if($_POST['voucher_type_hidden'] == 'new'){

				$maxId = $this->commonModel->getMaxId('party', 'pid') + 1;
				$accountDetail['pid'] = $maxId;
			}

			$where = array('pid' => $accountDetail['pid']);
			if(isset($accountDetail['default_date']) && $accountDetail['default_date'] != ''){

				$accountDetail['default_date'] = date('Y-m-d', strtotime($accountDetail['default_date']));	
			}
			
			unset($accountDetail['pid']);
			
			$result = $this->commonModel->saveForm('party', $where, $accountDetail);

			echo json_encode($result);
		} else {
			
			echo json_encode("duplicate_name");
		}

		exit();
	}

	public function fetch() {

		if ($this->input->post()) {

			$partyId = $this->input->post('pid');
			$select = 'pid, name, uname, mobile, `limit`, contact_person, email, cnic, phone, ntn, fax, city, cityarea, country, etype, etype, account_id, address, uaddress, level3, active, default_date';
			$result = $this->commonModel->find('party', $select, array('pid' => $partyId));
			if($result){
				$result[0]['default_date'] = date('d M y', strtotime($result[0]['default_date']));
			}
			echo json_encode($result);
		}
		exit();
	}
}