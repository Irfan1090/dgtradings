<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suppliers extends MY_Controller {
	private $controller = "suppliers"; 
    private $small = 'suppliers'; 
	private $big = "Suppliers";
	private $sbig = "Supplier"; 
	protected $shortname = "Supplier"; 
	protected $spid = "spid"; 
	protected $custom_type = "custom_type"; 
   	protected $etype = 'suppliers';
   	protected $table = 'party';
   	public $arr; 
	public function __construct()  {

		$this->arr = get_object_vars($this);
        
        parent::__construct();
        $this->arr['urlToNext'] = base_url()."application/views/setup/".$this->small."/add".$this->big.".php"; // pick data for js and detail listing
        $this->arr['join_val'] = "INNER JOIN level3 ON level3.l3 = ".$this->table.".level3"; // we are passing join values if needed to definitions library
        $this->arr['joins'] = array('val1'=>'level3', 'val2'=>'level3.l3 = party.level3'); // keys must be alphanumeric AND SHOULD BE DIFFERENT FROM EACH OTHER
        //THESE ARE THE PARAMETERS 4th,5th AND SO ON.. AND BEING USED IN getDetail() FUNCTION
        $this->arr['commonModel'] = $this->commonModel;
        $this->load->library('definitions',$this->arr);
    	$this->layout = 'default';
    }
    public function index()
	{
		$this->definitions->loadDefListView();
	}

	public function addEditSuppliers()
	{
		$data['countries'] = $this->commonModel->getDistinctFields('party', 'country');
		$data['typess'] = $this->commonModel->getDistinctFields('party', 'etype');
		$data['cities'] = $this->commonModel->getDistinctFields('party', 'city');
		$data['cityareas'] = $this->commonModel->getDistinctFields('party', 'cityarea');
		$data['custom_types'] = $this->commonModel->getDistinctFields('party', 'custom_type');
		$data['arr'] = $this->arr;
		$data['parties'] = $this->commonModel->fetchAllParties(1,$this->etype);
		$data['customertypes'] = $this->commonModel->fetchAll('customertype');
		$data['jsFiles'] = array('setup/generalaccounts/generalaccounts');
		
		$this->load->view('setup/'.$this->small.'/add'.$this->big, $data);
	}

	public function getSuppliers(){

		$this->definitions->getinfo();
	}

	public function deleteSuppliers(){

		$this->definitions->delDefinitions();
	}

	public function getSuppliersDetail(){

		$this->definitions->getDetail();
	}

	public function changeSuppliersStatus(){

		$this->definitions->changeStatus();
	}

	public function suppliersSearchTable(){

		$this->definitions->loadDefList();
	}

	public function getMaxId() {
		
		$this->definitions->getMaxId();
	}

	public function save(){

		$this->definitions->saveDef();
	}

	public function fetch() {

		$this->definitions->fetchDef();
	}
}