<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {

        parent::__construct();
        
    }

	public function index()
	{
		$this->layout = 'default';
		$this->load->view('dashboard');
	}

	public function changeSideMenu(){

		$isClosed = $this->input->post('is_closed');
		$activeStatus = $this->input->post('active');
		$result = $this->commonModel->update('user', array('is_closed' => $isClosed), array('uid' => $this->session->userdata('user_id')));
		if($result){

			$this->session->set_userdata(array('is_closed' => $isClosed));
		}
		echo json_encode($result);
		exit();
	}
}
