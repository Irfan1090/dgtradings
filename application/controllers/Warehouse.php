<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Warehouse extends MY_Controller {

	public function __construct() {

        parent::__construct();
    	$this->layout = 'default';
    }

    public function index()
	{
		$this->load->view('setup/warehouse/warehouses');
	}

	public function addEditWarehouse()
	{	
		$data['jsFiles'] = array('setup/warehouse/addWarehouse');
		$this->load->view('setup/warehouse/addWarehouse', $data);
	}

	public function getWarehouses(){

		$column = 'default';
	    $sortingOrder = 'DESC'; 
	    
	    if(isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['column'] != '')
	    {
	        $column = $_REQUEST['order'][0]['column'];
	    }  

	    if(isset($_REQUEST['order'][0]['dir']) && $_REQUEST['order'][0]['dir'] == 'asc')
	    {
	        $sortingOrder = 'ASC';
	    }  
	    
	    $orderBy = '';
	    switch($column)
	    {
	        case '1':
	            $orderBy = 'did';
	            break;
	        case '2':
	            $orderBy = 'name';
	            break;
	        case '3':
	            $orderBy = 'description';
	            break;
	        case '4':
	            $orderBy = 'created_at';
	            break;
	        case 'default':
	            $orderBy = 'did';
	            break;
	    }
	        
	    $orderBy = $orderBy.' '.$sortingOrder; 
	    $where = '';
	    if(isset($_REQUEST['did']) && $_REQUEST['did'] != ''){

	        $where .= " did LIKE '%".$_REQUEST['did']."%' ";
	    }
	    if(isset($_REQUEST['name']) && $_REQUEST['name'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." name LIKE '%".$_REQUEST['name']."%' ";
	    }
	    if(isset($_REQUEST['description']) && $_REQUEST['description'] != ''){

	        $oR = ($where != '') ? 'OR' : '';
	        $where .= $oR." description LIKE '%".$_REQUEST['description']."%' ";
	    }
	    
	    if($where != ""){

	        $where = " AND(".$where.")";
	    }

	    if(isset($_REQUEST['created_at_from']) && $_REQUEST['created_at_from'] != ''){

	        $where .= " AND created_at >= '".date('Y-m-d', strtotime($_REQUEST['created_at_from']))."' ";
	    }

	    if(isset($_REQUEST['created_at_to']) && $_REQUEST['created_at_to'] != ''){

	        $where .= " AND created_at <= '".date('Y-m-d', strtotime($_REQUEST['created_at_to']. ' +1 day'))."' ";
	    }
	    
	    $dbQuery = 'SELECT count(did) as total_records 
	    			FROM department
					WHERE did <> 0 '.$where;

		$dbTotalRecords = $this->commonModel->executeExactString($dbQuery);
		/* 
		* Paging
		*/

		$iTotalRecords = $dbTotalRecords[0]['total_records'];
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);

		$dbQuery = 'SELECT did, name, description, created_at
					FROM department
					WHERE did <> 0 '.$where.' order by '.$orderBy.' limit '.$iDisplayStart.', '.$iDisplayLength;

		$results = $this->commonModel->executeExactString($dbQuery);
		
		$data = [];
		foreach ($results as $key => $result) {

		    $data[] = array(
		    	'<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$result['did'].'"/><span></span></label>',
		      	$result['did'],
		      	($result['name']) ? $result['name'] : '-',
		      	($result['description']) ? $result['description'] : '-',
		      	($result['created_at']) ? date('d M Y', strtotime(substr($result['created_at'], 0, 11))) : '-',
		      	'<a href="#detailPopup" class="btn btn-sm blue btn-outline detailPopup" data-toggle="modal" data-did="'.$result['did'].'"><i class="fa fa-eye"></i></a>
		      	<a href="'.base_url().'warehouse/addEditWarehouse?id='.$result['did'].'" class="btn btn-sm btn-outline blue"><i class="fa fa-pencil"></i></a>
		      	<a href="javascript:;" class="btn btn-sm btn-outline red btnDelete" data-did="'.$result['did'].'"><i class="fa fa-trash"></i></a>',
		   );
		}
		
		$records["data"] = $data;
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		  
		echo json_encode($records);
  		exit();
	}

	public function getWarehouseDetail(){

		if($this->input->post()){

			$dId = $this->input->post('d_id');
			$select = 'did, name, description, created_at';
			$result = $this->commonModel->find('department', $select, array('did' => $dId));
			if($result){
				
				$data['result'] = $result[0];
				$html = $this->load->view('setup/warehouse/warehouseDetail', $data, true);	
			}else{
				
				$html = "<div class='row'>
							<div class='col-md-12'><div class='alert alert-danger'>
                                <button class='close' data-close='alert'></button> No warehouse detail found. 
                            </div>
                        </div>";
			}
			
			echo json_encode($html);
			exit();
		}
		exit();
	}

	public function getMaxId() {
		
		$maxId = $this->commonModel->getMaxId('department', 'did') + 1;
		echo $maxId;
		exit();
	}

	public function save(){

		$warehouseDetail = $this->input->post('warehouse_detail');
		$isValid = $this->commonModel->isAlreadySaved('department', array('did <>' => $warehouseDetail['did'], 'name' => $warehouseDetail['name']));
		
		if (!$isValid) {

			if($_POST['voucher_type_hidden'] == 'new'){

				$maxId = $this->commonModel->getMaxId('department', 'did') + 1;
				$warehouseDetail['did'] = $maxId;
			}

			$where = array('did' => $warehouseDetail['did']);
			unset($warehouseDetail['did']);
			
			$result = $this->commonModel->saveForm('department', $where, $warehouseDetail);

			echo json_encode($result);
		} else {
			
			echo json_encode("duplicate_name");
		}

		exit();
	}

	public function fetch() {

		if ($this->input->post()) {

			$dId = $this->input->post('d_id');
			$select = 'did, name, description';
			$result = $this->commonModel->find('department', $select, array('did' => $dId));
			echo json_encode($result);
		}
		exit();
	}

	public function deleteWarehouses(){

		if($this->input->post()){

			$dIds = $this->input->post('d_ids');
			if(is_array($dIds)){

				foreach ($dIds as $dId) {
				
					$this->commonModel->delete('department', array('did' => $dId));
				}
			}else{

				$this->commonModel->delete('department', array('did' => $dIds));
			}	
				
			echo json_encode(1);
		}
		exit();
	}
}