<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Level3 extends MY_Controller {

	private $controller = "level3"; 
    private $small = 'level3'; 
	private $big = "Level3";
	private $sbig = "Level3"; 
	protected $shortname = "Level3"; 
	protected $spid = "l3"; 
   	protected $etype = '';
   	protected $table = 'level3';
   	public $arr; 
	public function __construct()  {

		$this->arr = get_object_vars($this);
        
        parent::__construct();
        $this->arr['urlToNext'] = base_url()."application/views/setup/levels/add".$this->big.".php"; // pick data for js and detail listing
        $this->arr['commonModel'] = $this->commonModel; // we are passing model object to definitions library
        $this->arr['join_val'] = "LEFT JOIN level2 ON level2.l2 = level3.l2"; // we are passing join values if needed to definitions library // LISTING PAGE FETCH JOINS
        $this->arr['joins'] = array(); // keys must be alphanumeric AND SHOULD BE DIFFERENT FROM EACH OTHER VOUCHER FETCH JOINS
        //THESE ARE THE PARAMETERS 4th,5th AND SO ON.. AND BEING USED IN getDetail() FUNCTION
        $this->load->library('definitions',$this->arr);
    	$this->layout = 'default';
    }

    public function index()
	{
		$this->definitions->loadDefListView();
	}

	public function addEditLevel3()
	{	
		$data['arr'] = $this->arr;
		$data['level2s'] = $this->commonModel->find('level2', 'l2, name');
		$data['jsFiles'] = array('setup/generalaccounts/generalaccounts');
		$this->load->view('setup/levels/add'.$this->big, $data);
	}

	public function getLevel3(){

		$this->definitions->getinfo();
	}

	public function getLevel3Detail(){

		$this->definitions->getDetail();
	}
	public function changeLevel3Status(){

		$this->definitions->changeStatus();
	}
	public function getMaxId() {
		
		$this->definitions->getMaxId();
	}

	public function save(){

		$this->definitions->saveDef();
	}

	public function fetch() {

		$this->definitions->fetchDef();
	}

	public function deleteLevel3(){

		$this->definitions->delDefinitions();
	}
}