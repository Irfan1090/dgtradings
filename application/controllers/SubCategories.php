<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SubCategories extends MY_Controller {

	private $controller = "subcategories"; 
    private $small = 'subcategories'; 
	private $big = "SubCategories";
	private $sbig = "SubCategory"; 
	protected $shortname = "Subcategory"; 
	protected $spid = "subcatid"; 
   	protected $etype = '';
   	protected $table = 'subcategory';
   	public $arr; 
	public function __construct()  {

		$this->arr = get_object_vars($this);
        
        parent::__construct();
        $this->arr['urlToNext'] = base_url()."application/views/setup/".$this->small."/add".$this->big.".php"; // pick data for js and detail listing
        $this->arr['commonModel'] = $this->commonModel; // we are passing model object to definitions library
        $this->arr['join_val'] = " left join category on category.catid=subcategory.subcatid"; // we are passing join values if needed to definitions library
        $this->arr['joins'] = array('val1'=>'category', 'val2'=>'category.catid = subcategory.catid'); // keys must be alphanumeric AND SHOULD BE DIFFERENT FROM EACH OTHER
        //THESE ARE THE PARAMETERS 4th,5th AND SO ON.. AND BEING USED IN getDetail() FUNCTION
        $this->load->library('definitions',$this->arr);
    	$this->layout = 'default';
    }

    public function index()
	{
		$this->definitions->loadDefListView();
	}

	public function addEditSubCategories()
	{	
		$data['categories'] = $this->commonModel->find('category', 'catid, name');
		$data['arr'] = $this->arr;
		$data['jsFiles'] = array('setup/generalaccounts/generalaccounts');
		$this->load->view('setup/'.$this->small.'/add'.$this->big, $data);
	}

	public function getSubCategories(){

		$this->definitions->getinfo();
	}
	public function getSubCategoryDetail(){

		$this->definitions->getDetail();	
	}
	public function changeSubcategoriesStatus(){

		$this->definitions->changeStatus();
	}
	public function getMaxId() {
		
		$this->definitions->getMaxId();
	}
	public function deleteSubCategories(){

		$this->definitions->delDefinitions();
	}

	public function save(){

		$this->definitions->saveDef();
	}

	public function fetch() {

		$this->definitions->fetchDef();
	}
}