<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MY_Controller {

	private $controller = "categories"; 
    private $small = 'categories'; 
	private $big = "Categories";
	private $sbig = "Categorie"; 
	protected $shortname = "Category"; 
	protected $spid = "catid"; 
   	protected $etype = '';
   	protected $table = 'category';
   	public $arr; 
	public function __construct()  {

		$this->arr = get_object_vars($this);
        
        parent::__construct();
        $this->arr['urlToNext'] = base_url()."application/views/setup/".$this->small."/add".$this->big.".php"; // pick data for js and detail listing
        $this->arr['commonModel'] = $this->commonModel; // we are passing model object to definitions library
        $this->arr['join_val'] = " left join subcategory sc on ".$this->table.".catid = sc.subcatid"; // we are passing join values if needed to definitions library
        $this->arr['joins'] = array('val1'=>FALSE, 'val2'=>FALSE); // keys must be alphanumeric AND SHOULD BE DIFFERENT FROM EACH OTHER
        //THESE ARE THE PARAMETERS 4th,5th AND SO ON.. AND BEING USED IN getDetail() FUNCTION
        $this->load->library('definitions',$this->arr);
    	$this->layout = 'default';
    }

    public function index()
	{
		$this->definitions->loadDefListView();
	}

	public function addEditCategory()
	{	
		$data['arr'] = $this->arr;
		$data['jsFiles'] = array('setup/generalaccounts/generalaccounts');
		$this->load->view('setup/'.$this->small.'/add'.$this->big, $data);
	}

	public function getCategories(){

		$this->definitions->getinfo();
	}
	public function getCategoryDetail(){

		$this->definitions->getDetail();	
	}
	public function changeCategoriesStatus(){

		$this->definitions->changeStatus();
	}
	public function getMaxId() {
		
		$this->definitions->getMaxId();
	}
	public function save(){

		$this->definitions->saveDef();
	}
	public function fetch() {

		$this->definitions->fetchDef();
	}
	public function deleteCategories(){

		$this->definitions->delDefinitions();
	}
}