<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banks extends MY_Controller {
	private $controller = "banks"; 
    private $small = 'banks'; 
	private $big = "Banks";
	private $sbig = "Bank"; 
	protected $shortname = "Bank"; 
	protected $spid = "spid"; 
   	protected $etype = 'banks';
   	protected $table = 'party';
   	public $arr; 
	public function __construct()  {

		$this->arr = get_object_vars($this);
        
        parent::__construct();
        $this->arr['urlToNext'] = base_url()."application/views/setup/".$this->small."/add".$this->big.".php"; // pick data for js and detail listing
        $this->arr['commonModel'] = $this->commonModel; // we are passing model object to definitions library
        $this->arr['join_val'] = "INNER JOIN level3 ON level3.l3 = ".$this->table.".level3"; // we are passing join values if needed to definitions library
        $this->arr['joins'] = array('val1'=>'level3', 'val2'=>'level3.l3 = party.level3'); // keys must be alphanumeric AND SHOULD BE DIFFERENT FROM EACH OTHER
        //THESE ARE THE PARAMETERS 4th,5th AND SO ON.. AND BEING USED IN getDetail() FUNCTION
        $this->load->library('definitions',$this->arr);
    	$this->layout = 'default';
    }

    public function index()
	{
		$this->definitions->loadDefListView();
	}

	public function addEditBanks()
	{
		$data['arr'] = $this->arr;
		$data['parties'] = $this->commonModel->fetchAllParties(1,$this->etype);
		$data['jsFiles'] = array('setup/generalaccounts/generalaccounts');
		$this->load->view('setup/'.$this->small.'/add'.$this->big, $data);
	}

	public function getBanks(){

		$this->definitions->getinfo();
	}

	public function deleteBanks(){

		$this->definitions->delDefinitions();
	}

	public function getBanksDetail(){

		$this->definitions->getDetail();
	}

	public function changeBanksStatus(){

		$this->definitions->changeStatus();
	}

	public function banksSearchTable(){

		$this->definitions->loadDefList();
	}

	public function getMaxId() {
		
		$this->definitions->getMaxId();
	}

	public function save(){

		$this->definitions->saveDef();
	}

	public function fetch() {

		$this->definitions->fetchDef();
	}
}