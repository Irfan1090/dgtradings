<?php

class CommonModel extends CI_Model {

    function __construct() {

        parent::__construct();
        $this->db->flush_cache();
    }

    public function save($table, $data) {
        
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update($table, $data, $where) {
      
        $this->db->update($table, $data, $where);
        return $this->db->affected_rows();
    }

    public function find($table, $select = false, $where = false, $joins = false, $joins_on = false, $group = false, $order = false, $having = false, $limit1 = false, $limit2 = false, $where_in = FALSE, $where_in_col = FALSE) {
        
        if ($select) {
        
            $this->db->select($select, false);
        } else {
        
            $this->db->select('*');
        }

        if (is_array($joins) && is_array($joins_on)) {
        
            $index = 0;
            foreach ($joins as $join) {
                $this->db->join($join, $joins_on[$index], 'left');
                $index++;
            }
        } else {
        
            if ($joins && $joins_on) {
        
                $this->db->join($joins, $joins_on, 'left');
            }
        }

        if ($where) {
        
            //$where array or string
            $this->db->where($where);
        }
        
        if ($where_in) {
        
            //$where array or string
            $this->db->where_in($where_in_col, $where_in);
        }
        
        if ($group) {
        
            //$group array or string
            $this->db->group_by($group);
        }
        
        if ($order) {
        
            //$order string
            $this->db->order_by($order);
        }
        
        if ($having) {
        
            //$having string or array
            $this->db->having($having);
        }
        
        if (!$limit2 && $limit1) {
        
            $this->db->limit($limit1);
        } else {
        
            if ($limit1) {
        
                $this->db->limit($limit1, $limit2);
            }
        }
        
        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {
        
            return $query->result_array();
        } else {
        
            return false;
        }
    }

    public function delete($table, $where = false) {
        
        $this->db->delete($table, $where);
        return $this->db->affected_rows();
    }

    public function fetchAll($table = FALSE) {
        
        $result = $this->db->get($table)->result_array();
        return $result;
    }
    
    public function countAllRecords($table) {
        
        $result = $this->db->select('*')
                ->from($table)
                ->count_all_results();
        return $result;
    }
    
    public function countByCondition($table, $where, $joins = FALSE, $joins_on = TRUE) {
        
        $this->db->select('*');
        $this->db->from($table);
        if (is_array($joins) && is_array($joins_on)) {
        
            $index = 0;
            foreach ($joins as $join) {
        
                $this->db->join($join, $joins_on[$index], 'left');
                $index++;
            }
        } else {
        
            if ($joins && $joins_on) {
        
                $this->db->join($joins, $joins_on, 'left');
            }
        }
        
        $this->db->where($where);
        $result = $this->db->count_all_results();
        return $result;
    }

    public function findByCondition($table, $fields, $where) {
        
        $result = $this->db->select($fields)
                ->from($table)
                ->where($where)
                ->get()
                ->result_array();
        return $result;
    }

    public function getMax($table, $column, $where = FALSE) {
        
        $result = $this->db->select_max($column)
                ->from($table)
                ->where($where)
                ->get()
                ->result_array();
        return $result;
    }

    public function getMaxId($table, $column,$where="optional") {

        if($where==='optional')
        {
            $where = array('1'=>'1');
        }
        $result = $this->db->select_max($column)
                ->from($table)
                ->where($where)
                ->get()
                ->row_array();
        $maxId = $result[$column];
      
        return $maxId;
    }

    public function updateByCondition($table, $where, $data) {
        
        $this->db->where($where);
        $this->db->update($table, $data);
        return $this->db->affected_rows();
    }

    public function executeExactString($query) {
        
        $res = $this->db->query($query)
                ->result_array();
        return $res;
    }

    public function executeExactInsertString($query) {
        
        $res = $this->db->query($query);
        return $res;
    }

    public function executeExactStringUpdate($query) {
        
        $res = $this->db->query($query);
        return $res;
    }

    public function executeMysqlQuery($query) {
        
        $res = mysqli_multi_query($this->db->conn_id, $query) or die(mysqli_error($this->db->conn_id));
        $this->db->close();
        return $res;
    }

    public function post($data){
    
        $this->db->insert('users',$data);
    }

    public function getDistinctFields($table, $column) {

        $result = $this->db->distinct($column)
                ->select($column)
                ->from($table)
                ->where($column.' !=', '')
                ->get()
                ->result_array();
        return $result;
    }

    public function fetchAllLevel3() {

        $result = $this->db->select('level3.l3, level3.name AS level3_name, level2.name AS level2_name, level1.name AS level1_name')
                ->from('level3')
                ->join('level2', 'level2.l2 = level3.l2', 'inner')
                ->join('level1', 'level1.l1 = level2.l1', 'inner')
                ->get()
                ->result_array();
        return $result;
    }

    public function fetchAllParties($activee=-1,$specific='noval', $typee="all") {

        $crit="";
        if($specific!=='noval' && !empty($specific))
        {
            $crit=" and party.etype='$specific'  ";
        }
        

        if ($typee != 'all')
        {
            /*if($typee == "purchase" || $typee == "purchase return")
            {
                $crit = " and level3.name in ('creditors','CASH & BANK')";
            }
            elseif($typee == "sale" || $typee == "sale return")
            {
                $crit = " and level3.name in ('debitors','CASH & BANK')";
            }
            else
            {
                $crit = " and level3.name in ('debitors','creditors','customers','suppliers')";
            }*/
        }

        if ($activee == -1){
            
            $dbQuery = "SELECT `party`.pid,`party`.spid, `party`.name, `party`.limit, `party`.account_id, `party`.mobile, `party`.address, `party`.level3, level3.name AS level3_name, `party`.city, `party`.cityarea
                FROM `party`
                INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3`
                WHERE party.name <>'' $crit";
            $result = $this->db->query($dbQuery);
        }else{
            
            $dbQuery = "SELECT pl.balance, `party`.spid,`party`.limit_stop,party.city,party.address,party.cityarea,party.mobile,`party`.pid, `party`.name, `party`.limit, `party`.account_id, `party`.mobile, `party`.address, `party`.level3, level3.name AS level3_name
                FROM `party`
                INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3`
                LEFT JOIN (
                SELECT IFNULL(SUM(debit),0)- IFNULL(SUM(credit),0) AS balance,pid
                FROM pledger
                GROUP BY pid) AS pl ON pl.pid
                 = `party`.`pid`
                WHERE `party`.`active`=$activee $crit ";
            $result = $this->db->query($dbQuery);
        }
        
        return $result->result_array();
    }

    public function isAlreadySaved($table, $where)
    {
        $result = $this->db->select('*')
                ->from($table)
                ->where($where)
                ->get();
        if ($result->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function genAccStr($level3)
    {
        $result = $this->db->select('level3.l3, level2.l2, level1.l1')
                ->from('level3')
                ->join('level2', 'level2.l2 = level3.l2', 'inner')
                ->join('level1', 'level1.l1 = level2.l1', 'inner')
                ->where(array('level3.l3' => $level3))
                ->get()
                ->result_array();
        
        if ($result) {
            
            $l1 = str_pad($result[0]['l1'], 2, '0', STR_PAD_LEFT);
            $l2 = str_pad($result[0]['l2'], 2, '0', STR_PAD_LEFT);
            $l3 = str_pad($result[0]['l3'], 2, '0', STR_PAD_LEFT);

            $party_count = $this->countRows('party', array('level3' => $l3));
            $l4 = $party_count + 1;
            $l4 = str_pad($l4, 4, '0', STR_PAD_LEFT);

            return $l1 . '-' . $l2 . '-' . $l3 . '-' . $l4;
        }

        return false;
    }

    public function countRows($table, $where)
    {
        $query = $this->db->get_where($table, $where);
        return $query->num_rows();
    }

    public function saveForm($table, $where, $dataObject) {
        $this->db->where($where);
        $result = $this->db->get($table);              
        if ($result->num_rows() > 0) {

            $this->db->where($where);
            $result = $this->db->update($table, $dataObject);
            return $this->db->affected_rows();
        } else {    

            $result = $this->db->insert($table, $dataObject);
            return $this->db->insert_id();
        }
    }

    public function saveItemForm($table, $where, $dataObject)
    {

        if (isset($_FILES['photo']) && $_FILES['photo']['size'] > 0) {
            $dataObject['photo'] = $this->upload_photo($dataObject);
        } else {
            unset($dataObject['photo']);
        }

        $dataObject['item_code'] = $this->genItemString($dataObject['catid'], $item['subcatid']);

        $this->db->where($where);
        $result = $this->db->get($table);

        if ($result->num_rows() > 0) {

            $this->db->where($where);
            $result = $this->db->update($table, $dataObject);
            return $this->db->affected_rows();
        } else {    

            $result = $this->db->insert($table, $dataObject);
            return $this->db->insert_id();
        }
    }

    public function fetchAllItem($activee = -1)
    {
        $crit = '';
        if ($activee == -1) {

            $crit = '';
        } else {
            
            $crit = ' where item.active=1 ';
        }

        $result = $this->db->query("SELECT item.*,item.size, category.name AS 'category_name',
                subcategory.name AS 'subcategory_name', brand.name AS 'brand_name', made.name AS 'made_name',
                ifnull(sd.stqty,0) as stqty, ifnull(sd.stweight,0) as stweight, item.item_barcode
                FROM item
                INNER JOIN category ON item.catid = category.catid 
                LEFT JOIN subcategory ON item.subcatid = subcategory.subcatid 
                LEFT JOIN brand ON item.bid = brand.bid
                LEFT JOIN made ON item.made_id = made.made_id
                LEFT JOIN department_table ON department_table.department_id = item.department_id
                LEFT JOIN  (SELECT item_id,ifnull(sum(qty),0) stqty,ifnull(sum(weight),0) stweight from stockdetail group by item_id) sd on sd.item_id=item.item_id $crit");

        return $result->result_array();
    }

    public function genItemString($catId, $subCatId)
    {
        $catId = str_pad($catId, 2, '0', STR_PAD_LEFT);
        $subCatId = str_pad($subCatId, 2, '0', STR_PAD_LEFT);

        $items_count = $this->countRows('item', array('subcatid' => $subCatId));
        $code = $items_count + 1;
        $code = str_pad($code, 3, '0', STR_PAD_LEFT);

        return $catId . '-' . $subCatId . '-' . $code;
    }

    public function lastFiveSRate($crit, $companyId, $etype, $date)
    {
        $result = $this->db->query("SELECT  m.vrnoa, DATE(m.vrdate) vrdate,m.etype, ROUND(d.qty, 2) qty,d.rate
                                    FROM stockmain AS m
                                    INNER JOIN stockdetail AS d ON m.stid = d.stid
                                    INNER JOIN item i ON i.item_id = d.item_id
                                    LEFT JOIN department dep ON dep.did = d.godown_id
                                    LEFT JOIN party party ON party.pid=m.party_id
                                    WHERE   m.etype = '" . $etype . "' and m.company_id = $companyId  and m.vrdate <= '" . $date . "' $crit
                                    ORDER BY m.vrdate  desc
                                    limit 5");

        if ($result->num_rows() > 0) {

            return $result->result_array();
        } else {

            return false;
        }
    }

    public function lastStockLocatons($itemId, $etype){

        $result = $this->db->query("SELECT g.name AS location, IFNULL(SUM(d.qty),0) AS qty
                                    FROM stockdetail d
                                    INNER JOIN department g ON g.did=d.godown_id
                                    WHERE d.item_id= $itemId
                                    GROUP BY g.name");

        if ($result->num_rows() > 0) {

            return $result->result_array();
        } else {
            
            return false;
        }
    }

    public function getMaxVrno($table, $etype, $companyId)
    {

        $result = $this->db->query("SELECT MAX(vrno) vrno
                                    FROM $table
                                    WHERE etype = '" . $etype . "' AND company_id=" . $companyId . " AND DATE(vrdate) = DATE(NOW())");
        $row = $result->row_array();
        $maxVrno = $row['vrno'];
        return $maxVrno;
    }

    public function getMaxVrnoa($table, $etype, $companyId)
    {

        $result = $this->db->query("SELECT MAX(vrnoa) vrnoa
                                    FROM $table
                                    WHERE etype = '" . $etype . "' AND company_id=" . $companyId);
        $row = $result->row_array();
        $maxVrnoa = $row['vrnoa'];
        return $maxVrnoa;
    }

    public function fetchByCol($table, $col)
    {
        $result = $this->db->query("SELECT DISTINCT $col FROM $table");
        return $result->result_array();
    }

    public function saveStockForm($tableMain, $tableDetail, $mainData, $detailData, $vrnoa, $etype) {

        $companyId = $mainData['company_id'];
        $mainData['etype'] = $etype;
        $mainData['vrdate'] = date('Y-m-d', strtotime($mainData['vrdate']));
        $godownId = $detailData[0]['godown_id'];

        $this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype, 'company_id' => $mainData['company_id']));
        $result = $this->db->get($tableMain);

        $stId = "";
        $affect = 0;
        if ($result->num_rows() > 0) {

            $result = $result->row_array();
            $stId = $result['stid'];
            $this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype, 'company_id' => $mainData['company_id']));
            $this->db->update($tableMain, $mainData);

            $this->db->where(array('stid' => $stId));
            $this->db->delete($tableDetail);
            $affect = $this->db->affected_rows();
        } else {
            
            $this->db->insert($tableMain, $mainData);
            $stId = $this->db->insert_id();
            $affect = $this->db->affected_rows();
        }

        foreach ($detailData as $detail) {
            
            $detail['stid'] = $stId;
            $this->db->insert($tableDetail, $detail);
            $affect = $this->db->affected_rows();
        }

        if ($etype == 'production') {
            
            $this->db->query("call spw_ProductionLess($godownId, $vrnoa, $companyId)");
        }

        if ($affect) {
            
            return true;
        } else {
            
            return false;
        }
    }

    public function saveLedger($ledgers, $dcno, $etype, $voucherTypeHidden) {

        if ($voucherTypeHidden != 'new'){

            $this->db->where(array(
                'dcno' => $dcno,
                'etype' => $etype,
            ));
            $affect = $this->db->delete('pledger');
        }

        $affect = 0;
        foreach ($ledgers as $ledger) {
        
            $ledger["dcno"] = $dcno;
            $this->db->insert('pledger', $ledger);
            $affect = $this->db->affected_rows();
        }

        if ($affect === 0) {
            
            return false;
        } else {
            
            return true;
        }
    }
}