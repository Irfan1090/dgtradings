<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class RangeError extends Exception{  }
class CI_Definitions {
    private $controller; 
    private $small; 
	private $big ;
	private $sbig ; 
	private $shortname ; 
	private $spid ; 
	private $custom_type ; 
   	private $etype ;
   	private $table ;
   	private $commonModel ;
   	private $arr;
   	private $CI;
   	private $input;
   	private $load;
    private $detailUrl;
    private $_etypehere;
    private $_etyparray;
    private $join_val;
    private $joins;
    private $urlToNext;
   	private $modalDataFileUrl = "setup/generalaccounts/generalaccountsDetail";
       	public function __construct($arrr)
	{
		$this->controller = $arrr['controller'];
		$this->small = $arrr['small'];
		$this->big = $arrr['big'];
		$this->sbig = $arrr['sbig'];
		$this->spid = $arrr['spid'];
        $this->urlToNext = $arrr['urlToNext'];
		$this->shortname = $arrr['shortname'];
		$this->custom_type = isset($arrr['custom_type'])?$arrr['custom_type']:'';
		$this->etype = $arrr['etype'];
        $this->_etypearray = ($this->etype!="")? array('etype'=>$this->etype) : array(); // in else any non numeric value can be given but both key and value must be equal 
		$this->table = $arrr['table'];
		$this->commonModel = $arrr['commonModel'];
		$this->arr = get_object_vars($this);
		$data =  $this->getdata($this->urlToNext); // pick data for js and detail listing
        $this->arr['chunked'] = $data['chunked'];
    	$this->arr['jsData'] = $data['jsData'];
        $this->arr['uses_compdown'] = $data['uses_compdown'];
        $this->join_val =  $arrr['join_val'];
    	$this->joins =  $arrr['joins'];
    	$this->CI = & get_instance();
    	$this->input = $this->CI->input;
    	$this->load = $this->CI->load;
    	$this->detailUrl = base_url()."application/views/setup/generalaccounts/generalaccountsDetail.php"; // pick data for  voucher view modal data file making
	}

       
    public function fetchVoucherColumns()
    {
        $ret = [];
        $data = $this->filedata($this->urlToNext);
        $doc = new DOMDocument();
        @$doc->loadHTML($data);
        $doc->saveHTML();
        $dom = new DOMXPath($doc);
        $lisfinder = $dom->query("//*[contains(@class,'fetch')]");
        foreach ($lisfinder as $key => $value) 
        {
            $ambigousname = ($value->hasAttribute('ambigousname'))? $value->getAttribute('ambigousname') :$value->getAttribute('name');
            $lineno = $value->getLineNo();        
                $idAndLineNumber = ($value->hasAttribute('id'))?" near id like '".$value->getAttribute('id')."' OR near line number round about '".$lineno."'.": "near line number round about '".$lineno."'.";
            if($value->hasAttribute('name'))
            {
                $idAndLineNumber.=" OR near name attribute value like '".$value->getAttribute('name')."'";
                $name = $value->getAttribute('name');
                $message = 'You need to provide ambigousname attribute value e.g ambigousname="party.name -as- party_name,true" to use it for voucher fetch purposes.. in File '.$this->urlToNext.' '.$idAndLineNumber.' .. Notice: if you provide second parameter ( true ) then it will consider it to be executed for ( listing page search and data showing ) and voucher fetch column building and in case if  second parameter is ( false ) or ( comma or value after comma not given ) then it will just work for detail listing page not for voucher fetch column building page';
                if($value->hasAttribute('ambigousname'))
                { 
                    if(strpos( $ambname = $value->getAttribute('ambigousname') , ',')===false)
                        $this->Throwable('FetchVoucherWrongParameterPassing',$message,'console');
                    $ex = explode(',',$ambname);
                    if($ex[0]!='' && strpos($ex[0],'-as-')!==false && $ex[1]==true)
                        array_push($ret,preg_replace('/-as-/', ' as ', $ex[0]));
                    else
                        $this->Throwable('FetchVoucherWrongParameterPassing',$message,'console');
                }
                else
                {
                    array_push($ret,$name);
                }
            }
            else
            {           
                        // first parameter is Type OF ERROR And Second is Message to display.
                $this->Throwable(' AttributeError','With fetch class you need to give atrribute name="database-field-name" '.$idAndLineNumber);
            }
        }
        return $ret;
    }
	public function valgetter($tags)
        {
            $retarray = [];
            $retcomparray = [];
            $retcomp2array = [];
            $position = [];
            $number = 0;
            $i = 0;
                    foreach ($tags as $key => $value) 
                    {
                        if($value->hasAttribute('listattribute'))
                        { 
                            $at = $value->getAttribute('listattribute');
                            $name = $value->getAttribute('name');
                            
                            // $ambigousname = ($value->hasAttribute('ambigousname'))? ( ( strpos($attr = $value->getAttribute('ambigousname'),',')!==false && ($ex=explode(',', $attr)))?$ex[0] : $attr  )  :$value->getAttribute('name');
                            if(!empty($at))
                            {  
                                $i++;
                                    if(($ambattr=$value->hasAttribute('ambigousname')) && ( empty($am = $value->getAttribute('ambigousname')) || ($pos = strpos(( strpos($am,',')!==false?$exu = explode(',', $am)[0]:$exu = $am ),'-as-'))===false ) )
                                    {
                                        $this->Throwable('EmptyOrInvalidParamterPass','If you consider this column to be ambigous then Please follow this e.g ambigousname="party.name -as- party_name,true" true will be passed if you want this field to be fetched otherwise ambigousname="party.name -as- party_name" at Voucher  '.$this->urlToNext.' near listattribute="'.$at.'" and name="'.$name.'"','screen');
                                    }
                                $ambArray = ($ambattr)?  $exd = explode('-as-',$exu): $exd = $name;
                               
                                $ambigousname = is_array($ambArray)?$exd[0]:$ambArray;
                                array_push($retcomparray,$at,$name,$i);          
                                if(is_array($ambArray))
                                {   
                                     $getatt = trim($exd[1]);
                                      // $jsattr = explode(',',$getatt)[1];
                                    ($value->hasAttribute('position') && !empty($re = $value->getAttribute('position')))? array_push($position,array($re,[$at,$name,$ambigousname.",".$getatt],[$at,$getatt],$i)) : null;
                                	array_push($retcomp2array,$at,$name,$ambigousname.",".$getatt);
                                    
                                	array_push($retarray,$at,$getatt);
                                } 
                                else
                                {                 
                                    $name = $value->getAttribute('name');                                                                                      //third array in below array is is for jsData........ and $i is index position
                                    if($value->hasAttribute('position') && !empty($re = $value->getAttribute('position')))
                                    { 
                                        array_push($position,array($re,[$at,$name,$ambigousname],[$at,$name],$i)); // 2nd array is for chunked value
                                        
                                    }                                 	
                                    array_push($retcomp2array,$at,$name,$ambigousname);
                                	array_push($retarray,$at,$name);	 
                                }
                            }
                        }
                    }
                    // die(var_dump(''));
                $retcomp2array = $this->posSetter($retcomp2array,$position,2,3,1); // 2nd last parameter is for $position[whatever][1]  
                $retarray = $this->posSetter($retarray,$position,1,2,2); // 2nd last parameter is for $position[whatever][2]

            return [$retarray,$retcomparray,$retcomp2array];
        }
    public  function posSetter($retcomp2array,$position,$which,$chunk,$valindex)
            {
                $i = 0;
                $ii = 0;
                $retcomp2array = array_chunk($retcomp2array, $chunk);
                $arraycount = count($retcomp2array);
                if(count($position)>0)
                {
                    foreach ($retcomp2array as $key2 => $value2) 
                    {
                        $i++;
                        if(!empty($value2))
                        {
                            foreach ($position as $key => $value) 
                            {
                                    if($value[0]>$arraycount-1 || $value[0]<1)
                                    {
                                        
                                        $this->Throwable('RangeError','Position value should be between ( 1 and  '. ($arraycount-1) .' ) for Voucher  '.$this->urlToNext.' near listattribute="'.$value[1][0].'" and name="'.$value[1][1].'"','screen');
                                        
                                    }
                                if($key2==$value[0])
                                {
                                    $ii++;
                                //e.g 1st IS 'listattribute' AND 2ND IS 'name' attribute AND THIRD IS 'ambigousname' attribute IF NOT IT WILL HAVE 'name' VALUE
                                                $center = array("any_key".$i => $value[$valindex]);// any key can be any alphanumeric not just numeric that is invalid 
                                                            $keyindex = -1;
                                                            foreach ($retcomp2array as $innerkey => $innervalue) 
                                                            {
                                                                foreach ($innervalue as $innerkey2 => $innervalue2) 
                                                                {
                                                                    if($value[$valindex][0]===$innervalue2 && $value[$valindex][1]===$innervalue[$innerkey2+1])
                                                                    {
                                                                        $keyindex = $innerkey;    
                                                                    }
                                                                }
                                                            }
                                                    if($keyindex>-1)
                                                    {
                                                        unset($retcomp2array[$keyindex]);
                                                    }
                                                $retcomp2array = array_slice($retcomp2array, 0, $key2-1, true) +
                                                $center + // ADD NEW ARRAY AT GIVEN POSITION
                                                array_slice($retcomp2array, $key2-1, count($retcomp2array)-1, true);
                                                 
                                                    $retcomp2array = array_values($retcomp2array); //RESET THEIR INDEX KEYS AND REMOVE KEYS LIKE 'any_key' GIVEN ABOVE
                                }
                            }
                        }    
                    }
                }
                
                $ret = array_reduce($retcomp2array, function($old,$current){
                    return array_merge($old,$current);
                },[]);
                
                return $ret;   
            }
    public function Throwable($type,$msg,$tru='console')
    {  
        
        try
        {
            throw new RangeError(''); 
        }
        catch(RangeError $e)
        {
            if($tru==='screen' && ( defined('ENVIRONMENT')? ENVIRONMENT: 'production')!='production' )
            {
                $message = explode(' ',$msg);
                $msg = '';
                array_walk_recursive($message, function($val,$key) use (&$msg){
                $msg .= ($key%10!=0 || $key===0)?' '.$val:' '.$val.' <br/>&nbsp; ';
                });
                die(' <h3>Error Type: '.$type.'</h3><h2 style="border:2px dashed black;border-top:2px dotted black;border-radius:10px;padding:5px;">&nbsp;Message : '.$msg.' </h2>');   
            }
            else
            {
                die('Error Type: '.$type.'.. Message: '.$msg);
            }
        }
    }
    public function filedata($url)
    {
    	$ch = curl_init();
                    curl_setopt($ch,CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
                    curl_setopt($ch,CURLOPT_POST, true);
                    $result = curl_exec($ch);
                    curl_close($ch);
                    return $result;
    }
    public  function getdata($url)
        {
                    $result = $this->filedata($url);
                    $doc = new DOMDocument();
                    @$doc->loadHTML($result);
                    $doc->saveHTML();
                    $dom = new DOMXPath($doc);
                    $lisfinder = $dom->query("//*[contains(@listattribute,'')]");
                    $tags = $this->valgetter($lisfinder);
                    $lastelarray = array('Default Date','Created At','Actions');
                    $data['uses_compdown'] =  array_chunk($tags[1],3); // IS BEING USED FOR ORDER BY PURPOSE
                    $uses = array_merge($tags[0],$lastelarray);
                    $data['chunked'] =  array_chunk($tags[2], 3); // IS BEING USED TO FETCH AND SEARCH DATA ON LISTING PAGE AND IN FUNCTION getDefDetailColumns()
                    $data['jsData'] = implode(',', $uses); // THIS DATA IS BEING USED IN JS FOR LISTING PAGE
                   
                    return $data;
        }
    	public function getinfo()
        {
            $column = $this->spid;
        $sortingOrder = 'DESC'; 
        
        if(isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['column'] != '')
        {
            $column = $_REQUEST['order'][0]['column'];
        }  
        if(isset($_REQUEST['order'][0]['dir']) && $_REQUEST['order'][0]['dir'] == 'asc')
        {
            $sortingOrder = 'ASC';
        }  
        $orderBy = $this->spid;
        $local = $this->arr['uses_compdown'];
        foreach ($local as $key => $value) {

            $value2 = $value[2];
            $value1 = $value[1];
            if($column==$value2)
            {   
                $orderBy = $value1;    
                break;
            }
        }
        $orderBy = $orderBy.' '.$sortingOrder;
        $where = '';
        $arrr = $this->arr['chunked'];
        foreach ($arrr as $key => $value) {
            if( (bool)(strpos($value[2], ','))===false){
            	$req2 = $value[2]; // ambigous name if available otherwise it will be equal to name attribute....
            	$req = $value[1]; // name attribute value....
            }
            else{
            	$req2 = explode(',', $value[2])[0]; // HERE IT IS USING ATTRIBUTE NAME ('ambigousname')
            	$req = explode(',', $value[2])[1]; // HERE IT IS GETTING REQUEST WITH OUR ('as') ATTRIBUTE IN VIEW FILE
            } 
		if(isset($_REQUEST[$req]) && $_REQUEST[$req]!='')             
		{
			$oR = ($where != '') ? 'OR' : '';                             
			$where .= $oR." ".$req2." LIKE '%".$_REQUEST[$req]."%' ";             
		}

	}
        if($where != ""){

            $where = " AND(".$where.")";
        }
 
        if(isset($_REQUEST['created_at_from']) && $_REQUEST['created_at_from'] != ''){

            $where .= " AND created_at >= '".date('Y-m-d', strtotime($_REQUEST['created_at_from']))."' ";
        }

        if(isset($_REQUEST['created_at_to']) && $_REQUEST['created_at_to'] != ''){

            $where .= " AND created_at <= '".date('Y-m-d', strtotime($_REQUEST['created_at_to']. ' +1 day'))."' ";
        }

        if(isset($_REQUEST['default_date_from']) && $_REQUEST['default_date_from'] != ''){

            $where .= " AND default_date >= '".date('Y-m-d', strtotime($_REQUEST['default_date_from']))."' ";
        }

        if(isset($_REQUEST['default_date_to']) && $_REQUEST['default_date_to'] != ''){

            $where .= " AND default_date <= '".date('Y-m-d', strtotime($_REQUEST['default_date_to']. ' +1 day'))."' ";
        }
        $this->_etypehere = (!empty($this->etype))?  ' and '.$this->table.'.etype ="'.$this->etype.'"' : ' ';
        $dbQuery = 'SELECT count('.$this->table.'.'.$this->spid.') as total_records 
                    FROM '.$this->table.' '.$this->join_val.'                     
                    WHERE '.$this->table.'.'.$this->spid.' <> 0 '.$where . $this->_etypehere;
        $dbTotalRecords = $this->commonModel->executeExactString($dbQuery);
        /* 
        * Paging
        */

        $iTotalRecords = $dbTotalRecords[0]['total_records'];
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $dbQuery = 'SELECT '.$this->getDefDetailColumns().'
                    FROM '.$this->table.'
                    '.$this->join_val.' 
                    WHERE '.$this->table.'.'.$this->spid.' <> 0 '.$where.$this->_etypehere.' order by '.$orderBy.' limit '.$iDisplayStart.', '.$iDisplayLength;
           
        $results = $this->commonModel->executeExactString($dbQuery);
        $data = [];
        $da = [];
        $data1 = [];
        $i = -1;
        foreach ($results as $key => $result) {
            $i++;
            $labelClass = ($result['active'] == 1) ? 'success' : 'danger';
            $labelText = ($result['active'] == 1) ? 'Active' : 'Inactive';
            $statusClass = ($result['active'] == 1) ? 'btnUnverify red' : 'btnVerify green';
            $statusIcon = ($result['active'] == 1) ? '<i class="fa fa-clock-o"></i>' : '<i class="fa fa-check"></i>';
            $arrr = $this->arr['chunked'];
            $data1[] = array('<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$result[$this->spid].'"/><span></span></label>');
            foreach ($arrr as $key => $value) {
            	$strpos = ( (bool)(strpos($value[2], ','))===true)? explode(',', $value[2])[1] : false;
            	if($strpos!==false)
            	{ 
            		$index = trim($strpos);
            	} 
            	else 
            	{
            		$index = $value[1];
            	};
                
            	$value2 = ($index==='active')? ( '<span class="label label-sm label-'.$labelClass.'">'.$labelText.'</span>' ) : ( ($result[$index])? $result[$index]:'-' );
                array_push($data1[$i],$value2);
            }
            $data[] = array(
                
                ($result['default_date']) ? date('d M Y', strtotime(substr($result['default_date'], 0, 11))) : '-',
                ($result['created_at']) ? date('d M Y', strtotime(substr($result['created_at'], 0, 11))) : '-',
                '<a class="btn btn-sm blue btn-outline '.$statusClass.'" data-'.$this->spid.'="'.$result[$this->spid].'">'.$statusIcon.'</a>
                <a href="#detailPopup" class="btn btn-sm blue btn-outline detailPopup" data-toggle="modal" data-'.$this->spid.'="'.$result[$this->spid].'"><i class="fa fa-eye"></i></a>
                <a href="'.base_url().$this->controller.'/addEdit'.$this->big.'?id='.$result[$this->spid].'" class="btn btn-sm btn-outline blue"><i class="fa fa-pencil"></i></a>
                <a href="javascript:;" class="btn btn-sm btn-outline red btnDelete" data-'.$this->spid.'="'.$result[$this->spid].'"><i class="fa fa-trash"></i></a>',
           );
            $da[] = array_merge($data1[$i],$data[$i]);

             
        }
        $records["data"] = $da;
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
          
        echo json_encode($records);
        exit();
        }
    public function delDefinitions()
    {
    			if($this->input->post()){

			$spIds = $this->input->post('sp_ids');
			if(is_array($spIds)){

				foreach ($spIds as $spId) {
				
					$this->commonModel->delete($this->table, array_merge(array($this->spid => $spId),$this->_etypearray));
				}
			}else{

				$this->commonModel->delete($this->table, array_merge(array($this->spid => $spIds),$this->_etypearray));
			}	
				
			echo json_encode(1);
		}
		exit();
    }
    public function loadDefListView()
    {
       
    	$this->CI->load->view('setup/generalaccounts/generalaccounts',$this->arr);
    }

    public function getMaxId($param ="optional")
    {
                   
    	$maxId = (isset($this->etype) && !empty($this->etype))? $this->commonModel->getMaxId($this->table, $this->spid,$this->_etypearray) + 1 : $this->commonModel->getMaxId($this->table, $this->spid) + 1;
		if($param==='optional')
        {
		  echo $maxId;
		  exit();
        }
        else
        {
            return $maxId;
        }
    }
    public function saveDef()
    {
    	$accountDetail = $this->input->post($this->small.'_detail');
        $secarray = array_merge(array($this->spid.' <>' => $accountDetail[$this->spid], 'name' => $accountDetail['name']),$this->_etypearray);
		$isValid = ($this->spid==='pid')? $this->commonModel->isAlreadySaved($this->table, array($this->spid.' <>' => $accountDetail[$this->spid], 'name' => $accountDetail['name'])) : $this->commonModel->isAlreadySaved($this->table, $secarray);
        if (!$isValid) 
        { 
           
			if($this->spid==='pid' || $this->spid==='spid')
            { 
                $accountDetail['account_id'] = $this->commonModel->genAccStr($accountDetail['level3']); 
            }

			if($_POST['voucher_type_hidden'] == 'new'){

				$maxId = $this->getMaxId('anyvalue');

				$id = $accountDetail[$this->spid] = $maxId;

			}
            else
            {
                if(isset($accountDetail[$this->spid]) ) { $id = $accountDetail[$this->spid]; unset($accountDetail[$this->spid]); }
            }
                       
            
			$where = array_merge(array($this->spid => $id),$this->_etypearray);
			$accountDetail['default_date'] = date('Y-m-d', strtotime($accountDetail['default_date']));
			if(isset($accountDetail['pid']) ) unset($accountDetail['pid']);
			$result = $this->commonModel->saveForm($this->table, $where, $accountDetail);

			echo json_encode($result);
		} 
        else 
        {
            echo json_encode("duplicate_name");
        }

		exit();
    }
    public function getDefDetailColumns()
    {
        
    	$ch = $this->arr['chunked'];
				$arr = [];
				foreach ($ch as $key => $value) {
					$val = $value[2];
					if( (bool)strpos($val, ',')===true)
                    {
                        $exp = explode(',', $val);
						array_push($arr,$exp[0].' as '.$exp[1]);
					}
                    else
                    {
						array_push($arr,$value[2]);
				    }
                }
				array_push($arr, $this->table.'.created_at',$this->table.'.default_date');
				$ret = implode(',', $arr);
				return $ret;
    }
    public function getDetail()
    {
    		if($this->input->post()){

			$partyId = $this->input->post('sp_id');
			$select = $this->getDefDetailColumns();
			$result = call_user_func_array(array($this->commonModel,"find"),array_merge(array($this->table, $select, array_merge(array($this->spid => $partyId),$this->_etypearray)), $this->joins));
			if(count($result)>0){
				
				$data['result'] = $result[0];
				$filedat = $this->filedata($this->detailUrl);
				$doc = new DOMDocument();
				@$doc->loadHTML($filedat);
				$doc->saveHTML();
				$dynamic = $doc->getElementsByTagName('dynamic');
				foreach ($dynamic as $key => $value) {
					if($value->hasAttribute('name'))
					{
						$att = $value->getAttribute('name');
						if(array_key_exists($att, $data['result']))
							{ 
								$data[$att] = "style='display:block;'"; 
							}
						else 
							{
								$data[$att] = "style='display:none;'";
								$data['result'][$att] = "";
							}
					}
				}
				 
				$data['idname'] = $this->shortname;
				$html = $this->load->view($this->modalDataFileUrl, $data, true);	
			}else{
				
				$html = "<div class='row'>
							<div class='col-md-12'><div class='alert alert-danger'>
                                <button class='close' data-close='alert'></button> No ".$this->sbig." Detail found. 
                            </div>
                        </div>";
			}
			
			echo json_encode($html);
			exit();
		}
		exit();
    }
    public function changeStatus()
    {
    		if($this->input->post()){

			$accountsId = $this->input->post('sp_id');
			$activeStatus = $this->input->post('active');
			$result = ($this->spid==='pid')? $this->commonModel->update($this->table, array('active' => $activeStatus), array($this->spid => $accountsId)) :$this->commonModel->update($this->table, array('active' => $activeStatus), array_merge(array($this->spid => $accountsId),$this->_etypearray));
			
			echo json_encode($result);
			exit();
		}
		exit();
    }
    public function fetchDef()
    {
    			if ($this->input->post()) {

			$SpId = $this->input->post($this->spid);
			$select = $this->fetchVoucherColumns();
			$result = $this->commonModel->find($this->table, $select, array_merge(array($this->spid => $SpId),$this->_etypearray));
            if($result){
				$result[0]['default_date'] = date('d M y', strtotime($result[0]['default_date']));
			}
			echo json_encode($result);
		}
		exit();
    }
}


  //   public function loadDefList()
  //   {
  //   	$column = 'default';
	 //    $sortingOrder = 'DESC'; 
	    
	 //    if(isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['column'] != '')
	 //    {
	 //        $column = $_REQUEST['order'][0]['column'];
	 //    }  

	 //    if(isset($_REQUEST['order'][0]['dir']) && $_REQUEST['order'][0]['dir'] == 'asc')
	 //    {
	 //        $sortingOrder = 'ASC';
	 //    }  
	    
	 //    $orderBy = '';
	 //    $local = $this->arr['uses_compdown'];
  //       foreach ($local as $key => $value) {

  //           $value2 = $value[2];
  //           $value1 = $value[1];
  //           if($column==$value2)
  //           {   
  //               $orderBy = $value1;
  //               break;
  //           }
  //           else
  //           {
  //               $orderBy = $this->spid;
  //           }
  //       }
	        
	 //    $orderBy = $orderBy.' '.$sortingOrder; 
	 //    $where = '';
	 //    if(isset($_REQUEST['search']['value']) && $_REQUEST['search']['value'] != ''){

	 //    	$search = $_REQUEST['search']['value'];
	 //        $where = $this->spid."  LIKE '%".$search."%' OR party.name LIKE '%".$search."%' OR party.mobile LIKE '%".$search."%' OR party.address LIKE '%".$search."%'";
	 //    }
	    
	 //    if($where != ""){

	 //        $where = " AND(".$where.")";
	 //    }
	    
	 //    $dbQuery = 'SELECT count(party.'.$this->spid.') as total_records 
	 //    			FROM party
		// 			INNER JOIN level3 ON level3.l3 = party.level3 
		// 			WHERE party.pid <> 0 '.$where .'and etype="'.$this->etype.'"';

		// $dbTotalRecords = $this->commonModel->executeExactString($dbQuery);
		// /* 
		// * Paging
		// */

		// $iTotalRecords = $dbTotalRecords[0]['total_records'];
		// $iDisplayLength = intval($_REQUEST['length']);
		// $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		// $iDisplayStart = intval($_REQUEST['start']);
		// $sEcho = intval($_REQUEST['draw']);

		// $dbQuery = 'SELECT party.'.$this->spid.', party.name, party.mobile, party.address, party.active
		// 			FROM party
		// 			INNER JOIN level3 ON level3.l3 = party.level3 
		// 			WHERE party.pid <> 0 '.$where.' and etype="'.$this->etype.'" order by '.$orderBy.' limit '.$iDisplayStart.', '.$iDisplayLength;

		// $results = $this->commonModel->executeExactString($dbQuery);
		
		// $data = [];
		// foreach ($results as $key => $result) {
			
		// 	$data[] = array(
		    	
		//     	$result[$this->spid],
		//       	($result['name']) ? $result['name'] : '-',
		//       	($result['mobile']) ? $result['mobile'] : '-',
		//       	($result['address']) ? $result['address'] : '-',
		//       	'<a href="javascript:;" class="btn btn-sm btn-outline btn'.$this->big.'Edit blue" data-dismiss="modal" data-'.$this->spid.'="'.$result[$this->spid].'"><i class="fa fa-pencil"></i></a>',
		//    );
		// }
		
		// $records["data"] = $data;
		// $records["draw"] = $sEcho;
		// $records["recordsTotal"] = $iTotalRecords;
		// $records["recordsFiltered"] = $iTotalRecords;
		  
		// echo json_encode($records);
  // 		exit();
  //   }